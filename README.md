# MWV2 Storefront - Hydrogen Demo Store

The Hydrogen demo of the Storefront app of MWV2 Project.

[MWV2 Jira Board](https://endertech.jira.com/jira/software/c/projects/MWV2/boards/446)

## Requirements

- Node.js version 16.14.0 or higher

# Admin API GraphQL setup

## .env

We have to have the "Admin API access token".
We use the `.env` file to store it.

If the file does not exist in your local working copy, please copy the `.env.dist` file located at the project root and create a new file named `.env`.
Find the "Admin API access token", and set as the value for the new file.

```bash
cp .env.dist .env
vi .env
```

## Apply patch on Hydrogen if necessary

Running the `"npm install"` command should apply the patch.

When Shopify team updates the Hydrogen package, we will re-apply the patch with:

```bash
npx patch-package @shopify/hydrogen
```
