/* We  */
/* Tailwind Configuration Docs: https://tailwindcss.com/docs/configuration */

const defaultTheme = require('tailwindcss/defaultTheme');

function withOpacityValue(variable) {
  return ({opacityValue}) => {
    if (opacityValue === undefined) {
      return `rgb(var(${variable}))`;
    }
    return `rgb(var(${variable}) / ${opacityValue})`;
  };
}

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './app/**/*.{js,ts,jsx,tsx}',
    './node_modules/flowbite-react/**/*.js',
  ],
  theme: {
    extend: {
      colors: {
        primary: withOpacityValue('--color-primary'),
        contrast: withOpacityValue('--color-contrast'),
        notice: withOpacityValue('--color-accent'),
        shopPay: 'var(--color-shop-pay)',
        'mw-red': {
          50: '#F9F2EF',
          100: '#FEEEED',
          200: '#EEDBD3',
          '200-hover': '#D7BCAE',
          300: '#E1C2B6',
          400: '#ED5A5A',
          500: '#E32329',
          DEFAULT: '#E32329',
          600: '#CC0707',
        },
        'mw-grayscale': {
          100: '#EBEBEA',
          200: '#D7D6D5',
          300: '#C3C1C0',
          400: '#9A9798',
          500: '#7D7879',
          600: '#5E5A5B',
          700: '#292929',
          DEFAULT: '#292929',
          800: '#161615',
          900: '#0B0B0B',
        },
      },
      typography: ({theme}) => ({
        DEFAULT: {
          css: {
            '--tw-prose-body': theme('colors.mw-grayscale.700'),
            '--tw-prose-headings': theme('colors.mw-grayscale.800'),
          },
        },
      }),
      screens: {
        'x-sm': '25em',
        sm: '32em',
        md: '48em',
        lg: '64em',
        xl: '80em',
        '2xl': '96em',
        'sm-max': {max: '48em'},
        'sm-only': {min: '32em', max: '48em'},
        'md-only': {min: '48em', max: '64em'},
        'lg-only': {min: '64em', max: '80em'},
        'xl-only': {min: '80em', max: '96em'},
        '2xl-only': {min: '96em'},
      },
      spacing: {
        nav: 'var(--height-nav)',
        screen: 'var(--screen-height, 100vh)',
      },
      height: {
        screen: 'var(--screen-height, 100vh)',
        'screen-no-nav':
          'calc(var(--screen-height, 100vh) - var(--height-nav))',
        'screen-dynamic': 'var(--screen-height-dynamic, 100vh)',
      },
      width: {
        mobileGallery: 'calc(100vw - 4rem)',
      },
      fontFamily: {
        sans: ['Lato', ...defaultTheme.fontFamily.sans],
        serif: ['"IBMPlexSerif"', 'Palatino', 'ui-serif'],
      },
      fontSize: {
        display: ['var(--font-size-display)', '1.1'],
        heading: ['var(--font-size-heading)', '1.25'],
        subheading: '1.5rem',
        subheading2: '1.25rem',
        lead: ['var(--font-size-lead)', '1.333'],
        copy: ['var(--font-size-copy)', '1.5'],
        fine: ['var(--font-size-fine)', '1.333'],
      },
      maxWidth: {
        'prose-narrow': '45ch',
        'prose-wide': '80ch',
        'page-width': '1336px', // 1240px + 3rem + 3rem
      },
      boxShadow: {
        border: 'inset 0px 0px 0px 1px rgb(var(--color-primary) / 0.08)',
        darkHeader: 'inset 0px -1px 0px 0px rgba(21, 21, 21, 0.4)',
        lightHeader: 'inset 0px -1px 0px 0px rgba(21, 21, 21, 0.05)',
        borderHeader: 'inset 0px -1px 0px 0px rgb(21, 21, 21, 0.23)',
      },
      letterSpacing: {
        lead: '0.075rem',
      },
    },
  },
  // plugins: [formsPlugin, typographyPlugin],
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('flowbite/plugin'),
  ],
};
