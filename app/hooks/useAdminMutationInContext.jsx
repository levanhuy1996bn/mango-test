export function useAdminMutationInContext(context, mutation, payload) {
  return loadAdminMutationInContext(context, mutation, payload);
}

export function loadAdminMutationInContext(context, mutation, payload) {
  if (!payload?.variables) {
    payload.variables = {};
  }
  // this depends on the app/override/hydrogen/fetch.js file
  // and the related @shopify/hydrogen patch file being applied
  payload.variables.isUseAdminMutation = true;

  if (!payload?.headers) {
    payload.headers = {};
  }
  // supply the necessary token header
  // @see node_modules/@shopify/hydrogen/dist/development/index.js fetchStorefrontApi()
  payload.headers['X-Shopify-Access-Token'] =
    context?.env?.PRIVATE_SHOPIFY_ADMIN_API_TOKEN;

  return context?.storefront?.mutate(mutation, payload);
}
