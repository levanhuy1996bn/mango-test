import {catchChangeSelectedVariantEvent} from '~/lib/utils';
import {useEffect, useState} from 'react';

export function useSelectedVariant({selectedVariantFromServer}) {
  const [selectedVariant, setSelectedVariant] = useState(
    selectedVariantFromServer,
  );
  const handleOnChange = (event) => {
    catchChangeSelectedVariantEvent(event, setSelectedVariant);
  };
  useEffect(() => {
    document.addEventListener('onChangeSelectedVariant', handleOnChange);

    return () => {
      document.removeEventListener('onChangeSelectedVariant', handleOnChange);
    };
  }, []);

  return {selectedVariant};
}
