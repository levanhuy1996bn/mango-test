import {useShopQuery} from '@shopify/hydrogen';

export function useAdminQuery(input) {
  if (!input.variables) {
    input.variables = {};
  }
  // this depends on the src/override/foundation/fetchSync/server/fetchSync.js file
  // and the related useShopQuery patch file being applied
  input.variables.isUseAdminQuery = true;

  return useShopQuery(input);
}
