export function fetchProductFilters(
  url,
  body,
  handleProductFilters,
  setProductCount,
  setIsPending,
  selectedFilters,
) {
  fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
  })
    .then((response) => {
      return response.json();
    })
    .then((dataFilters) => {
      const filters =
        dataFilters?.data?.collection?.productFilters?.filters ?? [];
      if (selectedFilters !== undefined) {
        handleProductFilters(filters, setProductCount, selectedFilters);
      } else {
        handleProductFilters(filters);
      }
      setIsPending(false);
    })
    .catch((error) => {
      console.warn(error);
    });
}
