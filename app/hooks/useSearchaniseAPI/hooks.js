import {useQuery} from '@shopify/hydrogen';
import {SEARCH_ANISE_API_KEY, SEARCH_ANISE_BASE_URL} from '~/lib/const';

export function useSearchaniseAPI({params}) {
  const url = `${SEARCH_ANISE_BASE_URL}/getresults?apiKey=${SEARCH_ANISE_API_KEY}&${params}`;
  const {data} = useQuery([params, url], async () => {
    const response = await fetch(url, {
      method: 'GET',
    });

    return response.json();
  });

  return data;
}
