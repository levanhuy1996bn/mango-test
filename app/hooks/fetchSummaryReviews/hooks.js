import {useQuery} from '@shopify/hydrogen';
import {
  POWER_REVIEWS_BASE_URL,
  POWER_REVIEWS_MERCHANT_ID,
  POWER_REVIEWS_READ_API_KEY,
} from '~/lib/const';

export function fetchSummaryReviews({product}) {
  const productID = product.handle.substring(0, 50);
  const powerReviewsReadApiKey = POWER_REVIEWS_READ_API_KEY;
  const powerReviewsBaseUrl = POWER_REVIEWS_BASE_URL;
  const merchantId = POWER_REVIEWS_MERCHANT_ID;
  const locale = 'en_US';
  let url = `${powerReviewsBaseUrl}/m/${merchantId}/l/${locale}/product/${productID}/snippet?apikey=${powerReviewsReadApiKey}`;
  const {data} = useQuery(url, async () => {
    const response = await fetch(url, {
      method: 'GET',
    });
    return response.json();
  });

  return data;
}
