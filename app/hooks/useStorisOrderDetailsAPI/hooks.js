import {useQuery} from '@shopify/hydrogen';
import {MIDDLEWARE_API_BASE_URL} from '~/lib/const';

export function useStorisOrderDetailAPI({
  email,
  storisId,
  shopifyCustomerId,
  storisCustomerId = null,
}) {
  const PRIVATE_STORIS_TOKEN =
    typeof Oxygen !== 'undefined' ? Oxygen.env.PRIVATE_STORIS_TOKEN : '';
  let body = `email=${email}&shopifyCustomerId=${shopifyCustomerId}`;
  if (storisCustomerId) {
    body += `&storisCustomerId=${storisCustomerId}`;
  }
  const {data} = useQuery([body, storisId], async () => {
    const response = await fetch(
      `${MIDDLEWARE_API_BASE_URL}/api/orders/${storisId}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Bearer ${PRIVATE_STORIS_TOKEN}`,
          Origin: MIDDLEWARE_API_BASE_URL,
        },
        body,
      },
    );
    return response.json();
  });

  return {storisOrderDetails: data, oxygenData: Oxygen};
}
