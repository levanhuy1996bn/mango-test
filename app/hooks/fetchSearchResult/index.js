import {
  SEARCH_ANISE_API_KEY,
  SEARCH_ANISE_BASE_URL,
  SEARCH_PAGINATION_SIZE,
} from '~/lib/const';

export async function fetchSearchResult(
  isNotAnAdditionalAPICall,
  value,
  setProducts,
  setTotalItems,
  setIsPending,
  startIndex,
) {
  setIsPending(true);
  const searchAniseBaseUrl = SEARCH_ANISE_BASE_URL;
  const searchAniseApikey = SEARCH_ANISE_API_KEY;
  const endpoint = isNotAnAdditionalAPICall ? '/getresults' : '/search';
  fetch(
    `${searchAniseBaseUrl}${endpoint}?apiKey=${searchAniseApikey}&maxResults=${SEARCH_PAGINATION_SIZE}&q=${value}&startIndex=${startIndex}`,
    {
      method: 'GET',
    },
  )
    .then((response) => {
      return response.json();
    })
    .then(async (result) => {
      let newProducts = [];
      if (isNotAnAdditionalAPICall) {
        newProducts = result?.items || [];
      } else {
        if (result?.items?.length > 0) {
          const productIds = result?.items.map(
            (item) => `gid://shopify/Product/${item.product_id}`,
          );
          const response = await fetch('/api/productsByIds', {
            method: 'POST',
            body: JSON.stringify({productIds}),
          });

          if (response.ok) {
            const data = await response.json();
            newProducts = data?.nodes || [];
          }
        }
      }

      if (!startIndex) {
        setProducts(newProducts);
        setTotalItems(result.totalItems);
      } else {
        setProducts((prevProducts) => [...prevProducts, ...newProducts]);
      }
      setIsPending(false);
    })
    .catch((error) => {
      console.warn(error);
      setIsPending(false);
    });
}
