export function useAdminQueryInContext(context, query, payload) {
  return loadAdminQueryInContext(context, query, payload);
}

export function loadAdminQueryInContext(context, query, payload) {
  if (!payload?.variables) {
    payload.variables = {};
  }
  // this depends on the app/override/hydrogen/fetch.js file
  // and the related @shopify/hydrogen patch file being applied
  payload.variables.isUseAdminQuery = true;

  if (!payload?.headers) {
    payload.headers = {};
  }
  // supply the necessary token header
  // @see node_modules/@shopify/hydrogen/dist/development/index.js fetchStorefrontApi()
  payload.headers['X-Shopify-Access-Token'] =
    context?.env?.PRIVATE_SHOPIFY_ADMIN_API_TOKEN;

  return context?.storefront?.query(query, payload);
}
