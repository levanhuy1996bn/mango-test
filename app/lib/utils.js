import {countries} from '~/data/countries';
import {
  INITIAL_PRODUCT_PRICE_RANGE,
  PLANT_A_TREE_PRODUCT_ID,
} from '~/lib/const';
import {
  ADJUSTABLE_BASE,
  DEFAULT_TITLE,
  EXADJA_VARIANT_ID,
  EXADJB_VARIANT_ID,
  EXADJC_VARIANT_ID,
  EXMATA_VARIANT_ID,
  EXMATB_VARIANT_ID,
  EXMATC_VARIANT_ID,
  MATTRESSES,
  SYF_LIVE_URL,
  SYF_PARTNER_ID,
  SYF_TEST_URL,
} from '~/lib/const';
import moment from 'moment-timezone';
import {useCallback} from 'react';
// @ts-expect-error types not available
import typographicBase from 'typographic-base';

// import {getSelectedVariantFromProduct} from '../components/product/productVariantsMetafields';
import {fetchSearchResult} from '../hooks/fetchSearchResult';

/**
 * This is a hack until we have better built-in primitives for
 * causing server components to re-render.
 *
 * @returns function when called will cause the current page to re-render on the server
 */
// export function useRenderServerComponents() {
//   const {serverProps, setServerProps} = useServerProps();
//
//   return useCallback(() => {
//     setServerProps('renderRsc', !serverProps.renderRsc);
//   }, [serverProps, setServerProps]);
// }

export function missingClass(string, prefix) {
  if (!string) {
    return true;
  }

  const regex = new RegExp(` ?${prefix}`, 'g');
  return string.match(regex) === null;
}

export function formatText(input) {
  if (!input) {
    return;
  }

  if (typeof input !== 'string') {
    return input;
  }

  return typographicBase(input, {locale: 'en-us'}).replace(
    /\s([^\s<]+)\s*$/g,
    '\u00A0$1',
  );
}

export function isNewArrival(date, daysOld = 30) {
  return (
    new Date(date).valueOf() >
    new Date().setDate(new Date().getDate() - daysOld).valueOf()
  );
}

export function getUniqueFilterData(filterData) {
  let dataKeys = [];
  return filterData.filter((item) => {
    if (dataKeys.indexOf(item.id) === -1) {
      dataKeys.push(item.id);
      return item;
    }
  });
}

export function isDiscounted(price, compareAtPrice) {
  if (parseFloat(compareAtPrice?.amount) > parseFloat(price?.amount)) {
    return true;
  }
  return false;
}

export const saveAmount = (compareAtPrice, price) => {
  return parseFloat(compareAtPrice?.amount - price?.amount).toFixed(0);
};

export function getExcerpt(text) {
  const regex = /<p.*>(.*?)<\/p>/;
  const match = regex.exec(text);
  return match?.length ? match[0] : text;
}

function resolveToFromType(
  {customPrefixes, pathname, type} = {
    customPrefixes: {},
  },
) {
  if (!pathname || !type) return '';

  /*
        MenuItemType enum
        @see: https://shopify.dev/api/storefront/unstable/enums/MenuItemType
      */
  const defaultPrefixes = {
    BLOG: 'blogs',
    COLLECTION: 'collections',
    COLLECTIONS: 'collections',
    FRONTPAGE: 'frontpage',
    HTTP: '',
    PAGE: 'pages',
    CATALOG: 'collections/all',
    PRODUCT: 'products',
    SEARCH: 'search',
    SHOP_POLICY: 'policies',
  };

  const pathParts = pathname.split('/');
  const handle = pathParts.pop() || '';
  const routePrefix = {
    ...defaultPrefixes,
    ...customPrefixes,
  };

  switch (true) {
    // special cases
    case type === 'FRONTPAGE':
      return '/';

    case type === 'ARTICLE': {
      const blogHandle = pathParts.pop();
      return routePrefix.BLOG
        ? `/${routePrefix.BLOG}/${blogHandle}/${handle}/`
        : `/${blogHandle}/${handle}/`;
    }

    case type === 'COLLECTIONS':
      return `/${routePrefix.COLLECTIONS}`;

    case type === 'SEARCH':
      return `/${routePrefix.SEARCH}`;

    case type === 'CATALOG':
      return `/${routePrefix.CATALOG}`;

    // common cases: BLOG, PAGE, COLLECTION, PRODUCT, SHOP_POLICY, HTTP
    default:
      return routePrefix[type]
        ? `/${routePrefix[type]}/${handle}`
        : `/${handle}`;
  }
}

/*
  Parse each menu link and adding, isExternal, to and target
*/
function parseItem(customPrefixes = {}) {
  return function (item) {
    if (!item?.url || !item?.type) {
      // eslint-disable-next-line no-console
      console.warn('Invalid menu item.  Must include a url and type.');
      // @ts-ignore
      return;
    }

    // extract path from url because we don't need the origin on internal to attributes
    const {pathname} = new URL(item.url);

    /*
              Currently the MenuAPI only returns online store urls e.g — xyz.myshopify.com/..
              Note: update logic when API is updated to include the active qualified domain
            */
    const isInternalLink =
      /\.myshopify\.com/g.test(item.url) ||
      /\.mattresswarehouse\.com/g.test(item.url);

    const parsedItem = isInternalLink
      ? // internal links
        {
          ...item,
          isExternal: false,
          target: '_self',
          to: resolveToFromType({type: item.type, customPrefixes, pathname}),
        }
      : // external links
        {
          ...item,
          isExternal: true,
          target: '_blank',
          to: item.url,
        };

    return {
      ...parsedItem,
      items: item.items?.map(parseItem(customPrefixes)),
    };
  };
}

/*
  Recursively adds `to` and `target` attributes to links based on their url
  and resource type.
  It optionally overwrites url paths based on item.type
*/
export function parseMenu(menu, customPrefixes = {}) {
  if (!menu?.items) {
    // eslint-disable-next-line no-console
    console.warn('Invalid menu passed to parseMenu');
    // @ts-ignore
    return menu;
  }

  return {
    ...menu,
    items: menu.items.map(parseItem(customPrefixes)),
  };
}

export function getApiErrorMessage(field, data, errors) {
  if (errors?.length) return errors[0].message ?? errors[0];
  if (data?.[field]?.customerUserErrors?.length)
    return data[field].customerUserErrors[0].message;
  return null;
}

export function statusMessage(status) {
  const translations = {
    ATTEMPTED_DELIVERY: 'Attempted delivery',
    CANCELED: 'Canceled',
    CONFIRMED: 'Confirmed',
    DELIVERED: 'Delivered',
    FAILURE: 'Failure',
    FULFILLED: 'Fulfilled',
    IN_PROGRESS: 'In Progress',
    IN_TRANSIT: 'In transit',
    LABEL_PRINTED: 'Label printed',
    LABEL_PURCHASED: 'Label purchased',
    LABEL_VOIDED: 'Label voided',
    MARKED_AS_FULFILLED: 'Marked as fulfilled',
    NOT_DELIVERED: 'Not delivered',
    ON_HOLD: 'On Hold',
    OPEN: 'Open',
    OUT_FOR_DELIVERY: 'Out for delivery',
    PARTIALLY_FULFILLED: 'Partially Fulfilled',
    PENDING_FULFILLMENT: 'Pending',
    PICKED_UP: 'Displayed as Picked up',
    READY_FOR_PICKUP: 'Ready for pickup',
    RESTOCKED: 'Restocked',
    SCHEDULED: 'Scheduled',
    SUBMITTED: 'Submitted',
    UNFULFILLED: 'Unfulfilled',
  };
  try {
    return translations?.[status];
  } catch (error) {
    return status;
  }
}

export function emailValidation(email) {
  if (email.validity.valid) return null;

  return email.validity.valueMissing
    ? 'Please enter an email'
    : 'Please enter a valid email';
}

export function passwordValidation(password) {
  if (password.validity.valid) return null;

  if (password.validity.valueMissing) {
    return 'Please enter a password';
  }

  return 'Password must be at least 6 characters';
}

export const generateAdminApiVariantQuery = ({ids}) => {
  let query = '';

  ids.map((id) => {
    const variant = `variant${id.replace(/[^\d]+/, '')}`;
    query += `
      ${variant}: node(id: "${id}") {
         ... on ProductVariant {
            id
            product {
              averageReviewScore: metafield(namespace: "mw_marketing", key: "average_review_score") {
                value
              }
              status
              handle
              title
              featuredImage {
                id
                url
                altText
                width
                height
              }
            }
            availableForSale
            selectedOptions {
              name
              value
            }
            image {
              id
              url
              altText
              width
              height
            }
            price
            compareAtPrice
            sku
            title
         }
      }
    `;

    return id;
  });

  return query;
};

export const generateAdminApiVariantWithMetafieldsQuery = ({ids}) => {
  let query = '';

  ids.map((id) => {
    const variant = `variant${id.replace(/[^\d]+/, '')}`;
    query += `
      ${variant}: node(id: "${id}") {
        ... on ProductVariant {
          id
          product {
            averageReviewScore: metafield(namespace: "mw_marketing", key: "average_review_score") {
               value
            }
            handle
            title
            featuredImage {
              id
              url
              altText
              width
              height
            }
            metafields(first: 10) {
              nodes {
                namespace
                key
                value
              }
            }
          }
          availableForSale
          compareAtPrice
          selectedOptions {
            name
            value
          }
          image {
            id
            url
            altText
            width
            height
          }
          price
          sku
          title
          metafields(first: 10) {
            nodes {
              id
              namespace
              key
              value
            }
          }
        }
      }
    `;

    return id;
  });

  return query;
};

/**
 * A utility function to set cookies.
 * @param {string} cname - The name of the cookie.
 * @param {string} cvalue - The value of the cookie.
 * @param {number} exdays - The number of days until the cookie expires.
 */
export function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  let expires = 'expires=' + d.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

/**
 * A utility function to get cookies.
 * @param {string} cname - The name of the cookie.
 */
export function getCookie(cname) {
  if (!document?.cookie) {
    return '';
  }
  let name = cname + '=';
  let ca = document.cookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

/**
 * A utility funciton to check cookies.
 * @param {string} cname - The name of the cookie.
 */
export function checkCookie(cname) {
  const cvalue = getCookie(cname);
  if (cvalue != '' && cvalue != null && cvalue != 'undefined') {
    return true;
  } else {
    return false;
  }
}

/**
 * A utility function to remove cookies.
 * @param {string} cname - The name of the cookie.
 */
export function removeCookie(cname) {
  setCookie(cname, '', -1);
}

/**
 * A helper function to format phone number.
 * @param {string} phone number
 * @returns {string} formatted phone number. It returns the original phone number if it fails to format.
 */
export const MW_DEFAULT_PHONE_NUMBER = '8779597614';
export function formatPhoneNumber(str) {
  const cleaned = ('' + str).replace(/\D/g, '');
  const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
  if (match) {
    return '(' + match[1] + ') ' + match[2] + '-' + match[3];
  }
  return str;
}

/**
 * A helper function to generate a Google Map link.
 * @param {string} address
 * @returns {string} link to Google Map
 */
export function generateGoogleMapLink({address, googlePlaceId}) {
  const linkGoogleMap = googlePlaceId
    ? `https://www.google.com/maps/place/?q=place_id:${encodeURI(
        googlePlaceId,
      )}`
    : address
    ? `https://www.google.com/maps/search/?api=1&query=${encodeURI(address)}`
    : '';
  return linkGoogleMap;
}

/**
 * A helper function to convert 24 hours to 12 hours format.
 * @param {string} hour24 - two characters representing 24 hour value from '00' to '23'
 * @returns {string} hour12 - a string to represent 12 hour value with AM/PM such as '01:00 AM'
 */
export function convert24To12(hour24) {
  if (!hour24?.length) {
    return '';
  }
  const h24 = parseInt(hour24, 10);
  const h12 = h24 % 12 || 12;
  const amPm = Math.floor(h24 / 12) ? 'PM' : 'AM';
  const hour12 =
    h12 < 10 ? `${h12}:00 ${amPm}` : `${h12.toString()}:00 ${amPm}`;
  return hour12;
}

export function convertFiltersToString(filterObject, isChildren = false) {
  let filterString = '';
  Object.keys(filterObject).forEach(function (key) {
    if (typeof filterObject[key] === 'object') {
      filterString += `${key}:${convertFiltersToString(
        filterObject[key],
        true,
      )}`;

      if (isChildren) {
        filterString = `{${filterString}}`;
      }
    } else {
      let filterValue =
        typeof filterObject[key] === 'boolean'
          ? filterObject[key]
          : `"${filterObject[key].replaceAll('"', '\\"')}"`;
      filterString += `${filterString ? ',' : ''}${key}: ${filterValue}`;
    }
  });

  return `{${filterString}}`;
}

export function getProductFilterData(maxPrice, minPrice, selectedFilters) {
  let filters = '';
  let selectedLabels = '';

  if (maxPrice >= 0) {
    filters = `{price:{min:${minPrice},max:${maxPrice}}}`;
  }

  if (selectedFilters?.length) {
    selectedFilters.map((item, index) => {
      let filterString = convertFiltersToString(JSON.parse(item.input || '{}'));
      filters += filterString;
      selectedLabels += index === 0 ? item.label : `,${item.label}`;
    });
  }

  filters = filters ? `filters: [${filters}]` : '';
  return {selectedLabels, productFilterData: filters};
}

export function singleValueFromMetafields({
  metafields,
  namespace,
  metafieldKey,
}) {
  if (!metafields || !namespace || !metafieldKey) {
    return;
  }
  let value = null;
  value = metafields
    .filter((m) => m?.namespace === namespace && m?.key === metafieldKey)
    .map((m) => m?.value)
    .join();
  return value
    ? {
        key: metafieldKey,
        value,
      }
    : null;
}

export function singleValueWithLabelFromMetafields({
  metafields,
  namespace,
  metafieldKey,
  labels,
}) {
  if (!metafields || !namespace || !metafieldKey) {
    return;
  }
  let value = null;
  value = metafields
    .filter((m) => m?.namespace === namespace && m?.key === metafieldKey)
    .map((m) => m?.value)
    .join();
  return value
    ? {
        label: labels?.find(
          (label) =>
            label.namespace === namespace && label.key === metafieldKey,
        )?.name,
        value,
      }
    : null;
}

export const initializePrice = (filterValues) => {
  if (filterValues?.length) {
    const priceInput = JSON.parse(filterValues[0].input || '{}');
    if (priceInput?.price) {
      let maxPrice = parseInt(priceInput.price.max);
      maxPrice =
        maxPrice < parseFloat(priceInput.price.max) ? maxPrice + 1 : maxPrice;
      return {minPrice: priceInput.price.min, maxPrice};
    }
  }

  return INITIAL_PRODUCT_PRICE_RANGE;
};

export const priceFilterData = (filters) => {
  const priceFilters = filters.filter((filter) => {
    return filter.id === 'filter.v.price';
  });

  if (priceFilters.length) {
    return priceFilters[0].values;
  }

  return [];
};

export function getProductFiltersQuery(productFilterData, isGetAllFields) {
  const filterFields = getProductFilterFields(isGetAllFields);
  return `#grahql
    query ProductFilters(
      $collectionHandle: String
      $country: CountryCode
      $language: LanguageCode
      ) @inContext(country: $country, language: $language) {
        collection(handle: $collectionHandle) {
          productFilters: products(first: 1 ${productFilterData}) {
            filters {
              ${filterFields}
            }
          }
        }
    }
  `;
}

export function getProductFilterFields(isGetAllFields) {
  if (isGetAllFields) {
    return `
      id
       label
       type
       values {
          id
          label
          count
          input
       }
    `;
  }

  return `
    values {
       id
       count
    }`;
}

export const refreshProductCount = (productFilters, setProductCount) => {
  let productCountMatching = 0;
  productFilters.map((productFilter) => {
    productFilter.values.filter((value) => {
      const filterValue = JSON.parse(value?.input || '{}');
      if (filterValue?.available === true) {
        productCountMatching += value.count;
      }
    });
  });

  setProductCount(productCountMatching);
};

export function setProductFilterServerProps(
  setServerProps,
  productFilterData,
  sortKey,
  reverse,
) {
  setServerProps('productFilterData', productFilterData);
  setServerProps('sortKey', sortKey);
  setServerProps('reverse', reverse);
}

export function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func.apply(this, args);
    }, timeout);
  };
}

export const handleOnSearchInput = debounce(
  (
    isNotAnAdditionalAPICall,
    value,
    setSearchInputValue,
    setIsPending,
    setProducts,
    setTotalItems,
    products,
    totalItems,
  ) => {
    setSearchInputValue(value);
    if (value?.length >= 3) {
      setIsPending(true);
      fetchSearchResult(
        isNotAnAdditionalAPICall,
        value,
        setProducts,
        setTotalItems,
        setIsPending,
        0,
      );
    } else {
      if (products.length) {
        setProducts([]);
      }
      if (totalItems) {
        setTotalItems(0);
      }
    }
  },
);

export const dateDiff = (dateInMilliseconds) => {
  const currentDate = new Date();
  const date = new Date(dateInMilliseconds);

  let dateDiff = moment(currentDate).diff(date, 'years');
  if (dateDiff >= 1) {
    return `${dateDiff} ${dateDiff > 1 ? 'years' : 'year'} ago`;
  }

  dateDiff = moment(currentDate).diff(date, 'months');
  if (dateDiff >= 1) {
    return `${dateDiff} ${dateDiff > 1 ? 'months' : 'month'} ago`;
  }

  dateDiff = moment(currentDate).diff(date, 'days');

  if (dateDiff >= 1) {
    return `${dateDiff} ${dateDiff > 1 ? 'days' : 'day'} ago`;
  }

  return 'Just today';
};

export const getShopifyOrdersQueryByCustomerID = (
  customerID,
  endCursor = null,
) => {
  let after = endCursor ? `after: $cursor` : '';
  let declare = endCursor ? '($cursor: String)' : '';
  return `#grahql
    query getShopifyOrdersQueryByCustomerID${declare} {
      shopifyOrders: orders(
      first: 250
      ${after}
      query: "customer_id:${customerID}"
      ) {
         nodes {
           id
           tags
           name
           createdAt
           totalPriceSet {
             shopMoney {
               amount
               currencyCode
             }
           }
         }
         pageInfo {
           hasNextPage
           endCursor
         }
      }
    }
  `;
};

export const getIsPlantATreeOnCart = (lines) => {
  if (lines !== undefined && lines.length > 0) {
    const isPlantATreeOnCartExists = lines.filter(
      (item) => item.merchandise?.product?.id === PLANT_A_TREE_PRODUCT_ID,
    );

    if (isPlantATreeOnCartExists.length > 0) {
      return true;
    }
  }

  return false;
};

export const CART_LINE_ITEM_FIELDS = `
    warrantySKU: attribute(key: "_warranty_sku") {
      value
    }
    relatedLineId: attribute(key: "_related_line_id") {
      value
    }
    warrantyLineItemId: attribute(key: "_warranty_line_item_id") {
      value
    }
    warrantyPrice: attribute(key: "_warranty_price") {
      value
    }
    attributes {
      key
      value
    }
    id
    quantity
    cost {
       amountPerQuantity {
          amount
          currencyCode
       }
       compareAtAmountPerQuantity {
          amount
          currencyCode
       }
    }
    merchandise {
       ... on ProductVariant {
          id
          sku
          title
          price {
             amount
             currencyCode
          }
          dropShipStatus: metafield(
             namespace: "mw_fulfillment"
             key: "drop_ship_status"
          ) {
             value
          }
          compareAtPrice {
             amount
             currencyCode
          }
          selectedOptions {
            name
            value
          }
          image {
             altText
             height
             url
             id
             width
          }
          product {
             handle
             id
             title
             vendor
             options(first: 250) {
                name
                values
             }
             applicableDiscounts: metafield(
               namespace: "mw_marketing"
               key: "applicable_discounts"
            ) {
               value
            }
            shippingClass: metafield(
               namespace: "mw_fulfillment"
               key: "shipping_class"
            ) {
               value
            }
             collections(first: 250) {
                nodes {
                   handle
                }
             }
          }
          youMightAlsoNeed: metafield(
             namespace: "mw_marketing"
             key: "you_might_also_need")
          {
            value
          }
          metafields(
            identifiers: [
              {namespace: "mw_marketing", key: "you_might_also_need"}
            ]
          ) {
             references(first: 10) {
               nodes {
                 ... on ProductVariant {
                   id
                   product {
                     handle
                     title
                     featuredImage {
                       id
                       url
                       altText
                       width
                       height
                     }
                   }
                   availableForSale
                   price {
                     amount
                     currencyCode
                   }
                   compareAtPrice {
                     amount
                     currencyCode
                   }
                   sku
                   title
                 }
               }
             }
          }
       }
    }
    discountAllocations {
       ... on CartAutomaticDiscountAllocation {
          title
          discountedAmount {
             amount
             currencyCode
          }
       }
       ... on CartCodeDiscountAllocation {
          code
          discountedAmount {
             amount
             currencyCode
          }
       }
       ... on CartCustomDiscountAllocation {
          title
          discountedAmount {
            amount
            currencyCode
          }
       }
    }
`;

export const generateProductVariantQueryByIds = ({ids}) => {
  let query = '';

  ids.map((id) => {
    const variant = `variant${id.replace(/[^\d]+/, '')}`;
    query += `
      ${variant}: node(id: "${id}") {
         ... on ProductVariant {
            availableForSale
            price {
              amount
            }
            sku
         }
      }
    `;

    return id;
  });

  return query;
};

export const setCartLineData = (
  line,
  cartLineData,
  merchandiseIds,
  warrantyVariant,
) => {
  const lineAmount = line.merchandise?.price?.amount;
  const collections = line.merchandise?.product.collections.nodes;
  const warrantySKUAttribute = line?.warrantySKU?.value;

  const mattressesExists = collections.filter(
    (collection) => collection.handle === MATTRESSES,
  );
  const adjustableBaseExists = collections.filter(
    (collection) => collection.handle === ADJUSTABLE_BASE,
  );

  if (mattressesExists?.length) {
    if (lineAmount < 500) {
      cartLineData = pushDataToCartLineData(
        warrantySKUAttribute,
        'EXMATA',
        line.id,
        cartLineData,
        merchandiseIds,
        EXMATA_VARIANT_ID,
        warrantyVariant,
      );
    } else if (lineAmount >= 500 && lineAmount < 1000) {
      cartLineData = pushDataToCartLineData(
        warrantySKUAttribute,
        'EXMATB',
        line.id,
        cartLineData,
        merchandiseIds,
        EXMATB_VARIANT_ID,
        warrantyVariant,
      );
    } else {
      cartLineData = pushDataToCartLineData(
        warrantySKUAttribute,
        'EXMATC',
        line.id,
        cartLineData,
        merchandiseIds,
        EXMATC_VARIANT_ID,
        warrantyVariant,
      );
    }
  } else if (adjustableBaseExists?.length) {
    if (lineAmount < 1000) {
      cartLineData = pushDataToCartLineData(
        warrantySKUAttribute,
        'EXADJA',
        line.id,
        cartLineData,
        merchandiseIds,
        EXADJA_VARIANT_ID,
        warrantyVariant,
      );
    } else if (lineAmount >= 1000 && lineAmount < 2500) {
      cartLineData = pushDataToCartLineData(
        warrantySKUAttribute,
        'EXADJB',
        line.id,
        cartLineData,
        merchandiseIds,
        EXADJB_VARIANT_ID,
        warrantyVariant,
      );
    } else {
      cartLineData = pushDataToCartLineData(
        warrantySKUAttribute,
        'EXADJC',
        line.id,
        cartLineData,
        merchandiseIds,
        EXADJC_VARIANT_ID,
        warrantyVariant,
      );
    }
  }

  return cartLineData;
};

export const pushDataToCartLineData = (
  warrantySKUAttribute,
  compareSKU,
  lineId,
  cartLineData,
  merchandiseIds,
  warrantyVariantID,
  warrantyVariant,
) => {
  const clearVariantId = warrantyVariantID.replace(/[^\d]+/, '');
  if (merchandiseIds.indexOf(warrantyVariantID) > -1) {
    if (warrantySKUAttribute !== compareSKU) {
      cartLineData.push({
        id: lineId,
        attributes: [
          {
            key: '_warranty_sku',
            value: compareSKU,
          },
          {
            key: '_warranty_price',
            value: warrantyVariant[`variant${clearVariantId}`]?.price,
          },
        ],
      });
    }
  } else {
    if ([undefined].indexOf(warrantySKUAttribute) === -1) {
      cartLineData.push({
        id: lineId,
        attributes: [],
      });
    }
  }

  return cartLineData;
};

export const isDefaultTitle = (title) => {
  if (title && title.toLowerCase() === DEFAULT_TITLE) {
    return true;
  }

  return false;
};

export const triggerEventAfterRemoveFromCart = (sku) => {
  // console.log(`Remove SKU From Cart: ${sku}`);
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Remove From Cart',
        sku,
      },
    }),
  );
};

export const triggerEventAfterAddToCart = (sku) => {
  // console.log(`Add sku To Cart: ${sku}`);
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Add To Cart',
        sku,
      },
    }),
  );
};

export const triggerCartViewEvent = () => {
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'CartView',
      },
    }),
  );
};

export const triggerPhoneNumberEvent = (phoneNumber) => {
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Phone Number',
        value: phoneNumber,
      },
    }),
  );
};

export const triggerAddingProtectionPlanEvent = () => {
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Add Protection Plan',
      },
    }),
  );
};

export const triggerSortEvent = () => {
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Sort',
      },
    }),
  );
};

export const triggerCollectionEvent = () => {
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Collection',
      },
    }),
  );
};

export const triggerFilterEvent = () => {
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Filter',
      },
    }),
  );
};

export const triggerSearchActionEvent = (data, isSelected = false) => {
  const value = isSelected ? `Product:${data}` : `Keyword:${data}`;
  document.dispatchEvent(
    new CustomEvent('CustomPageEvent', {
      detail: {
        name: 'Search',
        value,
      },
    }),
  );
};

export const triggerCustomPageEvent = () => {
  document.dispatchEvent(new CustomEvent('NewPageEvent'));
};

export const carbonOffsetVariantQuery = () => {
  return `#grahql
    query carbonOffsetVariant($id: ID!) {
      carbonOffsetVariant: node(id: $id) {
        ... on ProductVariant {
          id
          sku
        }
      }
    }
  `;
};

export const dispatchAddToCartEvent = (currentVariant, lines) => {
  const matchingProductVariants = lines.filter(
    (item) => item?.merchandise?.id === currentVariant.id,
  );
  if (matchingProductVariants.length === 0) {
    triggerEventAfterAddToCart(currentVariant?.sku);
  }
};

export const changeCartEvent = () => {
  document.dispatchEvent(
    new CustomEvent('changeCartEvent', {
      detail: {
        name: 'changeCartEvent',
      },
    }),
  );
};

export const handleAddToCart = (
  event,
  variantId,
  variantSku,
  lines,
  linesAdd,
  linesUpdate,
  variant,
  product,
) => {
  const matchingLines = lines.filter(
    (item) => variantId === item?.merchandise?.id,
  );
  if (matchingLines?.length < 1) {
    linesAdd([
      {
        merchandiseId: variantId,
        quantity: 1,
      },
    ]);
  } else {
    let dataUpdates = [
      {
        id: matchingLines[0].id,
        quantity: matchingLines[0].quantity + 1,
        attributes: matchingLines[0].attributes,
      },
    ];

    linesUpdate(dataUpdates);
  }

  // GLR 2023-04-28 Code to send cart items to Listrak for Abandon Cart feature
  // addCartLinesToListrak(lines, product, variant);
  triggerEventAfterAddToCart(variantSku);
};

export const addCartLinesToListrak = (existingLines) => {
  (function (d) {
    if (typeof _ltk == 'undefined') {
      if (document.addEventListener)
        document.addEventListener('ltkAsyncListener', function () {
          _ltk_util.ready(d);
        });
      else {
        let e = document.documentElement;
        e.ltkAsyncProperty = 0;
        e.attachEvent('onpropertychange', function (e) {
          if (e.propertyName == 'ltkAsyncProperty') {
            _ltk_util.ready(d);
          }
        });
      }
    } else {
      _ltk_util.ready(d);
    }
  })(function () {
    /********** Begin Custom Code **********/
    let cartTotal = 0;
    existingLines.map((l) => {
      _ltk.SCA.AddItemWithLinks(
        l.merchandise.sku,
        l.quantity,
        l.cost.amountPerQuantity.amount,
        l.merchandise.product.title + ' - ' + l.merchandise.title,
        l.merchandise.image?.url,
      );
      cartTotal += parseFloat(l.cost.amountPerQuantity.amount) * l.quantity;
    });
    _ltk.SCA.Total = cartTotal.toFixed(2);
    _ltk.SCA.Submit();
    /********** End Custom Code ************/
  });
};

export const addCartLinesToKlaviyo = (existingLines, klaviyo) => {
  if (typeof klaviyo === 'undefined') {
    console.log('Added to Cart - library not loaded');
  } else if (
    typeof klaviyo === 'object' &&
    typeof klaviyo.push === 'function'
  ) {
    console.log('Added to Cart - sent');
    const items = [];
    let cartTotal = 0;
    existingLines.map((l) => {
      items.push({
        sku: l.merchandise.sku,
        quantity: l.quantity,
        amountPerQuantity: l.cost.amountPerQuantity.amount,
        title: l.merchandise.product.title + ' - ' + l.merchandise.title,
        image: l.merchandise.image?.url,
      });
      cartTotal += parseFloat(l.cost.amountPerQuantity.amount) * l.quantity;
    });
    const cart = {
      total_price: cartTotal.toFixed(2),
      $value: cartTotal.toFixed(2),
      items,
    };
    klaviyo.push(['track', 'Added to Cart', cart]);
  } else {
    console.log('Added to Cart - klaviyo.push undefined');
  }
};

export const getStorisIdFromShopifyOrder = (shopifyOrderTags) => {
  let storisId = '';
  shopifyOrderTags.map((tagName) => {
    if (tagName.toLowerCase().includes('storisid')) {
      storisId =
        tagName.toLowerCase().replace('storisid', '').replace(':', '').trim() ??
        '';
      return;
    }
  });
  return storisId;
};

export const SHOPIFY_ORDER_FIELDS = `
    name
    id
    tags
    createdAt
    paymentGatewayNames
    customer {
      id
      firstName
      lastName
      displayName
      email
      phone
      storisCustomerId: metafield(namespace: "storis", key: "customer_id") {
        value
      }
    }
    billingAddress {
      address1
      city
      name
      zip
      provinceCode
    }
    shippingAddress {
      address1
      name
      city
      zip
      provinceCode
    }
    lineItems(first: 250) {
      nodes {
        id
        product {
          id
          title
          handle
        }
        quantity
        name
        sku
        originalTotalSet {
          shopMoney {
            amount
          }
        }
      }
    }
    totalPriceSet {
      shopMoney {
        amount
        currencyCode
      }
    }
    subtotalPriceSet {
      shopMoney {
        amount
      }
    }
    totalShippingPriceSet {
      shopMoney {
        amount
      }
    }
    totalDiscountsSet {
      shopMoney {
        amount
      }
    }
    totalTaxSet {
      shopMoney {
        amount
      }
    }
`;

export const getMyOrderId = (shopifyOrder, storisOrderDetails) => {
  let myOrderId = '';
  if (shopifyOrder !== null) {
    myOrderId = `${shopifyOrder?.name}`;
  }

  if (storisOrderDetails !== null) {
    myOrderId +=
      myOrderId.length > 0
        ? `-${storisOrderDetails?.orderId}`
        : `#${storisOrderDetails?.orderId}`;
  }

  return myOrderId;
};

export const toggleChat = (event) => {
  const element = event.target.closest('.js-toggle-chat');
  if (element && element.contains(event.target)) {
    event.preventDefault();
    if (typeof Tawk_API !== 'undefined') {
      Tawk_API.toggle();
    }
  }
};

export const toggleSelectAStore = (event) => {
  const element = event.target.closest('.js-toggle-store-locator');
  if (element && element.contains(event.target)) {
    event.preventDefault();
    document.getElementById('select-a-store-trigger').click();
  }
};

export const createScriptElementForSynchrony = (isPDP = true) => {
  let syfMPPScript = document.createElement('script');
  let syfWidgetScript = document.createElement('script');
  syfMPPScript.id = 'syfMPPScript';
  syfMPPScript.type = 'text/javascript';
  syfMPPScript.src = import.meta.env.PROD ? SYF_LIVE_URL : SYF_TEST_URL;
  syfWidgetScript.id = 'syfWidgetScript';
  syfWidgetScript.innerHTML = `
      var syfWidgetObject={};
      syfWidgetObject.syfPartnerId = "${SYF_PARTNER_ID}";
      syfWidgetObject.flowType = "${isPDP ? 'PDP' : 'CART'}";
    `;

  return {syfMPPScript, syfWidgetScript};
};

export const removeSynchronyTag = (isRemove = false) => {
  if (isRemove) {
    const syncPrice = document.getElementsByClassName('sync-price');
    for (let i = 0; i < syncPrice.length; i++) {
      const element = syncPrice[i];
      element.innerHTML = '';
    }
  }

  const widgetBoxs = document.querySelectorAll('.sync-price .widget-box');
  Array.from(widgetBoxs).forEach((widget) => {
    widget.style.opacity = '0.5';
    const syfLearnHow = widget.querySelector('.syf-cta');
    if (syfLearnHow) {
      syfLearnHow.style.cursor = 'auto';
    }
  });
  const syfMPPScript = document.getElementById('syfMPPScript');
  const syfContainer = document.getElementById('syf-iframe-container');
  const syfCss = document.getElementById('syf-mpp-css');
  const dnsPrefetch = document.getElementById('dns-prefetch');
  const syfFull = document.getElementById('syf-full');
  const mppModal = document.getElementById('mpp_modal');
  const syfWidgetScript = document.getElementById('syfWidgetScript');
  const syfMPPAnalytics = document.getElementById('syfMPPAnalytics');
  const syfConfigScripts = document.querySelectorAll(
    'script[src="https://spdpone.syfpos.com/mpp/env-config.js"]',
  );
  const regexSyfWidget = /https:\/\/swidgets\.syfpos\.com\//;
  const regexAnalytics = /analytics\.synchrony\.com/;
  const styles = document.getElementsByTagName('style');

  syfConfigScripts.forEach((script) => {
    script.remove();
  });
  if (syfMPPScript) {
    syfMPPScript.remove();
  }
  if (syfContainer) {
    syfContainer.remove();
  }
  if (mppModal) {
    mppModal.remove();
  }
  if (syfMPPAnalytics) {
    syfMPPAnalytics.remove();
  }
  if (syfCss) {
    syfCss.remove();
  }
  if (dnsPrefetch) {
    dnsPrefetch.remove();
  }
  if (syfFull) {
    syfFull.remove();
  }
  if (syfWidgetScript) {
    syfWidgetScript.remove();
  }

  const syfWidgetScripts = Array.from(
    document.getElementsByTagName('script'),
  ).filter((script) => {
    return (
      script.src &&
      (regexSyfWidget.test(script.src) || regexAnalytics.test(script.src))
    );
  });

  syfWidgetScripts.forEach((script) => {
    script.remove();
  });

  Array.from(styles).forEach((style) => {
    const styleContent = style.innerHTML;
    if (
      styleContent.includes('.widget-box') ||
      styleContent.includes('#syf-promo')
    ) {
      style.parentNode.removeChild(style);
    }
  });
};

export const openCartEvent = () => {
  document.dispatchEvent(
    new CustomEvent('openCartEvent', {
      detail: {
        name: 'openCartEvent',
      },
    }),
  );
};

export const exitCartEvent = () => {
  document.dispatchEvent(
    new CustomEvent('exitCartEvent', {
      detail: {
        name: 'exitCartEvent',
      },
    }),
  );
};

// export const attachListrakListenersToFooterForm = () => {
//   (function (d) {
//     if (typeof _ltk == 'undefined') {
//       if (document.addEventListener) {
//         document.addEventListener('ltkAsyncListener', function () {
//           _ltk_util.ready(d);
//         });
//       } else {
//         let e = document.documentElement;
//         e.ltkAsyncProperty = 0;
//         e.attachEvent('onpropertychange', function (e) {
//           if (e.propertyName == 'ltkAsyncProperty') {
//             _ltk_util.ready(d);
//           }
//         });
//       }
//     } else {
//       _ltk_util.ready(d);
//     }
//   })(function () {
//     /********** Begin Custom Code **********/
//     _ltk.Signup.New(
//       'Footer',
//       'FooterNewsletterEmail',
//       _ltk.Signup.TYPE.CLICK,
//       'FooterNewsletterSubmit',
//       'email',
//     );
//     /********** End Custom Code ************/
//   });
// };

export const attachListrakListeners = () => {
  // attach listrak listener to email field
  (function (d) {
    if (typeof _ltk == 'undefined') {
      if (document.addEventListener)
        document.addEventListener('ltkAsyncListener', function () {
          _ltk_util.ready(d);
        });
      else {
        e = document.documentElement;
        e.ltkAsyncProperty = 0;
        e.attachEvent('onpropertychange', function (e) {
          if (e.propertyName == 'ltkAsyncProperty') {
            _ltk_util.ready(d);
          }
        });
      }
    } else {
      _ltk_util.ready(d);
    }
  })(function () {
    /********** Begin Custom Code **********/
    _ltk.SCA.CaptureEmail('email');
    _ltk.SCA.CaptureEmail('ltkpopup-email');
    _ltk.SCA.CaptureEmail('FooterNewsletterEmail');
    /********** End Custom Code ************/
  });
};

export const cartViewEventUtils = (event, syncPriceRef, productContentRef) => {
  if (
    event?.detail?.name === 'CartView' ||
    event?.detail?.name === 'openCartEvent'
  ) {
    if (syncPriceRef?.current) {
      syncPriceRef.current.className = 'temp-sync-price';
    }

    if (productContentRef.current) {
      productContentRef.current.setAttribute('id', 'temp-product-content');
    }
  }
};

export const getSelectedVariantFromProductVariants = (productVariants) => {
  let selectedVariant = null;
  (productVariants || []).map((variant) => {
    if (variant.availableForSale === true) {
      variant.selectedOptions.map((option) => {
        if (
          option?.name?.toLowerCase() === 'size' &&
          option?.value?.toLowerCase() === 'queen'
        ) {
          selectedVariant = variant;
        }
      });
    }
    if (selectedVariant) {
      return;
    }
  });
  return selectedVariant;
};

export const getSelectedVariantBySearchParams = (searchParams, product) => {
  let selectedVariant = null;
  product.variants.nodes.map((variant) => {
    if (variant.availableForSale === true) {
      let isMatching = true;
      let newOptions = [];
      variant.selectedOptions.map((item) => {
        if (
          searchParams.get(encodeURIComponent(item.name.toLowerCase())) ===
          encodeURIComponent(item.value.toLowerCase())
        ) {
          newOptions.push({name: item.name, value: item.value});
        } else {
          isMatching = false;
        }
      });

      if (isMatching && newOptions.length === variant.selectedOptions.length) {
        selectedVariant = variant;
      }
    }
  });

  return selectedVariant;
};

export const getVariantFromProduct = (product, search) => {
  const firstVariant =
    product?.variants?.nodes.filter(
      (item) => item.availableForSale === true,
    )[0] || product.variants.nodes[0];
  let selectedVariant = null;

  if (search) {
    const searchParams = new URLSearchParams(search);
    selectedVariant = getSelectedVariantBySearchParams(searchParams, product);

    if (selectedVariant === null) {
      selectedVariant = getSelectedVariantFromProductVariants(
        product?.variants?.nodes,
      );
    }
  } else {
    if (typeof window === 'undefined' || !window?.location?.search) {
      selectedVariant = getSelectedVariantFromProductVariants(
        product?.variants?.nodes,
      );
    } else {
      const searchParams = new URLSearchParams(window?.location?.search);
      selectedVariant = getSelectedVariantBySearchParams(searchParams, product);

      if (selectedVariant === null) {
        selectedVariant = getSelectedVariantFromProductVariants(
          product?.variants?.nodes,
        );
      }
    }
  }

  if (selectedVariant === null) {
    selectedVariant = firstVariant;
  }

  return {selectedVariant};
};

export const onChangeSelectedVariant = (variantId, product) => {
  document.dispatchEvent(
    new CustomEvent('onChangeSelectedVariant', {
      detail: {
        selectedVariantId: variantId,
        product,
      },
    }),
  );
};

export const setDefaultSizeForUrl = (selectedVariant, pathname) => {
  const reviewSort = new URLSearchParams(window.location.search);
  const clonedSearchParams = new URLSearchParams(window.location.search);
  if (reviewSort?.get('review_sort')) {
    clonedSearchParams.set('review_sort', reviewSort?.get('review_sort'));
  }

  selectedVariant.selectedOptions.map((item) => {
    clonedSearchParams.set(
      encodeURIComponent(item.name.toLowerCase()),
      encodeURIComponent(item.value.toLowerCase()),
    );
  });

  if (window !== undefined) {
    window.history.replaceState(
      null,
      '',
      `${pathname}?${clonedSearchParams.toString()}`,
    );
  }
};

export const catchChangeSelectedVariantEvent = (event, setSelectedVariant) => {
  if (event?.detail?.selectedVariantId && event?.detail?.product) {
    // const newSelectedVariant = getSelectedVariantFromProduct({
    //   product: event?.detail?.product,
    //   selectedVariantId: event?.detail?.selectedVariantId,
    // });
    // if (newSelectedVariant) {
    //   setSelectedVariant(newSelectedVariant);
    // }
  }
};

export const checkClickable = (
  selectedOptions,
  variants,
  optionName,
  optionValue,
) => {
  let currentVariant = null;

  variants.map((variant) => {
    let remainOptions = selectedOptions.filter(
      (item) => item.name !== optionName,
    );
    if (!currentVariant) {
      if (remainOptions.length === 0) {
        if (
          JSON.stringify(variant.selectedOptions) ===
          JSON.stringify([{name: optionName, value: optionValue}])
        ) {
          currentVariant = variant;
        }
      } else {
        remainOptions = [
          ...remainOptions,
          {name: optionName, value: optionValue},
        ];

        let isMatching = true;
        remainOptions.map((itemOption) => {
          const matchingOption = variant?.selectedOptions.find(
            (subOption) =>
              JSON.stringify(subOption) === JSON.stringify(itemOption),
          );

          if (!matchingOption) {
            isMatching = false;
          }
        });

        if (isMatching) {
          currentVariant = variant;
        }
      }
    }
  });

  return currentVariant?.availableForSale === true;
};

export const isCheckedOption = (optionName, optionValue, selectedVariantY) => {
  let isChecked = false;
  selectedVariantY.selectedOptions.map((selectedOption) => {
    if (
      selectedOption.name === optionName &&
      selectedOption.value === optionValue
    ) {
      isChecked = true;
    }
  });

  return isChecked;
};

export const setVariantBySelectedOptions = (
  lineItemData,
  customerGetsItems,
  productId,
) => {
  const selectedOptions = lineItemData?.merchandise?.selectedOptions || [];
  const customerGetsVariants =
    customerGetsItems[`product${productId}`]?.variants?.nodes;
  let currentVariant = null;
  let customerBuysSize = null;

  selectedOptions.map((option) => {
    if (option?.name === 'Size') {
      customerBuysSize = option?.value;
    }
  });

  (customerGetsVariants || []).map((item) => {
    let customerGetSize = null;
    (item?.selectedOptions || []).map((option) => {
      if (option?.name === 'Size') {
        customerGetSize = option?.value;
      }
    });

    if (customerBuysSize === customerGetSize && customerBuysSize) {
      currentVariant = item;
    }
  });

  if (currentVariant === null) {
    if (customerGetsVariants?.length > 0) {
      currentVariant =
        customerGetsVariants.filter(
          (item) => item.availableForSale === true,
        )[0] || customerGetsVariants[0];
    }
  }

  return currentVariant;
};

export const DEFAULT_LOCALE = Object.freeze({
  ...countries.default,
  pathPrefix: '',
});

export function getLocaleFromRequest(request) {
  const url = new URL(request.url);
  const firstPathPart =
    '/' + url.pathname.substring(1).split('/')[0].toLowerCase();

  return countries[firstPathPart]
    ? {
        ...countries[firstPathPart],
        pathPrefix: firstPathPart,
      }
    : {
        ...countries['default'],
        pathPrefix: '',
      };
}

export function removeNulls(obj) {
  let clean = Object.fromEntries(
    Object.entries(obj)
      .map(([k, v]) => [k, v === Object(v) ? removeNulls(v) : v])
      .filter(
        // ([_, v]) => v != null && (v !== Object(v) || Object.keys(v).length),
        // eslint-disable-next-line no-unused-vars
        ([_, v]) => v != null,
      ),
  );
  return Array.isArray(obj) ? Object.values(clean) : clean;
}
