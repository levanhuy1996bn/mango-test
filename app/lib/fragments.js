export const MEDIA_FRAGMENT = `#graphql
  fragment Media on Media {
    mediaContentType
    alt
    previewImage {
      url
    }
    ... on MediaImage {
      id
      image {
        url
        width
        height
      }
    }
    ... on Video {
      id
      sources {
        mimeType
        url
      }
    }
    ... on Model3d {
      id
      sources {
        mimeType
        url
      }
    }
    ... on ExternalVideo {
      id
      embedUrl
      host
    }
  }
`;

export const PRODUCT_CARD_FRAGMENT = `#graphql
  fragment ProductCard on Product {
    id
    title
    publishedAt
    handle
    variants(first: 1) {
      nodes {
        id
        image {
          url
          altText
          width
          height
        }
        price {
          amount
          currencyCode
        }
        compareAtPrice {
          amount
          currencyCode
        }
      }
    }
  }
`;

export const PRODUCT_GRID_CARD_FRAGMENT = `#graphql
  fragment ProductGridCard on Product {
    id
    title
    publishedAt
    handle
    applicableDiscounts: metafield(
      namespace: "mw_marketing"
      key: "applicable_discounts"
    ) {
      value
    }
    averageReviewScore: metafield(
      namespace: "mw_marketing"
      key: "average_review_score"
    ) {
      value
    }
    reviewCount: metafield(namespace: "mw_marketing", key: "review_count") {
      value
    }
    isBestseller: metafield(
      namespace: "mw_custom_fields"
      key: "is_bestseller"
    ) {
      value
    }
    isMWSelect: metafield(namespace: "mw_custom_fields", key: "is_mw_select") {
      value
    }
    priceRange {
      minVariantPrice {
        amount
        currencyCode
      }
    }
    compareAtPriceRange {
      minVariantPrice {
        amount
        currencyCode
      }
    }
    featuredImage {
      url
      altText
      width
      height
    }
  }
`;
