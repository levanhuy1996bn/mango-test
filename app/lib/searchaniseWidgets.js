import {
  IN_STOCK_STATUS,
  SEARCH_ANISE_API_KEY,
  SEARCH_ANISE_BASE_URL,
} from '~/lib/const';

export function searchaniseWidgets(searchInput, searchWidgets) {
  const script = getScriptForSearchaniseWidgets(searchWidgets);

  script.innerHTML = `
    Searchanise = {};
    Searchanise.host = '${SEARCH_ANISE_BASE_URL}';
    Searchanise.ApiKey = '${SEARCH_ANISE_API_KEY}';
    Searchanise.SearchInput = '#${searchInput}';

    Searchanise.AutoCmpParams = {};
    Searchanise.AutoCmpParams.restrictBy = {};
    Searchanise.AutoCmpParams.restrictBy.status = 'A';
    Searchanise.AutoCmpParams.restrictBy.stock_status = '${IN_STOCK_STATUS}';

    Searchanise.ResultsParams = {};
    Searchanise.ResultsParams.restrictBy = {};
    Searchanise.ResultsParams.restrictBy.status = 'A';
    Searchanise.ResultsParams.restrictBy.stock_status = '${IN_STOCK_STATUS}';

    Searchanise.options = {};
    Searchanise.options.PriceFormat = {
        rate : 1.0,
        symbol: '$',
        decimals: 2,
        decimals_separator: '.',
        thousands_separator: ',',
        after: false
    };

    (function() {
        var __se = document.createElement('script');
        __se.src = '${SEARCH_ANISE_BASE_URL}/widgets/v1.0/init.js';
        __se.setAttribute('async', 'true');
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(__se, s);
    })();
    `;

  return script;
}

export function getScriptForSearchaniseWidgets(searchWidgets) {
  let script = document.getElementById(searchWidgets);
  if (script === null) {
    script = document.createElement('script');
    script.id = searchWidgets;
  }

  return script;
}
