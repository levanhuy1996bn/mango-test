import {YEXT_ACCOUNT_ID, YEXT_API_KEY, YEXT_API_VER} from '~/lib/const';

export async function yextContentLocationsAPI(locId) {
  const response = await fetch(
    `https://cdn.yextapis.com/v2/accounts/${YEXT_ACCOUNT_ID}/content/locations?id=${locId}&api_key=${YEXT_API_KEY}&v=${YEXT_API_VER}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );

  if (response.ok) {
    return await response.json();
  } else {
    const jsonError = await response.json();
    console.log(`error: ${JSON.stringify(jsonError)}`);
  }
  return {};
}
