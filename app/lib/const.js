export const PAGINATION_SIZE = 8;
export const FILTER_AVAILABILITY_ID = 'filter.v.availability';
export const FILTER_VARIANT_OPTIONS_ID = 'filter.v.m.mw_variant_options';
export const DEFAULT_GRID_IMG_LOAD_EAGER_COUNT = 4;
export const SCRIPT_DATA_LAYER = 'script-data-layer';
export const ATTR_LOADING_EAGER = 'eager';
export const IN_STOCK_STATUS = '!Out Of Stock';
export const PRICE_LOW_TO_HIGH = {
  sortKey: 'PRICE',
  reverse: false,
};
export const PRICE_HIGH_TO_LOW = {
  sortKey: 'PRICE',
  reverse: true,
};
export const SORT_BY_RECOMMENDED = {
  sortKey: 'COLLECTION_DEFAULT',
  reverse: false,
};
export const INITIAL_PRODUCT_PRICE_RANGE = {
  minPrice: 0,
  maxPrice: 100000,
};

export const SORT_BY_RELEVANCE = {
  sortBy: 'relevance',
  sortOrder: 'desc',
};

export const SORT_BY_PRICE_DESC = {
  sortBy: 'price',
  sortOrder: 'desc',
};

export const SORT_BY_PRICE_ASC = {
  sortBy: 'price',
  sortOrder: 'asc',
};

export const PRODUCT_LIST_SIZE = 48;
export const SEARCH_PAGINATION_SIZE = 60;

export const DEFAULT_PRODUCT_FILTER =
  'filters: [{available:true}{price:{min:0}}]';

export function getImageLoadingPriority(
  index,
  maxEagerLoadCount = DEFAULT_GRID_IMG_LOAD_EAGER_COUNT,
) {
  return index < maxEagerLoadCount ? ATTR_LOADING_EAGER : undefined;
}

//LIVE
export const POWER_REVIEWS_READ_API_KEY =
  'd432f0a3-2679-4cce-8d2f-70178ab55e0c';
// LIVE
export const POWER_REVIEWS_WRITE_API_KEY =
  'cca1ea1d-affd-4676-93e1-2688260e151a';
// LIVE
export const POWER_REVIEWS_MERCHANT_ID = 251569;
// LIVE
export const POWER_REVIEWS_MERCHANT_GROUP_ID = 77850;

// // TEST
// export const POWER_REVIEWS_READ_API_KEY =
//   '2a92b973-953c-449d-a3f9-4cb655687831';
// // TEST
// export const POWER_REVIEWS_WRITE_API_KEY =
//   '4e4ea94d-cb3e-4d90-8f5d-970f21050a50';
// // TEST
// export const POWER_REVIEWS_MERCHANT_ID = 218689;
// // TEST
// export const POWER_REVIEWS_MERCHANT_GROUP_ID = 80589;

export const POWER_REVIEWS_BASE_URL =
  'https://readservices-b2c.powerreviews.com';

export const SEARCH_ANISE_API_KEY = '3q0v8X2Y4R';
export const SEARCH_ANISE_BASE_URL = 'https://searchserverapi.com';

export const AFFIRM_PUBLIC_API_KEY = 'H5CPRAQDHBEOEMVD';

export const MIDDLEWARE_API_BASE_URL = 'https://mw-omni.endertech.net';
export const PLANT_A_TREE_PRODUCT_ID = 'gid://shopify/Product/8184765546753';
export const PLANT_A_TREE_PRODUCT_VARIANT_ID =
  'gid://shopify/ProductVariant/44473875202305';
export const DEFAULT_TITLE = 'default title';
export const MATTRESSES = 'mattresses';
export const ADJUSTABLE_BASE = 'adjustable-beds-bases';

//if mattress < $1,000
export const EXMATA_VARIANT_ID = 'gid://shopify/ProductVariant/44473875267841';
//if mattress >=$1,000 && < $2,500
export const EXMATB_VARIANT_ID = 'gid://shopify/ProductVariant/44473875300609';
//if mattress  > $2,500
export const EXMATC_VARIANT_ID = 'gid://shopify/ProductVariant/44473875333377';
//if base < $1,000
export const EXADJA_VARIANT_ID = 'gid://shopify/ProductVariant/44473875366145';
//if base >= $1,000 && < $2,500
export const EXADJB_VARIANT_ID = 'gid://shopify/ProductVariant/44473875398913';
//if base > $2,500
export const EXADJC_VARIANT_ID = 'gid://shopify/ProductVariant/44473875431681';

export const NO_ORDERS_FOUND =
  'Your order could not be found. Please try again in a few minutes.';

export const SYF_PARTNER_ID = 'PI10000083';
// TEST
export const SYF_TEST_URL = 'https://spdpone.syfpos.com/mpp/UniFi.js';
// LIVE
export const SYF_LIVE_URL = 'https://pdpone.syfpayments.com/mpp/UniFi.js';

export const YEXT_ACCOUNT_ID = '2012273';
export const YEXT_API_KEY = '0277c3ad01c944bfc148a777fa36764d';
export const YEXT_API_VER = '20230807';
export const YEXT_API_DEFAULT_TIMEZONE = 'America/New_York';
export const YEXT_MANAGEMENT_API_KEY = '04866e11a0c51c396c21d984b3b2efc7';
export const YEXT_MANAGEMENT_API_VER = '20200202';

export const GOOGLE_GEO_API_KEY = 'AIzaSyAGrgD-RcdRlOlta7ObDyWd12Duo0750wE';
export const SIGNIFYD_URL =
  'https://cdn-scripts.signifyd.com/shopify/script-tag.js';
export const FRIEND_BUY_MERCHANT_ID = 'a5f3a094-2226-4f9d-983f-9f1fe1ef06e7';
export const KLAVIYO_PUBLIC_API_KEY = 'VP6WBr';

export const HEADER_MENU_HANDLE = 'main-menu';
export const BLOCK_PROMO_BAR_HANDLE = 'block-promo-bar';
export const BLOCK_CART_PROMO_HANDLE = 'block-cart-promo';
export const BLOCK_CART_SHIPPING_NOTICE_HANDLE = 'block-cart-shipping-notice';
export const BLOCK_MOBILE_SHIPPING_PROMO_HANDLE = 'block-mobile-shipping-promo';
export const SHOP_NAME_FALLBACK = 'Mattress Warehouse';
export const ID_SELECT_A_STORE_TRIGGER = 'select-a-store-trigger';
