import {FRIEND_BUY_MERCHANT_ID} from '~/lib/const';

export function initializeFriendBuyScript() {
  let script = document.getElementById('friendBuyScript');
  if (!script) {
    script = document.createElement('script');
    script.id = 'friendBuyScript';
    script.innerHTML = `
            window["friendbuyAPI"] = friendbuyAPI = window["friendbuyAPI"] || [];

            friendbuyAPI.merchantId = "${FRIEND_BUY_MERCHANT_ID}";
            friendbuyAPI.push(["merchant", friendbuyAPI.merchantId]);
        
            // load the merchant SDK and your campaigns
            (function(f, r, n, d, b, u, y) {
              while ((u = n.shift())) {
                (b = f.createElement(r)), (y = f.getElementsByTagName(r)[0]);
                b.async = 1;
                b.src = u;
                y.parentNode.insertBefore(b, y);
              }
            })(document, "script", [
              "https://static.fbot.me/friendbuy.js",
              "https://campaign.fbot.me/" + friendbuyAPI.merchantId + "/campaigns.js",
            ]);
        `;
  }

  return script;
}
