import {removeNulls} from '~/lib/utils';
import {InjectLandingPageFeaturedProductData} from '~/model/product/featured';

export async function getLandingPageFeaturedProductData(
  context,
  variables,
  query,
) {
  const SPACE_ID = context?.env?.PRIVATE_CONTENTFUL_SPACE_ID;
  const PUBLISHED_ACCESS_TOKEN =
    context?.env?.PRIVATE_CONTENTFUL_PUBLISHED_ACCESS_TOKEN;
  const CONTENTFUL_URL = `https://graphql.contentful.com/content/v1/spaces/${SPACE_ID}`;
  const response = await fetch(CONTENTFUL_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${PUBLISHED_ACCESS_TOKEN}`,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  });

  if (response.ok) {
    const data = await response.json();
    let newData = {...data};
    newData = removeNulls(newData);

    const {data: contentfulData} = await InjectLandingPageFeaturedProductData({
      query,
      variables,
      data: newData,
      storefront: context.storefront,
    });

    return contentfulData;
  } else {
    const data = await response.json();
    console.log(`error: ${JSON.stringify(data)}`);
  }

  return {};
}
