import {SCRIPT_DATA_LAYER} from '~/lib/const';
import {isDefaultTitle, saveAmount} from '~/lib/utils';
export function dataLayerForPages(dataLayer) {
  const script = getScriptForDataLayer();
  let pageDataLayer = dataLayer;
  if (window.dataLayer !== undefined) {
    pageDataLayer = window.dataLayer;
  }
  pageDataLayer.mwDataLayer.page = dataLayer.mwDataLayer.page;
  script.innerHTML = `
      window.dataLayer = ${JSON.stringify(pageDataLayer)}`;

  return script;
}

export function dataLayerForSearch(totalItems, searchTerm) {
  const script = getScriptForDataLayer();
  const dataLayer = {
    mwDataLayer: {
      search: {
        searchResults: totalItems,
        searchTerm,
      },
    },
  };

  let searchDataLayer = dataLayer;

  if (window.dataLayer !== undefined) {
    searchDataLayer = window.dataLayer;
  }

  searchDataLayer.mwDataLayer.search = dataLayer.mwDataLayer.search;

  script.innerHTML = `
      window.dataLayer = ${JSON.stringify(searchDataLayer)}`;

  return script;
}

export function dataLayerForAccount(customer) {
  const script = getScriptForDataLayer();
  const dataLayer = {
    mwDataLayer: {
      myAccount: {
        customerID: customer.id,
        firstName: customer.firstName,
        lastName: customer.lastName,
        optInStatus: customer.acceptsMarketing,
      },
    },
  };

  let accountDataLayer = dataLayer;

  if (window.dataLayer !== undefined) {
    accountDataLayer = window.dataLayer;
  }

  accountDataLayer.mwDataLayer.myAccount = dataLayer.mwDataLayer.myAccount;

  script.innerHTML = `
      window.dataLayer = ${JSON.stringify(accountDataLayer)}`;

  return script;
}

export function dataLayerForCollection(collection, productCount) {
  productCount =
    collection?.products?.pageInfo?.hasNextPage === false
      ? collection?.products?.nodes?.length
      : parseInt(productCount);
  const products = collection?.products?.nodes?.map((product) => {
    const minPrice = product.priceRange?.minVariantPrice;
    const minCompareAtPrice = product.compareAtPriceRange.minVariantPrice;
    const diffAmount = saveAmount(minCompareAtPrice, minPrice);
    const badges = [];
    if (product.isMWSelect?.value === 'true') {
      badges.push('MW Select');
    }
    if (product.isBestseller?.value === 'true') {
      badges.push('Best Seller');
    }
    return {
      name: product.title,
      price: minPrice?.amount,
      avgStarRating: product.avgStarRating?.value || '0.0',
      promoMessage: diffAmount > 0 ? `Save $${diffAmount} Today!` : '',
      badges,
    };
  });
  const script = getScriptForDataLayer();
  const dataLayer = {
    mwDataLayer: {
      collection: {
        name: collection.title,
        productCount,
        products,
      },
    },
  };

  let collectionDataLayer = dataLayer;

  if (window.dataLayer !== undefined) {
    collectionDataLayer = window.dataLayer;
  }

  collectionDataLayer.mwDataLayer.collection = dataLayer.mwDataLayer.collection;

  script.innerHTML = `
      window.dataLayer = ${JSON.stringify(collectionDataLayer)}`;
  return script;
}

export function dataLayerForProductDetails(product) {
  const script = getScriptForDataLayer();
  let productOptions = {};
  if (product?.options?.length > 0) {
    productOptions = setProductOptions(product.options);
  }

  let productVariants = [];
  if (product?.variants?.nodes?.length > 0) {
    product.variants.nodes.map((variant) => {
      productVariants.push({
        name: variant?.title,
        price: variant?.price?.amount,
        qty: variant?.quantityAvailable,
        sku: variant?.sku,
      });
    });
  }

  const dataLayer = {
    mwDataLayer: {
      product: {
        id: product.id,
        name: product.title,
        rating: product?.averageReviewScore?.value ?? 0,
        price: product.priceRange?.minVariantPrice?.amount ?? 0,
        compareAtPrice:
          product.compareAtPriceRange?.minVariantPrice?.amount ?? 0,
        ...productOptions,
        brand: product?.vendor,
        tags: product?.tags,
        type: product?.productType,
        variants: productVariants,
      },
    },
  };

  let productDetailsDataLayer = dataLayer;

  if (window.dataLayer !== undefined) {
    productDetailsDataLayer = window.dataLayer;
  }

  productDetailsDataLayer.mwDataLayer.product = dataLayer.mwDataLayer.product;

  script.innerHTML = `
      window.dataLayer = ${JSON.stringify(productDetailsDataLayer)};`;

  return script;
}

export function dataLayerForCart(cartData) {
  const cartLineData = cartData?.lines?.nodes ?? [];
  const cartDiscountAllocation = cartData?.discountAllocations ?? [];
  const script = getScriptForDataLayer();
  let orderDiscountAllocationAmount = 0;
  if (cartDiscountAllocation?.length > 0) {
    cartDiscountAllocation.map((discount) => {
      if (discount?.discountedAmount?.amount) {
        orderDiscountAllocationAmount += parseFloat(
          discount?.discountedAmount?.amount,
        );
      }
    });
  }
  const {cartSubtotal, estimatedTotal, savings, lineItems} = setCartData(
    cartLineData,
    cartDiscountAllocation,
  );

  const dataLayer = {
    mwDataLayer: {
      cart: {
        subTotal: cartSubtotal,
        discountTotal: parseFloat(savings.toFixed(2)),
        total: estimatedTotal,
        count: cartLineData?.length,
        discount: orderDiscountAllocationAmount,
        lineItems,
      },
    },
  };

  let cartDataLayer = dataLayer;

  if (window.dataLayer !== undefined) {
    cartDataLayer = window.dataLayer;
  }

  cartDataLayer.mwDataLayer.cart = dataLayer.mwDataLayer.cart;

  script.innerHTML = `
      window.dataLayer = ${JSON.stringify(cartDataLayer)};
      `;

  return script;
}

export function remainingDataLayer() {
  if (
    window?.dataLayer?.mwDataLayer !== undefined &&
    JSON.stringify(window?.dataLayer?.mwDataLayer) !== '{}'
  ) {
    let script = document.getElementById(SCRIPT_DATA_LAYER);
    if (script === null) {
      script = document.createElement('script');
      script.id = SCRIPT_DATA_LAYER;
    }

    script.innerHTML = `
      window.dataLayer = ${JSON.stringify(window?.dataLayer)};
      `;
    return script;
  }

  return null;
}

export function setCartData(cartLineData, cartDiscountAllocations) {
  let cartSubtotal = 0;
  let estimatedTotal = 0;
  let lineItems = [];
  cartLineData.map((item) => {
    //set order summary data
    let amountPerQuantity = parseFloat(item.cost?.amountPerQuantity?.amount);
    let compareAtAmountPerQuantity = parseFloat(
      item.cost?.compareAtAmountPerQuantity?.amount,
    );

    cartSubtotal =
      compareAtAmountPerQuantity > amountPerQuantity
        ? cartSubtotal + compareAtAmountPerQuantity * item.quantity
        : cartSubtotal + amountPerQuantity * item.quantity;

    estimatedTotal = estimatedTotal + amountPerQuantity * item.quantity;

    // set lineItems
    const product = item?.merchandise?.product;
    let lineItemName = product?.title;
    if (isDefaultTitle(item?.merchandise?.title) !== true) {
      lineItemName += ` - ${item.merchandise.title}`;
    }

    let productOptions = {};
    if (product?.options?.length > 0) {
      productOptions = setProductOptions(product.options);
    }

    let itemDiscountAmount = 0;
    if (item?.discountAllocations?.length > 0) {
      item.discountAllocations.map((discount) => {
        if (discount?.discountedAmount?.amount) {
          itemDiscountAmount += parseFloat(discount?.discountedAmount?.amount);
        }
      });
    }

    estimatedTotal = estimatedTotal - itemDiscountAmount;

    lineItems.push({
      name: lineItemName,
      sku: item?.merchandise?.sku,
      variantId: item?.merchandise?.id,
      price: item?.merchandise?.price?.amount,
      compareAtPrice: item?.merchandise?.compareAtPrice?.amount ?? 0,
      qty: item?.quantity,
      discount: JSON.stringify(itemDiscountAmount),
      parentId: item?.merchandise?.product?.id,
      ...productOptions,
    });
  });

  let discountAllocationAmount = 0;
  if (cartDiscountAllocations?.length > 0) {
    cartDiscountAllocations.map((discount) => {
      if (discount?.discountedAmount?.amount) {
        discountAllocationAmount += parseFloat(
          discount?.discountedAmount?.amount,
        );
      }
    });
  }

  estimatedTotal -= discountAllocationAmount;

  return {
    cartSubtotal,
    estimatedTotal,
    savings: cartSubtotal - estimatedTotal,
    lineItems,
  };
}

export function dataLayerForCartEmpty() {
  const script = getScriptForDataLayer();
  const dataLayer = {
    mwDataLayer: {
      cart: null,
    },
  };

  let cartDataLayer =
    window.dataLayer !== undefined ? window.dataLayer : dataLayer;

  cartDataLayer.mwDataLayer.cart = null;

  script.innerHTML = `
      window.dataLayer = ${JSON.stringify(cartDataLayer)};
      `;

  return script;
}

export function setProductOptions(options) {
  let productOptions = {};
  options.map((option, index) => {
    if (option?.values?.length > 0) {
      productOptions[`option${index + 1}Name`] = option.name;
      productOptions[`option${index + 1}Value`] = option.values[0];
    }
  });

  return productOptions;
}

export function getScriptForDataLayer() {
  let script = document.getElementById(SCRIPT_DATA_LAYER);
  if (script === null) {
    script = document.createElement('script');
    script.id = SCRIPT_DATA_LAYER;
  }

  return script;
}
