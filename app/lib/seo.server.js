import {flattenConnection} from '@shopify/hydrogen';

function root({shop, url}) {
  return {
    title: shop?.name,
    titleTemplate: '%s · Mattress Warehouse',
    description: truncate(shop?.description ?? ''),
    handle: '@shopify',
    url,
    robots: {
      noIndex: false,
      noFollow: false,
    },
    jsonLd: {
      '@context': 'https://schema.org',
      '@type': 'Organization',
      name: 'Mattress Warehouse: Best Price Guaranteed & Free Delivery',
      logo: shop.brand?.logo?.image?.url,
      sameAs: [
        'https://twitter.com/shopify',
        'https://facebook.com/shopify',
        'https://instagram.com/shopify',
        'https://youtube.com/shopify',
        'https://tiktok.com/@shopify',
      ],
      url,
    },
  };
}

function home({url}) {
  return {
    title: 'Mattress Warehouse: Best Price Guaranteed & Free Delivery',
    titleTemplate: `%s · Mattress Warehouse`,
    description:
      'Our mission is to provide quality mattresses at an affordable price. We offer a 1 year price match guarantee. Buy online or visit a store near you.',
    robots: {
      noIndex: false,
      noFollow: false,
    },
    jsonLd: [
      {
        '@context': 'https://schema.org',
        '@type': 'WebSite',
        name: 'Mattress Warehouse: Best Price Guaranteed & Free Delivery',
        url,
      },
    ],
  };
}

function productJsonLd({
  url,
  title,
  description,
  vendor,
  featuredImage,
  variants,
  averageReviewScore,
  reviewCount,
}) {
  const productSchema = {
    '@context': 'http://schema.org/',
    '@type': 'Product',
    name: title,
    description,
    brand: {
      '@type': 'Thing',
      name: vendor,
    },
    url,
  };
  if (featuredImage) {
    productSchema.image = featuredImage.url;
  }

  // customized
  // @see https://developers.google.com/search/docs/appearance/structured-data/product#single-product-page-example
  const pageUrl = url;
  const cleanProductUrl =
    -1 !== pageUrl?.indexOf('?')
      ? pageUrl?.substring(0, pageUrl?.indexOf('?'))
      : pageUrl;
  const nowPlus30Days = new Date();
  nowPlus30Days?.setDate(nowPlus30Days?.getDate() + 30);
  const nowPlus30DaysFormatted = nowPlus30Days?.toISOString()?.split('T')[0]; // YYYY-MM-DD

  productSchema.brand['@type'] = 'Brand';

  if (productSchema?.url?.indexOf('?') && !!cleanProductUrl) {
    productSchema.url = cleanProductUrl;
  }

  if (
    parseFloat(averageReviewScore?.value) > 0 &&
    parseInt(reviewCount?.value) > 0
  ) {
    if (averageReviewScore?.value) {
      productSchema.aggregateRating = {
        '@type': 'AggregateRating',
        ratingValue: averageReviewScore?.value,
      };
      if (reviewCount?.value) {
        productSchema.aggregateRating.ratingCount = reviewCount?.value;
      }
    }
  }
  // end customized

  const flattenedVariants = flattenConnection(variants ?? {}) ?? [];
  if (flattenedVariants.length) {
    const firstVariant = flattenedVariants[0];
    if (firstVariant && firstVariant.sku) {
      productSchema.sku = firstVariant.sku;
    }
    productSchema.offers = flattenedVariants.map((node) => {
      const offerSchema = {
        '@type': 'Offer',
        availability: `https://schema.org/${
          node.availableForSale ? 'InStock' : 'OutOfStock'
        }`,
        price: node.price.amount,
        priceCurrency: node.price.currencyCode,
      };
      if (node.sku) {
        offerSchema.sku = node.sku;
      }
      if (node.image && node.image.url) {
        offerSchema.image = node.image.url;
      }

      // customized
      // @see https://developers.google.com/search/docs/appearance/structured-data/product#single-product-page-example
      if (node?.displayName) {
        offerSchema.name = node?.displayName;
      } else if (node?.title) {
        offerSchema.name = title + ' - ' + node?.title;
      }
      if (node?.barcode) {
        offerSchema.gtin = node?.barcode;
      }
      if (node?.selectedOptions?.length) {
        let variantUrl = cleanProductUrl;
        node?.selectedOptions?.map((selectedOption) => {
          const name = selectedOption?.name
            ?.toLowerCase()
            ?.replace(' ', '%2520');
          const value = selectedOption?.value
            ?.toLowerCase()
            ?.replace(' ', '%2520');
          if (name) {
            variantUrl +=
              (-1 !== variantUrl?.indexOf('?') ? '&' : '?') +
              name +
              '=' +
              value;
          }
          return selectedOption;
        });
        offerSchema.url = variantUrl;
      }

      offerSchema.priceValidUntil = nowPlus30DaysFormatted;
      offerSchema.itemCondition = 'https://schema.org/NewCondition';
      // end customized

      return offerSchema;
    });
  }

  return productSchema;
}

function product(product) {
  const description = truncate(
    product?.seo?.description ?? product?.description ?? '',
  );
  return {
    title: product?.seo?.title ?? product?.title,
    description,
    price: {
      amount: '30',
      currency: 'US',
    },
    media: product?.featuredImage,
    jsonLd: productJsonLd(product),
  };
}

function collectionJsonLd({url, collection}) {
  const siteUrl = new URL(url);
  const itemListElement = collection.products.nodes.map((product, index) => {
    return {
      '@type': 'ListItem',
      position: index + 1,
      url: `/products/${product.handle}`,
    };
  });

  return [
    {
      '@context': 'https://schema.org',
      '@type': 'BreadcrumbList',
      itemListElement: [
        {
          '@type': 'ListItem',
          position: 1,
          name: 'Collections',
          item: `${siteUrl.host}/collections`,
        },
        {
          '@type': 'ListItem',
          position: 2,
          name: collection.title,
        },
      ],
    },
    {
      '@context': 'https://schema.org',
      '@type': 'CollectionPage',
      name: collection?.seo?.title ?? collection?.title ?? '',
      description: truncate(
        collection?.seo?.description ?? collection?.description ?? '',
      ),
      image: collection?.image?.url,
      url: `/collections/${collection.handle}`,
      mainEntity: {
        '@type': 'ItemList',
        itemListElement,
      },
    },
  ];
}

function collection({collection, url}) {
  return {
    title: collection?.seo?.title,
    description: truncate(
      collection?.seo?.description ?? collection?.description ?? '',
    ),
    titleTemplate: '%s | Collection',
    media: {
      type: 'image',
      url: collection?.image?.url,
      height: collection?.image?.height,
      width: collection?.image?.width,
      altText: collection?.image?.altText,
    },
    jsonLd: collectionJsonLd({collection, url}),
  };
}

function collectionsJsonLd({url, collections}) {
  const itemListElement = collections.nodes.map((collection, index) => {
    return {
      '@type': 'ListItem',
      position: index + 1,
      url: `/collections/${collection.handle}`,
    };
  });

  return {
    '@context': 'https://schema.org',
    '@type': 'CollectionPage',
    name: 'Collections',
    description: 'All collections',
    url,
    mainEntity: {
      '@type': 'ItemList',
      itemListElement,
    },
  };
}

function listCollections({collections, url}) {
  return {
    title: 'Collections',
    titleTemplate: '%s | Collections',
    description: 'All hydrogen collections',
    url,
    jsonLd: collectionsJsonLd({collections, url}),
  };
}

function article({article, url}) {
  return {
    title: article?.seo?.title ?? article?.title,
    description: truncate(article?.seo?.description ?? ''),
    titleTemplate: '%s | Journal',
    url,
    media: {
      type: 'image',
      url: article?.image?.url,
      height: article?.image?.height,
      width: article?.image?.width,
      altText: article?.image?.altText,
    },
    jsonLd: {
      '@context': 'https://schema.org',
      '@type': 'Article',
      alternativeHeadline: article.title,
      articleBody: article.contentHtml,
      datePublished: article?.publishedAt,
      description: truncate(
        article?.seo?.description || article?.excerpt || '',
      ),
      headline: article?.seo?.title || '',
      image: article?.image?.url,
      url,
    },
  };
}

function storeLocator(seoData, url) {
  return {
    title: seoData.title,
    description: seoData.description,
    titleTemplate: seoData.titleTemplate,
    url,
    jsonLd: {
      '@context': 'https://schema.org',
      '@type': 'WebPage',
      name: seoData.title,
    },
  };
}

function blog({blog, url, title = null}) {
  const titleForSeo = title
    ? title
    : blog?.seo?.title
    ? blog?.seo?.title
    : blog?.title;
  return {
    title: titleForSeo,
    description: truncate(blog?.seo?.description || ''),
    titleTemplate: '%s · Mattress Warehouse',
    url,
    jsonLd: {
      '@context': 'https://schema.org',
      '@type': 'Blog',
      name: blog?.seo?.title || blog?.title || '',
      description: blog?.seo?.description || '',
      url,
    },
  };
}

function page({page, url}) {
  return {
    description: truncate(page?.description || ''),
    title: page?.title,
    titleTemplate: '%s · Mattress Warehouse',
    url,
    jsonLd: {
      '@context': 'https://schema.org',
      '@type': 'WebPage',
      name: page.title,
    },
  };
}

function policy({policy, url}) {
  return {
    description: truncate(policy?.body ?? ''),
    title: policy?.title,
    titleTemplate: '%s | Policy',
    url,
    jsonLd: {
      '@context': 'https://schema.org',
      '@type': 'WebPage',
      name: policy.title,
    },
  };
}

function policies({policies, url}) {
  const origin = new URL(url).origin;
  const itemListElement = policies.filter(Boolean).map((policy, index) => {
    return {
      '@type': 'ListItem',
      position: index + 1,
      name: policy.title,
      item: `${origin}/policies/${policy.handle}`,
    };
  });
  return {
    title: 'Policies',
    titleTemplate: '%s | Policies',
    description: 'Mattress Warehouse Store Policies',
    jsonLd: [
      {
        '@context': 'https://schema.org',
        '@type': 'BreadcrumbList',
        itemListElement,
      },
      {
        '@context': 'https://schema.org',
        '@type': 'WebPage',
        description: 'Mattress Warehouse Store Policies',
        name: 'Policies',
        url,
      },
    ],
  };
}

export const seoPayload = {
  article,
  blog,
  collection,
  home,
  listCollections,
  page,
  policies,
  policy,
  product,
  root,
  storeLocator,
};

/**
 * Truncate a string to a given length, adding an ellipsis if it was truncated
 * @param str - The string to truncate
 * @param num - The maximum length of the string
 * @returns The truncated string
 * @example
 * ```js
 * truncate('Hello world', 5) // 'Hello...'
 * ```
 */
function truncate(str, num = 155) {
  if (typeof str !== 'string') return '';
  if (str.length <= num) {
    return str;
  }
  return str.slice(0, num - 3) + '...';
}
