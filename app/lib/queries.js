export const LANDING_PAGE_CONTAINER_QUERY = `#graphql
  query landingPageContainerEntryQuery($handle: String!) {
    landingPageContainerCollection(where: {slug: $handle}, limit: 1) {
      items {
        title
        slug
        landingPageCollection(limit: 3) {
          items {
            title
            slug
          }
        }
      }
    }
  }
`;

export const LANDING_2ND_PAGE_QUERY = `#graphql
  query landingPageEntryQuery($handle: String!) {
    landingPageCollection(where: {slug: $handle}, limit: 1) {
      items {
         title
         slug
         widgetsCollection(limit: 50) {
            items {
            ... on DecoratedHeadline {
              __typename
              title
              slug
              headline {
                json
              }
            }
            ... on CardPortrait {
              __typename
              title
              slug
              ctaUrl
              link
              image {
                url
                title
                width
                height
              }
              mobileImage {
                url
                title
                width
                height
              }
            }
            ... on ButtonGrid {
              __typename
              title
              slug
              callToActionUrl
              callToActionText
              buttonsCollection(limit: 10) {
                total
                items {
                  buttonText
                  ctaUrl
                  buttonUrl
                  slug
                }
              }
            }
            ... on ImageCarousel {
              __typename
              title
              slug
              slidesCollection(limit: 10) {
                items {
                  title
                  slug
                  imagesCollection {
                    items {
                      title
                      slug
                      image {
                        url
                        title
                        width
                        height
                      }
                      mobileImage {
                        url
                        title
                        width
                        height
                      }
                      ctaUrl
                      link
                      mobileOnly
                    }
                  }
                }
              }
            }
            ... on FeaturedProducts {
              __typename
              title
              slug
              headline {
                json
              }
              layoutStyle
              showPrices
              showPromoBadges
              showRatingSummaries
              ctaUrl
              callToActionUrl
              callToActionText
              products
            }
            ... on PromoStack {
              __typename
              title
              slug
              promoCardsCollection(limit: 10) {
                total
                items {
                  title
                  slug
                  description
                  headline
                  subheadline
                  image {
                    title
                    url
                    width
                    height
                  }
                  brandLogo {
                    title
                    url
                    width
                    height
                  }
                  card1CtaUrl
                  cta1Url
                  cta1Text
                  card2CtaUrl
                  cta2Url
                  cta2Text
                }
              }
            }
            ... on Slideshow {
              __typename
              title
              slug
              slidesCollection(limit: 10) {
                total
                items {
                  title
                  slug
                  foregroundAlignment
                  foregroundHtmlBlock {
                    json
                  }
                  foregroundImage {
                    title
                    url
                    width
                    height
                  }
                  backgroundImageMobile {
                    title
                    url
                    width
                    height
                  }
                  backgroundImageDesktop {
                    title
                    url
                    width
                    height
                  }
                }
              }
            }
            ... on BannerSingleCta {
              __typename
              title
              slug
              backgroundImageMobile {
                title
                url
                width
                height
              }
              backgroundImageDesktop {
                title
                url
                width
                height
              }
              foregroundImage {
                title
                url
                width
                height
              }
              ctaButtonLabel
              inverseCtaButtonColors
              ctaUrl
              link
            }
            ... on BannerCarousel {
              __typename
              title
              slug
              bannersCollection(limit: 10) {
                items {
                  __typename
                  title
                  slug
                  backgroundImageMobile {
                    title
                    url
                    width
                    height
                  }
                  backgroundImageDesktop {
                    title
                    url
                    width
                    height
                  }
                  foregroundImage {
                    title
                    url
                    width
                    height
                  }
                  ctaButtonLabel
                  inverseCtaButtonColors
                  link
                }
              }
            }
            }
         }
      }
    }
  }
`;

export const LANDING_FIRST_PAGE_QUERY = `#graphql
  query landingPageEntryQuery($handle: String!) {
    landingPageCollection(where: {slug: $handle}, limit: 1) {
      items {
        title
        slug
        widgetsCollection(limit: 50) {
          items {
            ... on Row {
              __typename
              title
              slug
              widgetsCollection(limit: 10) {
                items {
                  ... on PromoStack {
                    __typename
                    title
                    slug
                    promoCardsCollection(limit: 10) {
                      total
                      items {
                        title
                        slug
                        description
                        headline
                        subheadline
                        image {
                          title
                          url
                          width
                          height
                        }
                        brandLogo {
                          title
                          url
                          width
                          height
                        }
                        card1CtaUrl
                        cta1Url
                        cta1Text
                        card2CtaUrl
                        cta2Url
                        cta2Text
                      }
                    }
                  }
                  ... on Slideshow {
                    __typename
                    title
                    slug
                    slidesCollection(limit: 10) {
                      total
                      items {
                        title
                        slug
                        foregroundAlignment
                        foregroundHtmlBlock {
                          json
                        }
                        foregroundImage {
                          title
                          url
                          width
                          height
                        }
                        backgroundImageMobile {
                          title
                          url
                          width
                          height
                        }
                        backgroundImageDesktop {
                          title
                          url
                          width
                          height
                        }
                      }
                    }
                  }
                }
              }
            }
            ... on TwoPromoCardsRow {
              __typename
              title
              slug
              promoCardEntriesCollection(limit: 2) {
                total
                items {
                  title
                  slug
                  description
                  headline
                  subheadline
                  image {
                    title
                    url
                    width
                    height
                  }
                  brandLogo {
                    title
                    url
                    width
                    height
                  }
                  card1CtaUrl
                  cta1Url
                  cta1Text
                  card2CtaUrl
                  cta2Url
                  cta2Text
                }
              }
            }
            ... on VideoBanner {
              __typename
              title
              slug
              video {
                title
                url
                width
                height
              }
              poster {
                title
                url
                width
                height
              }
              ctaUrl
              link
            }
            ... on MultiCtaWithCards {
              __typename
              title
              slug
              backgroundImageMobile {
                title
                url
                width
                height
              }
              backgroundImageDesktop {
                title
                url
                width
                height
              }
              badgeImageMobile {
                title
                url
                width
                height
              }
              badgeImageDesktop {
                title
                url
                width
                height
              }
              badgeCtaUrl
              badgeLink
              ctaOneImage {
                title
                url
                width
                height
              }
              card1CtaUrl
              ctaOneLink
              ctaTwoImage {
                title
                url
                width
                height
              }
              card2CtaUrl
              ctaTwoLink
              ctaThreeImage {
                title
                url
                width
                height
              }
              card3CtaUrl
              ctaThreeLink
            }
            ... on TwoGrid {
              __typename
              title
              slug
              sectionTitle
              banner1Desktop {
                title
                url
                width
                height
              }
              banner1Mobile {
                title
                url
                width
                height
              }
              banner1CtaUrl
              banner2Desktop {
                title
                url
                width
                height
              }
              banner2Mobile {
                title
                url
                width
                height
              }
              banner2CtaUrl
            }
            ... on FourGrid {
              __typename
              title
              slug
              sectionTitle
              banner1Desktop {
                title
                url
                width
                height
              }
              banner1Mobile {
                title
                url
                width
                height
              }
              banner1CtaUrl
              banner2Desktop {
                title
                url
                width
                height
              }
              banner2Mobile {
                title
                url
                width
                height
              }
              banner2CtaUrl
              banner3Desktop {
                title
                url
                width
                height
              }
              banner3Mobile {
                title
                url
                width
                height
              }
              banner3CtaUrl
              banner4Desktop {
                title
                url
                width
                height
              }
              banner4Mobile {
                title
                url
                width
                height
              }
              banner4CtaUrl
            }
          }
        }
      }
    }
  }
`;
