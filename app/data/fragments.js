export const MEDIA_FRAGMENT = `#graphql
  fragment Media on Media {
    __typename
    mediaContentType
    alt
    previewImage {
      url
    }
    ... on MediaImage {
      id
      image {
        url
        width
        height
      }
    }
    ... on Video {
      id
      sources {
        mimeType
        url
      }
    }
    ... on Model3d {
      id
      sources {
        mimeType
        url
      }
    }
    ... on ExternalVideo {
      id
      embedUrl
      host
    }
  }
`;

export const PRODUCT_GRID_CARD_FRAGMENT = `#graphql
  fragment ProductGridCard on Product {
    id
    title
    publishedAt
    handle
    applicableDiscounts: metafield(
      namespace: "mw_marketing"
      key: "applicable_discounts"
    ) {
      value
    }
    averageReviewScore: metafield(
      namespace: "mw_marketing"
      key: "average_review_score"
    ) {
      value
    }
    reviewCount: metafield(namespace: "mw_marketing", key: "review_count") {
      value
    }
    isBestseller: metafield(
      namespace: "mw_custom_fields"
      key: "is_bestseller"
    ) {
      value
    }
    isMWSelect: metafield(namespace: "mw_custom_fields", key: "is_mw_select") {
      value
    }
    priceRange {
      minVariantPrice {
        amount
        currencyCode
      }
    }
    compareAtPriceRange {
      minVariantPrice {
        amount
        currencyCode
      }
    }
    featuredImage {
      url
      altText
      width
      height
    }
  }
`;

export const PRODUCT_CARD_FRAGMENT = `#graphql
  fragment ProductCard on Product {
    id
    title
    publishedAt
    handle
    variants(first: 1) {
      nodes {
        id
        image {
          url
          altText
          width
          height
        }
        price {
          amount
          currencyCode
        }
        compareAtPrice {
          amount
          currencyCode
        }
        selectedOptions {
          name
          value
        }
        product {
          handle
          title
        }
      }
    }
  }
`;

export const DRAFT_ORDER_FRAGMENT = `#graphql
  fragment DraftOrderFragment on DraftOrder {
    id
    name
    status
    updatedAt
    tags
    totalPriceSet {
      shopMoney {
        amount
        currencyCode
      }
    }
    purchasingEntity {
      ... on Customer {
        id
        firstName
        lastName
      }
    }
    lineItemsSubtotalPrice {
      shopMoney {
        amount
        currencyCode
      }
    }
    lineItems(first: 70) {
      nodes {
        id
        appliedDiscount {
          value
          valueType
          description
          title
        }
        customAttributes {
          key
          value
        }
        variant {
          id
          sku
          title
          price
          compareAtPrice
          discountable: metafield(namespace: "mw_marketing", key: "discountable") {
            value
          }
          minimumSellingPrice: metafield(namespace: "mw_marketing", key: "minimum_selling_price") {
            value
          }
        }
        product {
          id
          title
          handle
          inMattresses: inCollection(id:$mattressesId)
          inAdjustableBedsBases: inCollection(id:$adjustableBedsBasesId)
          applicableDiscounts: metafield(
            namespace: "mw_marketing"
            key: "applicable_discounts"
          ) {
            value
          }
        }
        discountedTotalSet {
          shopMoney {
            amount
            currencyCode
          }
        }
        discountedUnitPriceSet {
          shopMoney {
            amount
            currencyCode
          }
        }
        originalTotalSet {
          shopMoney {
            amount
            currencyCode
          }
        }
        id
        sku
        image {
          id
          height
          width
          url
        }
        quantity
        totalDiscountSet {
          shopMoney {
            amount
            currencyCode
          }
        }
      }
    }
  }
`;

export const MODIFY_DRAFT_ORDER_FRAGMENT = `#graphql
  fragment DraftOrderFragment on DraftOrder {
      id
      name
      lineItems(first: 100) {
        nodes {
          id
          variant {
            id
            price
          }
          quantity
        }
      }
  }
`;
