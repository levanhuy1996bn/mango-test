import {
  Links,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  isRouteErrorResponse,
  useLoaderData,
  useMatches,
  useRouteError,
} from '@remix-run/react';
import {Seo, ShopifySalesChannel} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {
  FriendBuyScript,
  GenericError,
  Layout,
  NotFound,
  ScriptLauncher,
} from '~/components';
import {useAnalytics} from '~/hooks/useAnalytics';
import {PRODUCT_CARD_FRAGMENT} from '~/lib';
import {
  BLOCK_CART_PROMO_HANDLE,
  BLOCK_CART_SHIPPING_NOTICE_HANDLE,
  BLOCK_MOBILE_SHIPPING_PROMO_HANDLE,
  BLOCK_PROMO_BAR_HANDLE,
  EXADJA_VARIANT_ID,
  EXADJB_VARIANT_ID,
  EXADJC_VARIANT_ID,
  EXMATA_VARIANT_ID,
  EXMATB_VARIANT_ID,
  EXMATC_VARIANT_ID,
  HEADER_MENU_HANDLE,
  PLANT_A_TREE_PRODUCT_VARIANT_ID,
  SHOP_NAME_FALLBACK,
} from '~/lib/const';
import {seoPayload} from '~/lib/seo.server';
import {
  DEFAULT_LOCALE,
  carbonOffsetVariantQuery,
  generateProductVariantQueryByIds,
  parseMenu,
} from '~/lib/utils';
import getMenuItemPageMetafieldsNavigationImages from '~/model/menu/menuItems/navigationImages';
import getMenuItemResourceData from '~/model/menu/menuItems/resources';
import invariant from 'tiny-invariant';

import appleTouchIconPrecomposed from '../public/apple-touch-icon-precomposed.png';
import appleTouchIcon from '../public/apple-touch-icon.png';
import favicon from '../public/favicon.svg';
import safariPinnedTab from '../public/safari-pinned-tab.svg';
import styles from './styles/app.css';

export const links = () => {
  return [
    {rel: 'stylesheet', href: styles},
    {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap',
    },
    {
      rel: 'preconnect',
      href: 'https://fonts.googleapis.com',
    },
    {
      rel: 'preconnect',
      href: 'https://cdn.shopify.com',
    },
    {
      rel: 'preconnect',
      href: 'https://checkout.mattresswarehouse.com',
    },
    {
      rel: 'preconnect',
      href: 'https://mattresswarehouse.com',
    },
    {
      rel: 'preconnect',
      href: 'https://fonts.gstatic.com',
    },
    {rel: 'apple-touch-icon', sizes: '180x180', href: appleTouchIcon},
    {
      rel: 'apple-touch-icon-precomposed',
      sizes: '180x180',
      href: appleTouchIconPrecomposed,
    },
    {rel: 'shortcut icon', type: 'image/svg+xml', href: favicon},
    {rel: 'manifest', href: '/site.webmanifest'},
    {rel: 'mask-icon', href: safariPinnedTab, color: '#e32329'},
  ];
};

export async function loader({request, context}) {
  const headers = new Headers();
  const {session} = context;
  const nearestLocations = await session.get('nearestLocations');
  const nearestStoreLocations = await session.get('nearestStoreLocations');
  const userLocation = await session.get('userLocation');
  const customerAccessToken = await session.get('customerAccessToken');
  const [shopData] = await Promise.all([getLayoutData(context)]);

  let isProd;

  if (typeof Oxygen !== 'undefined' && Oxygen?.env?.IS_PROD) {
    isProd = Oxygen?.env?.IS_PROD;
  } else if (typeof MiniOxygen !== 'undefined' && MiniOxygen?.env?.IS_PROD) {
    isProd = MiniOxygen?.env?.IS_PROD;
  } else {
    isProd = context?.env?.IS_PROD;
  }

  const PRODUCT_VARIANT_QUERY = `#graphql
    query warrantyVariant {
      ${generateProductVariantQueryByIds({
        ids: [
          EXMATA_VARIANT_ID,
          EXMATB_VARIANT_ID,
          EXMATC_VARIANT_ID,
          EXADJA_VARIANT_ID,
          EXADJB_VARIANT_ID,
          EXADJC_VARIANT_ID,
        ],
      })}
    }
  `;

  const {carbonOffsetVariant} = await getCarbonOffsetVariant(
    context?.storefront,
  );

  let LOCATION_DETAILS_QUERY = '';
  let data = {};
  let nearestStoreLocationsWithDetails = {};

  if (nearestLocations?.length > 0) {
    LOCATION_DETAILS_QUERY = `#graphql
      query locationsByIds {
        locations: shop {
          ${(nearestLocations || []).reduce((full, locationId) => {
            return `${full}
              locationId${locationId}: metafield(namespace: "location_details", key: "${locationId}") {
                value
              }`;
          }, '')}
        }
      }`;

    data = await context.storefront.query(LOCATION_DETAILS_QUERY, {
      variables: {},
      cache: context?.storefront.CacheLong(),
    });
    nearestStoreLocationsWithDetails = data?.locations ? data.locations : {};
    Object.entries(nearestStoreLocationsWithDetails || {}).map((loc) => {
      const locId =
        loc[0]?.indexOf('locationId') > -1
          ? loc[0]?.replace('locationId', '')
          : '00';

      const locInfo = JSON.parse(loc[1]?.value ?? '{}') || {};
      nearestStoreLocations[locId] = {
        ...nearestStoreLocations[locId],
        ...locInfo,
      };
    });
  }

  const featuredData = await context.storefront.query(NOT_FOUND_QUERY, {
    variables: {
      language: context.storefront.i18n.language,
      country: context.storefront.i18n.country,
    },
    cache: context.storefront.CacheLong(),
  });

  const {featuredCollections, featuredProducts} = featuredData;
  const seo = seoPayload.root({shop: shopData.shop, url: request.url});

  return defer(
    {
      isProd: isProd === '1',
      selectedLocale: context.storefront.i18n,
      analytics: {
        shopifySalesChannel: ShopifySalesChannel.hydrogen,
        shopId: shopData?.shopId,
        shopify: {
          isLoggedIn: !!customerAccessToken,
        },
      },
      shopData,
      seo,
      featuredProducts,
      featuredCollections,
      userLocation,
      nearestStoreLocations,
      nearestLocations,
      customerAccessToken,
      warrantyVariant: await getWarrantyVariant(
        context?.storefront,
        PRODUCT_VARIANT_QUERY,
      ),
      carbonOffsetVariant,
    },
    {headers},
  );
}

export default function App() {
  const data = useLoaderData();
  const locale = data.selectedLocale ?? DEFAULT_LOCALE;
  const hasUserConsent = true;
  useAnalytics(hasUserConsent, locale);

  return (
    <html lang={locale.language}>
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
        <script
          dangerouslySetInnerHTML={{
            __html: `;(function(win, doc, style, timeout) { var STYLE_ID = 'at-body-style'; function getParent() { return doc.getElementsByTagName('head')[0]; } function addStyle(parent, id, def) { if (!parent) {      return; } var style = doc.createElement('style'); style.id = id; style.innerHTML = def; parent.appendChild(style); } function removeStyle(parent, id) { if (!parent) { return; } var style = doc.getElementById(id); if (!style) { return; } parent.removeChild(style); } addStyle(getParent(), STYLE_ID, style); setTimeout(function() { removeStyle(getParent(), STYLE_ID); }, timeout); }(window, document, "body {opacity: 0 !important}", 3000));`,
          }}
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `(g=>{var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=\`https://maps.googleapis.com/maps/api/js?\`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
              key: "AIzaSyAGrgD-RcdRlOlta7ObDyWd12Duo0750wE",
              v: "weekly",
            });`,
          }}
        />
        <Seo />
        <Meta />
        <Links />
      </head>
      <body className="overflow-x-hidden">
        <Layout
          carbonOffsetVariant={data?.carbonOffsetVariant}
          customerAccessToken={data?.customerAccessToken}
          nearestStoreLocations={data?.nearestStoreLocations}
          warrantyVariant={data?.warrantyVariant}
          userLocation={data?.userLocation}
          shopData={data?.shopData}
        >
          <Outlet />
        </Layout>
        <ScrollRestoration />
        <Scripts />
        {data?.isProd === true && <ScriptLauncher />}
        <FriendBuyScript />
      </body>
    </html>
  );
}

export function ErrorBoundary({error}) {
  const [root] = useMatches();
  const locale = root?.data?.selectedLocale ?? DEFAULT_LOCALE;
  const routeError = useRouteError();
  const isRouteError = isRouteErrorResponse(routeError);

  let title = 'Error';
  let pageType = 'page';

  if (isRouteError) {
    title = 'Not found';
    if (routeError.status === 404) pageType = routeError.data || pageType;
  }

  return (
    <html lang={locale.language}>
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <title>{title}</title>
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
        <script
          dangerouslySetInnerHTML={{
            __html: `;(function(win, doc, style, timeout) { var STYLE_ID = 'at-body-style'; function getParent() { return doc.getElementsByTagName('head')[0]; } function addStyle(parent, id, def) { if (!parent) {      return; } var style = doc.createElement('style'); style.id = id; style.innerHTML = def; parent.appendChild(style); } function removeStyle(parent, id) { if (!parent) { return; } var style = doc.getElementById(id); if (!style) { return; } parent.removeChild(style); } addStyle(getParent(), STYLE_ID, style); setTimeout(function() { removeStyle(getParent(), STYLE_ID); }, timeout); }(window, document, "body {opacity: 0 !important}", 3000));`,
          }}
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `(g=>{var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=\`https://maps.googleapis.com/maps/api/js?\`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
              key: "AIzaSyAGrgD-RcdRlOlta7ObDyWd12Duo0750wE",
              v: "weekly",
            });`,
          }}
        />
        <Meta />
        <Links />
      </head>
      <body className="overflow-x-hidden">
        <Layout
          carbonOffsetVariant={root?.data?.carbonOffsetVariant}
          customerAccessToken={root?.data?.customerAccessToken}
          nearestStoreLocations={root?.data?.nearestStoreLocations}
          warrantyVariant={root?.data?.warrantyVariant}
          userLocation={root?.data?.userLocation}
          shopData={root?.data?.shopData}
        >
          {isRouteError ? (
            <>
              {routeError.status === 404 ? (
                <NotFound type={pageType} />
              ) : (
                <GenericError
                  error={{message: `${routeError.status} ${routeError.data}`}}
                />
              )}
            </>
          ) : (
            <GenericError error={error instanceof Error ? error : undefined} />
          )}
        </Layout>
        <ScrollRestoration />
        <Scripts />
        {root?.data?.isProd === true && <ScriptLauncher />}
        <FriendBuyScript />
      </body>
    </html>
  );
}

export async function getWarrantyVariant(storefront, query) {
  invariant(storefront, 'missing storefront client in cart query');

  return await storefront.query(query, {
    variables: {},
    cache: storefront.CacheLong(),
  });
}

export async function getCarbonOffsetVariant(storefront) {
  invariant(storefront, 'missing storefront client in cart query');

  return await storefront.query(carbonOffsetVariantQuery(), {
    variables: {id: PLANT_A_TREE_PRODUCT_VARIANT_ID},
    cache: storefront.CacheLong(),
  });
}

async function getLayoutData({storefront}) {
  const data = await storefront.query(SHOP_QUERY, {
    variables: {
      language: storefront.i18n.language,
      headerMenuHandle: HEADER_MENU_HANDLE,
      blockPromoBarHandle: BLOCK_PROMO_BAR_HANDLE,
      blockCartPromoHandle: BLOCK_CART_PROMO_HANDLE,
      blockCartShippingNoticeHandle: BLOCK_CART_SHIPPING_NOTICE_HANDLE,
      blockMobileShippingPromoHandle: BLOCK_MOBILE_SHIPPING_PROMO_HANDLE,
    },
    cache: storefront.CacheLong(),
  });

  invariant(data, 'No data returned from Shopify API');

  const shopName = data ? data.shop.name : SHOP_NAME_FALLBACK;

  const customPrefixes = {};

  const headerMenu = data?.headerMenu
    ? parseMenu(data.headerMenu, customPrefixes)
    : undefined;

  const getMenuResourceIds = (menu, resourceIds = []) => {
    if (menu?.items) {
      menu?.items?.map((item) => {
        if (item?.resourceId && -1 === resourceIds.indexOf(item?.resourceId)) {
          resourceIds.push(item?.resourceId);
        }

        if (item?.items) {
          resourceIds = getMenuResourceIds(item, resourceIds);
        }

        return item;
      });
    }

    return resourceIds;
  };

  const updateMenuResourceData = (menu, resourceData = {}) => {
    if (menu?.items) {
      menu?.items?.map((item) => {
        if (item?.resourceId) {
          item.resourceData = resourceData[item?.resourceId] || {};
        }

        if (item?.items) {
          item = updateMenuResourceData(item, resourceData);
        }

        return item;
      });
    }

    return menu;
  };

  const updateMenuResourceDataWithPageMetafields = (
    menu,
    resourceData = {},
  ) => {
    if (menu?.items) {
      menu?.items?.map((item) => {
        if (
          item?.resourceId &&
          !item?.resourceData?.image && // prevent overwrite
          resourceData[item?.resourceId]?.image
        ) {
          item.resourceData = resourceData[item?.resourceId] || {};
        }

        if (item?.items) {
          item = updateMenuResourceDataWithPageMetafields(item, resourceData);
        }
        return item;
      });
    }
    return menu;
  };

  const headerMenuResourceIds = getMenuResourceIds(headerMenu);

  const headerMenuResourceData = getMenuItemResourceData({
    resourceIds: headerMenuResourceIds,
    storefront,
  });

  const headerMenuPageMetafieldsNavigationImages =
    getMenuItemPageMetafieldsNavigationImages({
      resourceIds: headerMenuResourceIds,
      storefront,
    });

  let updatedHeaderMenu = {};
  updatedHeaderMenu = updateMenuResourceData(
    headerMenu,
    headerMenuResourceData,
  );

  updatedHeaderMenu = updateMenuResourceDataWithPageMetafields(
    updatedHeaderMenu,
    headerMenuPageMetafieldsNavigationImages,
  );

  const blockPromoBar = data?.blockPromoBar || undefined;
  const blockCartPromo = data?.blockCartPromo || undefined;
  const blockCartShippingNotice = data?.blockCartShippingNotice || undefined;
  const blockMobileShippingPromo = data?.blockMobileShippingPromo || undefined;

  return {
    blockPromoBar,
    headerMenu: updatedHeaderMenu,
    shopName,
    shop: data.shop,
    shopId: data?.shop?.id,
    blockCartPromo,
    blockCartShippingNotice,
    blockMobileShippingPromo,
  };
}

const SHOP_QUERY = `#graphql
  fragment MenuItem on MenuItem {
    id
    resourceId
    tags
    title
    type
    url
  }
  query layoutMenus(
    $language: LanguageCode
    $headerMenuHandle: String!
    $blockPromoBarHandle: String!
    $blockCartPromoHandle: String!
    $blockCartShippingNoticeHandle: String!
    $blockMobileShippingPromoHandle: String!
  ) @inContext(language: $language) {
    shop {
      id
      name
      description
      primaryDomain {
        url
      }
      brand {
       logo {
         image {
          url
         }
       }
     }
    }
    headerMenu: menu(handle: $headerMenuHandle) {
      id
      items {
        ...MenuItem
        items {
          ...MenuItem
          items {
            ...MenuItem
          }
        }
      }
    }
    blockPromoBar: page(handle: $blockPromoBarHandle) {
      body
    }
    blockCartPromo: page(handle: $blockCartPromoHandle) {
      body
    }
    blockCartShippingNotice: page(handle: $blockCartShippingNoticeHandle) {
      body
    }
    blockMobileShippingPromo: page(handle: $blockMobileShippingPromoHandle) {
      body
    }
  }
`;

const NOT_FOUND_QUERY = `#graphql
  ${PRODUCT_CARD_FRAGMENT}
  query homepage($country: CountryCode, $language: LanguageCode)
  @inContext(country: $country, language: $language) {
    featuredCollections: collections(first: 3, sortKey: UPDATED_AT) {
      nodes {
        id
        title
        handle
        image {
          altText
          width
          height
          url
        }
      }
    }
    featuredProducts: products(first: 12) {
      nodes {
        ...ProductCard
      }
    }
  }
`;
