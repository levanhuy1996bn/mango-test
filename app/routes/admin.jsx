import {redirect} from '@shopify/remix-oxygen';

export async function loader({context}) {
  const data = await context.storefront.query(SHOP_QUERY, {
    cache: context.storefront.CacheLong(),
    variables: {},
  });

  const {url} = data.shop.primaryDomain;
  return redirect(`${url}/admin`);
}

const SHOP_QUERY = `#graphql
  query {
    shop {
      primaryDomain {
        url
      }
    }
  }
`;
