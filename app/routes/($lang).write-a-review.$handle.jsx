import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {DataLayer, WriteAReview} from '~/components';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';
export async function loader({context, request, params}) {
  const {handle} = params;
  const {storefront} = context;
  const data = await storefront.query(PRODUCT_QUERY, {
    variables: {
      country: storefront.i18n.country,
      language: storefront.i18n.language,
      handle,
    },
  });

  if (!data?.product) {
    throw new Response('product', {status: 404});
  }

  const {product} = data;

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: `Write a review for ${product.title}`,
        slug: handle,
        pageType: 'write-a-review',
      },
    },
  };

  const seo = seoPayload.product({...product, url: request.url});

  return defer({
    analytics: {
      shopify: {
        resourceId: data?.product?.id,
        pageType: AnalyticsPageType.product,
      },
    },
    seo,
    product: data?.product,
    dataLayer,
  });
}

export default function Review() {
  const data = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
      <Suspense>
        <WriteAReview product={data?.product} pageID={data?.product?.handle} />
      </Suspense>
    </div>
  );
}

const PRODUCT_QUERY = `#graphql
  query Product(
    $country: CountryCode
    $language: LanguageCode
    $handle: String!
  ) @inContext(country: $country, language: $language) {
    product(handle: $handle) {
      id
      title
      handle
      featuredImage {
        url
        altText
        width
        height
      }
    }
  }
`;
