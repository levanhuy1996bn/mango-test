import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType, flattenConnection} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {ArticleCard, BlogPagination, DataLayer, PageHeader} from '~/components';
import {CACHE_SHORT, routeHeaders} from '~/data/cache';
import {PAGINATION_SIZE, getImageLoadingPriority} from '~/lib/const';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';

export const headers = routeHeaders;

export async function loader({context, request, params}) {
  const {storefront} = context;
  const {blogHandle} = params;
  const {language, country} = storefront.i18n;
  const searchParams = new URL(request.url).searchParams;

  const startCursor = searchParams?.get('startCursor');
  const endCursor = searchParams?.get('endCursor');

  let firstPageBy = startCursor ? undefined : PAGINATION_SIZE;
  let lastPageBy = endCursor ? undefined : PAGINATION_SIZE;
  if (firstPageBy && lastPageBy) {
    // only one can be defined at a time, unset both to allow the next check to define the firstPageBy
    firstPageBy = undefined;
    lastPageBy = undefined;
  }
  if (!firstPageBy && !lastPageBy) {
    firstPageBy = PAGINATION_SIZE;
    lastPageBy = undefined;
  }

  const data = await storefront.query(BLOG_QUERY, {
    variables: {
      language,
      blogHandle,
      pageBy: firstPageBy,
      lastPageBy,
      startCursor,
      endCursor,
    },
  });

  if (!data?.blog) {
    throw new Response(null, {status: 404});
  }

  const rawArticles = flattenConnection(data?.blog?.articles || {}) ?? [];

  const articles = rawArticles.map((article) => {
    const {publishedAt} = article;
    return {
      ...article,
      publishedAt: new Intl.DateTimeFormat(`${language}-${country}`, {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
      }).format(new Date(publishedAt)),
    };
  });

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: data?.blog?.title,
        slug: blogHandle,
        pageType: 'blogs',
      },
    },
  };

  const seo = seoPayload.blog({blog: data?.blog, url: request.url});

  return defer(
    {
      analytics: {
        shopify: {
          pageType: AnalyticsPageType.page,
        },
      },
      seo,
      dataLayer,
      blog: data?.blog,
      articles,
      blogHandle,
    },
    {
      headers: {
        'Cache-Control': CACHE_SHORT,
      },
    },
  );
}

export default function Blog() {
  const {dataLayer, blog, articles, blogHandle} = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={dataLayer} />
      </Suspense>
      <Suspense>
        <div>
          <section className="max-w-page-width mx-auto">
            <PageHeader heading={blog?.title} className="gap-0">
              <Suspense>
                <BlogsGrid
                  blogHandle={blogHandle}
                  articles={articles}
                  blog={blog}
                />
              </Suspense>
            </PageHeader>
          </section>
        </div>
      </Suspense>
    </div>
  );
}

function BlogsGrid({blogHandle, articles, blog}) {
  if (articles.length === 0) {
    return <p>No articles found</p>;
  }

  return (
    <div className="flex flex-col gap-8">
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-5">
        {articles.map((article, i) => {
          return (
            <ArticleCard
              blogHandle={'blogs/' + blogHandle?.toLowerCase()}
              article={article}
              key={article.id}
              loading={getImageLoadingPriority(i, 2)}
            />
          );
        })}
      </div>
      <BlogPagination
        blogHandle={blogHandle}
        pageInfo={blog?.articles?.pageInfo}
      />
    </div>
  );
}

const BLOG_QUERY = `#graphql
  query Blog(
    $language: LanguageCode
    $blogHandle: String!
    $pageBy: Int
    $lastPageBy: Int
    $startCursor: String
    $endCursor: String
  ) @inContext(language: $language) {
    blog(handle: $blogHandle) {
      title
      seo {
        title
        description
      }
      articles(
        first: $pageBy
        after: $endCursor
        last: $lastPageBy
        before: $startCursor
        sortKey: PUBLISHED_AT
        reverse: true
      ) {
        edges {
          node {
            author: authorV2 {
              name
            }
            handle
            id
            image {
              id
              altText
              url
              width
              height
            }
            publishedAt
            title
          }
        }
        pageInfo {
          hasPreviousPage
          hasNextPage
          startCursor
          endCursor
        }
      }
    }
  }
`;
