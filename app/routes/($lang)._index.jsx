import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {DataLayer, GoogleReviewsBadge, MWLandingPage} from '~/components';
import {getLandingPageFeaturedProductData} from '~/lib/contentfulQuery.server';
import {
  LANDING_2ND_PAGE_QUERY,
  LANDING_FIRST_PAGE_QUERY,
  LANDING_PAGE_CONTAINER_QUERY,
} from '~/lib/queries';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';

export async function loader({request, context}) {
  const variables = {
    handle: 'home-page-container',
  };
  const contentfulData = await getLandingPageFeaturedProductData(
    context,
    variables,
    LANDING_PAGE_CONTAINER_QUERY,
  );

  const contentfulItems = contentfulData?.landingPageContainerCollection?.items;
  const thisLandingPageContainer =
    contentfulItems?.length > 0 ? contentfulItems[0] : null;
  const landingPageItems =
    thisLandingPageContainer?.landingPageCollection?.items;
  const landingPages = await getLandingPage(landingPageItems, context);

  const seo = seoPayload.home({url: request.url});
  return defer({
    analytics: {
      shopify: {
        canonicalPath: '/',
        pageType: AnalyticsPageType.home,
      },
    },
    seo,
    landingPages,
    dataLayer: {
      mwDataLayer: {
        page: {
          currency: 'USD',
          pageName: 'Mattress Warehouse Home Page',
          slug: 'home',
          pageType: 'home',
        },
      },
    },
  });
}

export default function Homepage() {
  const data = useLoaderData();
  return (
    <>
      <div className="max-w-page-width mx-auto p-0 md:p-4 mw-home-page-container">
        {(data?.landingPages ?? []).map((item) => {
          return (
            <div key={item?.slug} className="mw-landing-page-container">
              <div className="max-w-page-width mx-auto mw-landing-page p-4">
                {item?.landingPageData && (
                  <MWLandingPage
                    landingPage={item?.landingPageData}
                    landing2ndPage={item?.landing2ndPageData}
                  />
                )}
              </div>
            </div>
          );
        })}
        <Suspense>
          <DataLayer dataLayer={data?.dataLayer} />
        </Suspense>
      </div>
      <GoogleReviewsBadge />
    </>
  );
}

export async function getLandingPage(landingPageItems, context) {
  let landingPage = [];
  if (landingPageItems?.length > 0) {
    await Promise.all(
      landingPageItems.map(async (item) => {
        const landingPageVariables = {
          handle: item?.slug,
        };

        const landingPageData = await getLandingPageFeaturedProductData(
          context,
          landingPageVariables,
          LANDING_FIRST_PAGE_QUERY,
        );

        const landing2ndPageData = await getLandingPageFeaturedProductData(
          context,
          landingPageVariables,
          LANDING_2ND_PAGE_QUERY,
        );

        landingPage.push({
          slug: item?.slug,
          landingPageData: landingPageData?.landingPageCollection?.items[0],
          landing2ndPageData:
            landing2ndPageData?.landingPageCollection?.items[0],
        });
      }),
    );
  }

  return landingPage;
}
