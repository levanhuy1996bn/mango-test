import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {DataLayer, Link} from '~/components';
import {seoPayload} from '~/lib/seo.server';
import {formatPhoneNumber, generateGoogleMapLink} from '~/lib/utils';
import {Suspense} from 'react';
export async function loader({context, request, params}) {
  const {storefront} = context;
  const {handle} = params;
  const [firstSlug, secondSlug] = handle.split('-', 2);
  const stateSlug = firstSlug && secondSlug ? secondSlug : firstSlug;
  const citySlug = firstSlug && secondSlug ? firstSlug : '';
  const data = await storefront.query(STORE_LOCATOR_QUERY, {
    variables: {
      key: stateSlug,
    },
    cache: storefront.CacheLong(),
  });

  const metafieldValueJson = data?.shop?.metafield?.value;

  if (!metafieldValueJson) {
    throw new Response('page', {status: 404});
  }

  const stateObj = metafieldValueJson.length
    ? JSON.parse(metafieldValueJson)
    : {};
  const pageTitle =
    firstSlug && secondSlug
      ? `Mattress Stores in ${stateObj?.cities?.[handle]?.name || ''}, ${
          stateObj?.name || ''
        }`
      : `Mattress Stores in ${stateObj?.name || ''}`;

  const {locationDetails, cityName, cityStateSlug, locations, stateName} =
    await getLocationDetails(storefront, citySlug, stateSlug, stateObj);

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: pageTitle,
        slug: `mattress-stores-in-${handle}`,
        pageType: 'store-locator',
      },
    },
  };

  const seoData = {
    title: pageTitle,
    _description: `${pageTitle}. Our mission is to provide quality mattresses at an affordable price. We offer a 1 year price match guarantee. Buy online or visit a store near you.`,
    titleTemplate: `%s · Mattress Warehouse`,
  };

  const seo = seoPayload.storeLocator(seoData, request.url);

  return defer({
    analytics: {
      shopify: {
        pageType: AnalyticsPageType.page,
      },
    },
    seo,
    dataLayer,
    firstSlug,
    secondSlug,
    citySlug,
    stateSlug,
    cityName,
    stateObj,
    locationDetails,
    cityStateSlug,
    locations: locations ?? {},
    stateName,
  });
}

export default function StoreLocator() {
  const data = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
      <Suspense>
        {data?.firstSlug && data?.secondSlug
          ? renderCity(
              data?.citySlug,
              data?.stateSlug,
              data?.stateName,
              data?.cityName,
              data?.locations,
              data?.locationDetails,
            )
          : renderState({stateObj: data?.stateObj})}
      </Suspense>
    </div>
  );
}

function renderCity(
  citySlug,
  stateSlug,
  stateName,
  cityName,
  locations,
  locationDetails,
) {
  return (
    <div className="w-full gap-8 grid border-none max-w-page-width mx-auto p-6 md:p-8 lg:p-12">
      <h1 className="text-2xl lg:text-3xl text-center">
        Shop Mattress Warehouse Stores{' '}
        <b>
          in {cityName}, {stateName}
        </b>
      </h1>
      <div className="justify-center flex flex-wrap gap-3 lg:gap-5">
        {(Object.entries(locations) || []).map((l) => {
          const locName = l?.[1]?.name || '';
          const locSlug = l?.[1]?.slug || '';
          const locId = l?.[1]?.id || locSlug || '';
          let locPhone = '';
          let locGooglePlaceId = '';
          let locAddress = '';
          if (locationDetails?.[`locationId${locId}`]?.value) {
            const locDetailObj = JSON.parse(
              locationDetails?.[`locationId${locId}`]?.value || '',
            );
            locPhone = locDetailObj?.phone || '';
            locGooglePlaceId = locDetailObj?.googlePlaceId || '';
            locAddress = locDetailObj?.address || '';
          }
          return (
            <div
              key={locId}
              className="border border-gray-100 rounded-sm shadow-md p-3 flex basis-full sm:basis-[calc(50%-8px)] md:basis-[calc(33%-16px)] lg:basis-[calc(25%-16px)]"
            >
              <div className="flex flex-col gap-1 sm:gap-1 md:gap-1.5 lg:gap-2">
                <Link
                  to={`/store-locator/mattress-warehouse-of/${locSlug}-${locId}-${citySlug}-${stateSlug}`}
                  className="font-bold text-base md:text-md lg:text-lg hover:text-mw-red-500 hover:underline hover:underline-offset-1"
                >
                  Mattress Warehouse of
                  <br />
                  {locName}
                </Link>
                {locPhone && (
                  <a
                    href={`tel:${locPhone}`}
                    className="font-bold hover:text-mw-red-500 hover:underline hover:underline-offset-1"
                  >
                    {formatPhoneNumber(locPhone)}
                  </a>
                )}
                {locAddress && (
                  <div className="flex flex-col gap-1">
                    <div>{locAddress}</div>
                    <Link
                      to={generateGoogleMapLink({
                        address: locAddress,
                        googlePlaceId: locGooglePlaceId,
                      })}
                      target="_blank"
                      className="underline underline-offset-1 font-bold hover:text-mw-red-500"
                    >
                      Get Directions
                    </Link>
                  </div>
                )}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

function renderState({stateObj}) {
  const {name, cities} = stateObj;
  const stateName = name || '';
  return (
    <div className="w-full gap-8 grid border-none max-w-page-width mx-auto p-6 md:p-8 lg:p-12">
      <h1 className="text-2xl lg:text-3xl text-center">
        Shop Mattress Warehouse Stores <b>in {stateName}</b>
      </h1>
      <div className="justify-center flex flex-wrap gap-3 lg:gap-5">
        {(Object.entries(cities) || []).map((c) => {
          const {name, slug} = c?.[1] || {
            name: '',
            slug: null,
            locations: {},
          };
          const cityStateSlug = slug;
          const cityName = name;
          return cityStateSlug ? (
            <Link
              key={cityStateSlug}
              className="border border-gray-300 rounded-md p-3 flex justify-center items-center basis-[calc(50%-8px)] md:basis-[calc(33%-16px)] lg:basis-[calc(25%-16px)] hover:bg-mw-red-500 hover:border-mw-red-500 hover:text-white"
              to={`/store-locator/mattress-stores-in/${cityStateSlug}`}
            >
              <div className="font-bold text-base md:text-md lg:text-lg text-center">
                {cityName}
              </div>
            </Link>
          ) : (
            <></>
          );
        })}
      </div>
    </div>
  );
}

async function getLocationDetails(storefront, citySlug, stateSlug, stateObj) {
  const cityStateSlug = `${citySlug}-${stateSlug}`;
  const locations = stateObj?.cities?.[cityStateSlug]?.locations || {};
  const cityName = stateObj?.cities?.[cityStateSlug]?.name || '';
  let locationDetails = {};

  const locationIds = Object.keys(locations);
  const locationDetailsQuery = locationIds.reduce((full, locationId) => {
    return `${full}
      locationId${locationId}: metafield(namespace: "location_details", key: "${locationId}") {
        value
      }`;
  }, '');

  const stateNameArr = stateSlug.split('_');
  for (let i = 0; i < stateNameArr.length; i++) {
    stateNameArr[i] =
      stateNameArr[i][0].toUpperCase() + stateNameArr[i].substr(1);
  }
  const stateName = stateNameArr.join(' ');

  if (locationDetailsQuery?.length > 0) {
    const data = await storefront.query(
      getLocationQuery(locationDetailsQuery),
      {
        variables: {
          key: stateSlug,
        },
        cache: storefront.CacheLong(),
      },
    );

    locationDetails = data?.locations || {};
  }

  return {
    locationDetails,
    cityName,
    cityStateSlug,
    locations,
    stateName,
  };
}

const STORE_LOCATOR_QUERY = `#graphql
  query State($key: String!) {
    shop {
      metafield(namespace: "locations_state_cities", key: $key) {
        namespace
        key
        value
      }
    }
  }
`;

const getLocationQuery = (query) => {
  return `#graphql
      query LocationsByIds {
        locations: shop {
          ${query}
        }
      }
    `;
};
