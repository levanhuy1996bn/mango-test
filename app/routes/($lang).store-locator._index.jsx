import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {DataLayer, Link} from '~/components';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';
export async function loader({context, request}) {
  const {storefront} = context;
  const data = await storefront.query(STORE_LOCATOR_QUERY, {
    variables: {
      languageCode: storefront.i18n.language,
    },
    cache: storefront.CacheLong(),
  });

  const metafieldValueJson = data?.shop?.metafield?.value;

  if (!metafieldValueJson) {
    throw new Response('page', {status: 404});
  }

  const statesObj = metafieldValueJson.length
    ? JSON.parse(metafieldValueJson ?? '{}')
    : {};
  const {states} = statesObj;
  const stateArray = Object.entries(states);

  if (!stateArray) {
    throw new Response('page', {status: 404});
  }

  const pageTitle = 'Stores By State';

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: pageTitle,
        slug: 'store-locator',
        pageType: 'store-locator',
      },
    },
  };

  const seoData = {
    title: pageTitle,
    description: `${pageTitle}. Our mission is to provide quality mattresses at an affordable price. We offer a 1 year price match guarantee. Buy online or visit a store near you.`,
    titleTemplate: `%s · Mattress Warehouse`,
  };

  const seo = seoPayload.storeLocator(seoData, request.url);

  return defer({
    analytics: {
      shopify: {
        pageType: AnalyticsPageType.page,
      },
    },
    seo,
    dataLayer,
    stateArray,
  });
}

export default function StoreLocatorHome() {
  const data = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
      <Suspense>
        <div className="w-full gap-8 grid border-none max-w-page-width mx-auto p-6 md:p-8 lg:p-12">
          <h1 className="text-2xl lg:text-3xl text-center">
            Shop Mattress Warehouse Stores <b>by State</b>
          </h1>
          <div className="justify-center flex flex-wrap gap-3 lg:gap-5">
            {(data?.stateArray || []).map((s) => {
              const {name, slug} = s?.[1] || {name: '', slug: null};
              return slug ? (
                <Link
                  key={slug}
                  className="border border-gray-300 rounded-md p-3 flex justify-center items-center basis-[calc(50%-16px)] md:basis-[calc(33%-16px)] lg:basis-[calc(25%-16px)] hover:bg-mw-red-500 hover:border-mw-red-500 hover:text-white"
                  to={`/store-locator/mattress-stores-in/${slug}`}
                >
                  <div className="font-bold text-base md:text-md lg:text-lg text-center">
                    {name}
                  </div>
                </Link>
              ) : (
                <></>
              );
            })}
          </div>
        </div>
      </Suspense>
    </div>
  );
}

const STORE_LOCATOR_QUERY = `#graphql
  query States {
    shop {
      metafield(namespace: "locations", key: "states") {
        namespace
        key
        value
      }
    }
  }
`;
