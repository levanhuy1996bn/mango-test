import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType, Image} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {CustomFont, DataLayer, PageHeader, Section} from '~/components';
import {CACHE_SHORT, routeHeaders} from '~/data/cache';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';

export const headers = routeHeaders;

export async function loader({context, request, params}) {
  const {storefront} = context;
  const {blogHandle, handle} = params;
  const {language, country} = storefront.i18n;

  const data = await storefront.query(ARTICLE_QUERY, {
    variables: {
      language,
      blogHandle,
      articleHandle: handle,
    },
  });

  if (
    !data?.blog?.articleByHandle ||
    data?.blog?.articleByHandle?.blog?.handle !== blogHandle
  ) {
    throw new Response(null, {status: 404});
  }

  const {title, publishedAt, contentHtml, author} = data.blog.articleByHandle;

  const formattedDate = new Intl.DateTimeFormat(`${language}-${country}`, {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }).format(new Date(publishedAt));

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: data?.blog?.articleByHandle.title,
        slug: handle,
        pageType: 'blogs',
      },
    },
  };

  const seo = seoPayload.blog({
    blog: data?.blog,
    url: request.url,
    title: data?.blog?.articleByHandle?.title,
  });

  return defer(
    {
      analytics: {
        shopify: {
          pageType: AnalyticsPageType.page,
        },
      },
      seo,
      dataLayer,
      formattedDate,
      title,
      author,
      blog: data?.blog,
      contentHtml,
    },
    {
      headers: {
        'Cache-Control': CACHE_SHORT,
      },
    },
  );
}

export default function Post() {
  const {dataLayer, formattedDate, title, author, blog, contentHtml} =
    useLoaderData();
  return (
    <div>
      <CustomFont />
      <Suspense>
        <DataLayer dataLayer={dataLayer} />
      </Suspense>
      <Suspense>
        <PageHeader heading={title} variant="blogPost">
          <span>
            {formattedDate} &middot; {author.name}
          </span>
        </PageHeader>
        <Section as="article" padding="x">
          {blog?.articleByHandle?.image && (
            <Image
              data={blog.articleByHandle.image}
              className="w-full mx-auto mt-8 md:mt-16 max-w-7xl"
              sizes="90vw"
              loading="eager"
            />
          )}
          <div
            dangerouslySetInnerHTML={{__html: contentHtml}}
            className="article"
          />
        </Section>
      </Suspense>
    </div>
  );
}

const ARTICLE_QUERY = `#graphql
  query ArticleDetails(
    $language: LanguageCode
    $blogHandle: String!
    $articleHandle: String!
  ) @inContext(language: $language) {
    blog(handle: $blogHandle) {
      articleByHandle(handle: $articleHandle) {
        title
        contentHtml
        publishedAt
        author: authorV2 {
          name
        }
        blog {
          handle
        }
        image {
          id
          altText
          url
          width
          height
        }
      }
    }
  }
`;
