import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {BlogFeaturesCarousel, DataLayer} from '~/components';
import {CACHE_SHORT, routeHeaders} from '~/data/cache';
import {seoPayload} from '~/lib/seo.server';
import moment from 'moment';
import {Suspense} from 'react';

export const headers = routeHeaders;

export async function loader({context, request}) {
  const {storefront} = context;
  const topics = await storefront.query(BLOGINFO_QUERY, {
    variables: {},
  });

  const newPosts = await storefront.query(NEWPOST_QUERY, {
    variables: {},
  });

  const features = await storefront.query(FEATURES_QUERY, {
    variables: {},
  });

  const articlesInfoHolder = features?.articles?.nodes || [];
  const sorted = articlesInfoHolder.sort((a, b) => {
    if (a.tags.includes('feature1') !== b.tags.includes('feature1')) {
      return a.tags.includes('feature1') ? -1 : 1;
    }
    if (a.tags.includes('feature2') !== b.tags.includes('feature2')) {
      return a.tags.includes('feature2') ? -1 : 1;
    }
    if (a.tags.includes('feature3') !== b.tags.includes('feature3')) {
      return a.tags.includes('feature3') ? -1 : 1;
    }
    return 0; // If none of the features match, maintain the order.
  });

  const articlesInfo = sorted.map((article) => {
    return {
      ...article,
    };
  });

  const articlesInfoHolders = newPosts?.articles?.nodes || [];

  const blogInfo = topics?.blogs?.nodes || [];

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: 'Bed Post',
        slug: 'blogs',
        pageType: 'blogs',
      },
    },
  };

  const seo = seoPayload.home({url: request.url});

  return defer(
    {
      analytics: {
        shopify: {
          pageType: AnalyticsPageType.page,
        },
      },
      blogInfo,
      articlesInfo,
      articlesInfoHolders,
      seo,
      dataLayer,
    },
    {
      headers: {
        'Cache-Control': CACHE_SHORT,
      },
    },
  );
}

export default function BlogLandingPage() {
  const {dataLayer, articlesInfoHolders, articlesInfo, blogInfo} =
    useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={dataLayer} />
      </Suspense>
      <Suspense>
        <div className="bedPostMaincontainer max-w-page-width mx-auto mw-landing-page p-4 flex flex-col gap-4 md:gap-6 lg:gap-8">
          <section className="w-full">
            <img
              src="https://cdn.shopify.com/s/files/1/0657/8752/7425/files/BlogBannerV1.jpg?v=1697520907"
              className="hidden md:block"
              alt="BlogBannerV1"
            />
            <img
              src="https://cdn.shopify.com/s/files/1/0657/8752/7425/files/BlogBanner-mobileV1.jpg?v=1697520907"
              className="block md:hidden"
              alt="BlogBanner-mobileV1"
            />
          </section>

          <section className="newPost">
            <h2 className="text-3xl font-bold pt-6 pb-4">New Post</h2>
            <div className="postCarousel grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
              <Suspense>
                <NewPostGrid articlesInfoHolders={articlesInfoHolders} />
              </Suspense>
            </div>
          </section>

          <section className="browseTopic">
            <h2 className="text-3xl font-bold pt-6 pb-4">Browse By Topic</h2>
            <div className="grid grid-cols-3 lg:grid-cols-5 gap-4">
              <Suspense>
                <TopicsGrid blogInfo={blogInfo} />
              </Suspense>
            </div>
          </section>

          <section className="featuredStories">
            <h2 className="text-3xl font-bold pt-6 pb-4">Featured Stories</h2>
            <div className="postCarousel">
              <Suspense>
                <FeaturesGrid articlesInfo={articlesInfo} />
              </Suspense>
            </div>
          </section>
          <section className="block md:hidden" />
        </div>
      </Suspense>
    </div>
  );
}

function FeaturesGrid({articlesInfo}) {
  return <BlogFeaturesCarousel articlesInfo={articlesInfo} />;
}

function TopicsGrid({blogInfo}) {
  return (
    <>
      {(Object.values(blogInfo) || []).map((blog) => (
        <a
          key={`topics-${blog.handle}`}
          href={`/blogs/${blog.handle?.toLowerCase()}`}
          className={
            Object.values(blog.articles.nodes).length > 0
              ? 'bg-gray-200 rounded-lg text-center font-bold p-2 text-[11px] md:text-base flex justify-center items-center'
              : 'hidden'
          }
        >
          {blog.title}
        </a>
      ))}
    </>
  );
}

function NewPostGrid({articlesInfoHolders}) {
  return (
    <>
      {(Object.values(articlesInfoHolders) || []).map((article, index) => (
        <a
          key={index}
          className="bg-gray-200 rounded-lg align-top overflow-hidden no-underline flex flex-col"
          href={`/blogs/${article.blog?.handle?.toLowerCase()}/${article.handle?.toLowerCase()}`}
        >
          <div className="">
            <img
              className="h-96 object-cover"
              alt="New Post"
              src={article.image.url}
            />
          </div>
          <div className="text-xl font-bold text-black no-underline max-w-xs p-2">
            {article.title}
          </div>
          <div className="text-sm text-black no-underline italic mt-auto p-2">
            {moment(article.publishedAt).format('MMMM DD, YYYY')}
          </div>
        </a>
      ))}
    </>
  );
}

const NEWPOST_QUERY = `#graphql
  query {
    articles(first: 4, sortKey: PUBLISHED_AT, reverse: true) {
      nodes {
        title
        handle
        image {
          url
        }
        publishedAt
        blog {
          handle
        }
      }
    }
  }
`;
const BLOGINFO_QUERY = `#graphql
  query {
    blogs(first: 20, query: "NOT title:news AND NOT title:information") {
      nodes {
        title
        handle
        seo {
          title
          description
        }
        articles(first: 1) {
          nodes {
            title
          }
        }
      }
    }
  }
`;

const FEATURES_QUERY = `#graphql
  query {
    articles(
      first: 4
      query: "tag:feature1 OR tag:feature2 OR tag:feature3 OR tag:feature4"
    ) {
      nodes {
        tags
        title
        handle
        content(truncateAt: 150)
        image {
          url
        }
        publishedAt
        blog {
          handle
        }
      }
    }
  }
`;
