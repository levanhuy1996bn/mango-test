import {flattenConnection} from '@shopify/hydrogen';
import {json} from '@shopify/remix-oxygen';
import {loadAdminQueryInContext} from '~/hooks/useAdminQueryInContext';

export async function action({request, context}) {
  const {session} = context;
  const headers = new Headers();
  if (request.method !== 'POST') {
    return new Response('Method not allowed', {
      status: 405,
      headers: {Allow: 'POST'},
    });
  }

  const jsonBody = await request.json();
  let nearestLocations = [];

  let userLat = jsonBody?.latitude || '';
  let userLng = jsonBody?.longitude || '';
  let withoutSession = jsonBody?.withoutSession || false;

  if (!userLat || !userLng) {
    return nearestLocations;
  }

  userLat = parseFloat(userLat);
  userLng = parseFloat(userLng);

  const data = await loadAdminQueryInContext(context, LOCATION_DETAILS_QUERY, {
    variables: {},
  });

  const {locationDetails} = data || {};
  const storeLocations = [];
  storeLocations.push(...flattenConnection(locationDetails?.metafields || {}));
  if (locationDetails?.metafields?.pageInfo?.hasNextPage) {
    const data2ndPage = await loadAdminQueryInContext(
      context,
      LOCATION_DETAILS_QUERY,
      {
        variables: {
          cursor: locationDetails?.metafields?.pageInfo?.endCursor,
        },
      },
    );

    const locationDetails2ndPage = data2ndPage.locationDetails || {};
    storeLocations.push(
      ...flattenConnection(locationDetails2ndPage?.metafields || {}),
    );
  }

  let locationDetailsFinal = {};
  storeLocations.map((loc) => {
    locationDetailsFinal[loc.key] = JSON.parse(loc.value || '[]');
    return loc;
  });

  if (
    geolocationHelper.hasLocationsNearby(userLat, userLng, locationDetailsFinal)
  ) {
    const results =
      geolocationHelper
        .sortWithCrow(userLat, userLng, locationDetailsFinal)
        .slice(0, 10) || [];

    results.map((result) => {
      nearestLocations.push(result[0]);
      return result;
    });
  }

  let nearestStoreLocations = {};
  // storeLocations.map((loc) => {
  //   if (nearestLocations.includes(loc.key)) {
  //     nearestStoreLocations[loc.key] = JSON.parse(loc.value || '[]');
  //   }
  //   return loc;
  // });
  // preserve the order of the nearest locations when adding to nearestStoreLocations (to appear correctly in lists)
  nearestLocations.map((locId) => {
    storeLocations.map((loc) => {
      if (loc.key === locId) {
        nearestStoreLocations[loc.key] = JSON.parse(loc.value || '[]');
        // TODO determine workaround to retrieve this info while satisfying (session) 4096 byte cookie size limitation
        delete nearestStoreLocations[loc.key]['googlePlaceId'];
        delete nearestStoreLocations[loc.key]['hours'];
        /*
        // in order to fit this data into the session cookie, minimize the original data for the hours as much as possible
        if (nearestStoreLocations[loc.key]?.hours) {
          let formattedHours = {
            su: '',
            m: '',
            t: '',
            w: '',
            th: '',
            f: '',
            s: '',
          };
          nearestStoreLocations[loc.key]?.hours?.sunday?.openIntervals?.map(
            (openInterval) => {
              if (formattedHours.su) {
                formattedHours.su += ',';
              }
              formattedHours.su +=
                openInterval?.start + '-' + openInterval?.end;
              return openInterval;
            },
          );
          nearestStoreLocations[loc.key]?.hours?.monday?.openIntervals?.map(
            (openInterval) => {
              if (formattedHours.m) {
                formattedHours.m += ',';
              }
              formattedHours.m += openInterval?.start + '-' + openInterval?.end;
              return openInterval;
            },
          );
          nearestStoreLocations[loc.key]?.hours?.tuesday?.openIntervals?.map(
            (openInterval) => {
              if (formattedHours.t) {
                formattedHours.t += ',';
              }
              formattedHours.t += openInterval?.start + '-' + openInterval?.end;
              return openInterval;
            },
          );
          nearestStoreLocations[loc.key]?.hours?.wednesday?.openIntervals?.map(
            (openInterval) => {
              if (formattedHours.w) {
                formattedHours.w += ',';
              }
              formattedHours.w += openInterval?.start + '-' + openInterval?.end;
              return openInterval;
            },
          );
          nearestStoreLocations[loc.key]?.hours?.thursday?.openIntervals?.map(
            (openInterval) => {
              if (formattedHours.th) {
                formattedHours.th += ',';
              }
              formattedHours.th +=
                openInterval?.start + '-' + openInterval?.end;
              return openInterval;
            },
          );
          nearestStoreLocations[loc.key]?.hours?.friday?.openIntervals?.map(
            (openInterval) => {
              if (formattedHours.f) {
                formattedHours.f += ',';
              }
              formattedHours.f += openInterval?.start + '-' + openInterval?.end;
              return openInterval;
            },
          );
          nearestStoreLocations[loc.key]?.hours?.saturday?.openIntervals?.map(
            (openInterval) => {
              if (formattedHours.s) {
                formattedHours.s += ',';
              }
              formattedHours.s += openInterval?.start + '-' + openInterval?.end;
              return openInterval;
            },
          );
          nearestStoreLocations[loc.key].hours = formattedHours;
        }
        */
      }
      return loc;
    });
    return locId;
  });

  if (withoutSession) {
    return {nearestLocations, nearestStoreLocations};
  }

  session.set('userLocation', jsonBody);
  // make sure it is set in the session
  await session.get('userLocation');

  session.set('nearestLocations', nearestLocations);
  // make sure it is set in the session
  await session.get('nearestLocations');

  session.set('nearestStoreLocations', nearestStoreLocations);
  // make sure it is set in the session
  await session.get('nearestStoreLocations');

  headers.set('Set-Cookie', await session.commit());

  return json(
    {
      nearestLocations,
      nearestStoreLocations,
    },
    {
      headers,
    },
  );
}

const geolocationHelper = {
  // process locations
  hasLocationsNearby: (userLat, userLng, _locationDetails) => {
    if (!_locationDetails) {
      return false;
    } // check for metafields data
    // find locations within 2 degrees
    const latMin = userLat - 2;
    const latMax = userLat + 2;
    const lngMin = userLng - 2;
    const lngMax = userLng + 2;
    const locations = Object.entries(_locationDetails).filter(
      (a) =>
        latMin <= a[1].lat &&
        a[1].lat <= latMax &&
        lngMin <= a[1].long &&
        a[1].long <= lngMax,
    );
    return !!locations.length;
  },
  calcCrow: (lat1, lon1, lat2, lon2) => {
    const R = 6371; // Radius of the earth in km
    const dLat = ((lat2 - lat1) * Math.PI) / 180; // deg2rad below
    const dLon = ((lon2 - lon1) * Math.PI) / 180;
    let a =
      0.5 -
      Math.cos(dLat) / 2 +
      (Math.cos((lat1 * Math.PI) / 180) *
        Math.cos((lat2 * Math.PI) / 180) *
        (1 - Math.cos(dLon))) /
        2;
    return R * 2 * Math.asin(Math.sqrt(a));
  },
  sortWithCrow: (userLat, userLng, _locationDetails) => {
    if (!_locationDetails) {
      return [];
    } // check for metafields data
    const locations = Object.entries(_locationDetails).map((a) => {
      a[1].crow = geolocationHelper.calcCrow(
        userLat,
        userLng,
        a[1].lat,
        a[1].long,
      );
      return a;
    });
    locations.sort((a, b) => a[1].crow - b[1].crow);
    return locations;
  },
};

const LOCATION_DETAILS_QUERY = `#graphql
  query LocationDetails($cursor: String) {
    locationDetails: shop {
      metafields(first: 200, after: $cursor, namespace: "location_details") {
        edges {
          node {
            value
            namespace
            key
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;
