import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {
  DataLayer,
  Link,
  LocationCustomerReview,
  LocationFrequentlyAskedQuestions,
  LocationHours,
  LocationPopularCategories,
  LocationShoppingOptions,
  MapMarkerIcon,
} from '~/components';
import {YEXT_API_DEFAULT_TIMEZONE} from '~/lib/const';
import {seoPayload} from '~/lib/seo.server';
import {formatPhoneNumber, generateGoogleMapLink} from '~/lib/utils';
import {yextContentLocationsAPI} from '~/lib/yextContentLocationsAPI.server';
import {Suspense} from 'react';
const GoogleGeoAPIKey = 'AIzaSyAGrgD-RcdRlOlta7ObDyWd12Duo0750wE';
export async function loader({context, request, params}) {
  const {handle} = params;
  const {storefront} = context;
  const [locSlug, locId, citySlug, stateSlug] = handle.split('-', 4);
  const stateNameArr = stateSlug.split('_');
  for (let i = 0; i < stateNameArr.length; i++) {
    stateNameArr[i] =
      stateNameArr[i][0].toUpperCase() + stateNameArr[i].substr(1);
  }
  const stateName = stateNameArr.join(' ');

  const data = await storefront.query(STORE_LOCATOR_QUERY, {
    variables: {
      key: locId,
    },
    cache: storefront.CacheLong(),
  });

  const metafieldValueJson = data?.shop?.metafield?.value;

  if (!metafieldValueJson) {
    throw new Response('page', {status: 404});
  }

  const metafieldObj = metafieldValueJson.length
    ? JSON.parse(metafieldValueJson)
    : {};
  metafieldObj.nameMetafield = metafieldObj?.name || '';
  metafieldObj.addressMetafield = metafieldObj?.address || '';

  // location detail from Yext
  const yextLocations = await yextContentLocationsAPI(locId);
  const {response} = yextLocations;
  const yextObj = response?.docs?.[0] || {};

  // merge
  const locationObj = {
    ...metafieldObj,
    ...yextObj,
  };

  const {
    name,
    nameMetafield,
    address,
    addressMetafield,
    phone,
    mainPhone,
    googlePlaceId,
    hours,
    brands,
    description,
    frequentlyAskedQuestions,
    paymentOptions,
    pickupAndDeliveryServices,
    products,
    ref_reviewsAgg,
    specialities,
    timezone,
    tollFreePhone,
    videos,
  } = locationObj;

  const pageTitle = `Mattress Warehouse of ${name || ''}, ${stateName || ''}`;
  const locNameMetafield = nameMetafield; // retrieve the name from metafield
  const locNameYext = name || '';
  const locAddressMetafield = addressMetafield; // retrieve the address from metafield
  const locAddressYext = `${address?.line1}, ${address?.city}, ${address?.region} ${address?.postalCode} ${address?.countryCode}`;
  const locAddress = locAddressMetafield || locAddressYext || '';
  const locPhone = mainPhone || formatPhoneNumber(phone) || '';
  const locGooglePlaceId = googlePlaceId || '';
  const locHours = hours || {};
  // const locBrands = brands || [];
  const locDescription = description || '';
  const locFrequentlyAskedQuestions = frequentlyAskedQuestions || [];
  // const locPaymentOptions = paymentOptions || [];
  const locPickupAndDeliveryServices = pickupAndDeliveryServices || [];
  const locProducts = products || [];
  // const locRef_reviewsAgg = ref_reviewsAgg || [];
  // const locSpecialities = specialities || [];
  const locTimezone = timezone || YEXT_API_DEFAULT_TIMEZONE;
  // const locTollFreePhone = tollFreePhone || '';
  // const locVideos = videos || [];

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: pageTitle,
        slug: `mattress-warehouse-of-${handle}`,
        pageType: 'store-locator',
      },
    },
  };

  const seoData = {
    title: pageTitle,
    _description: `${pageTitle}. Our mission is to provide quality mattresses at an affordable price. We offer a 1 year price match guarantee. Buy online or visit a store near you.`,
    titleTemplate: `%s · Mattress Warehouse`,
  };

  const seo = seoPayload.storeLocator(seoData, request.url);

  return defer({
    analytics: {
      shopify: {
        pageType: AnalyticsPageType.page,
      },
    },
    locNameMetafield,
    locNameYext,
    locAddress,
    seo,
    dataLayer,
    locGooglePlaceId,
    pageTitle,
    locDescription,
    locPickupAndDeliveryServices,
    locHours,
    locTimezone,
    locPhone,
    locProducts,
    locFrequentlyAskedQuestions,
    locId,
  });
}

export default function StoreLocator() {
  const data = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
      <Suspense>
        <div className="flex flex-col gap-14 py-6 md:py-8 lg:py-12">
          <div className="w-full max-w-page-width mx-auto px-6 md:px-8 lg:px-12">
            <div className="flex flex-col gap-8">
              <h1>
                {data?.locNameMetafield ? (
                  <>
                    <div className="text-2xl lg:text-3xl">
                      Mattress Warehouse of
                    </div>
                    <div className="text-3xl lg:text-4xl font-bold">
                      {data?.locNameMetafield}
                    </div>
                  </>
                ) : data?.locNameYext ? (
                  <>{data?.locNameYext}</>
                ) : (
                  <></>
                )}
              </h1>
              <div className="grid grid-cols-1 lg:grid-cols-[60%_40%] gap-12">
                <div className="flex flex-col gap-8">
                  <h2 className="text-2xl lg:text-[1.6875rem] font-bold">
                    Location
                  </h2>
                  <div className="flex flex-col gap-8">
                    {data?.locAddress && (
                      <div className="flex flex-wrap md:flex-nowrap gap-3">
                        <div className="flex items-center gap-3">
                          <MapMarkerIcon className="text-mw-red-500 text-4xl lg:text-5xl" />
                          <div className="text-xl">{data?.locAddress}</div>
                        </div>
                        <div>
                          <Link
                            to={generateGoogleMapLink({
                              address: data?.locAddress,
                              googlePlaceId: data?.locGooglePlaceId,
                            })}
                            target="_blank"
                            className="inline-block px-5 py-2 text-sm font-bold text-white rounded-md border border-mw-red-500 bg-mw-red-500 hover:text-mw-red-500 hover:bg-white whitespace-nowrap"
                          >
                            Get Directions
                          </Link>
                        </div>
                      </div>
                    )}
                    <div className="bg-mw-grayscale-100 flex justify-center items-center">
                      <iframe
                        title={data?.pageTitle}
                        width="960"
                        height="440"
                        src={
                          data?.locGooglePlaceId
                            ? `https://www.google.com/maps/embed/v1/place?key=${GoogleGeoAPIKey}&q=place_id:${encodeURIComponent(
                                data?.locGooglePlaceId,
                              )}`
                            : data?.locAddress
                            ? `https://www.google.com/maps/embed/v1/place?key=${GoogleGeoAPIKey}&q=${encodeURIComponent(
                                data?.locAddress,
                              )}`
                            : ''
                        }
                      />
                    </div>
                  </div>
                  {data?.locDescription && <div>{data?.locDescription}</div>}
                </div>
                <div>
                  <div className="flex flex-col gap-8 lg:gap-10">
                    {data?.locPickupAndDeliveryServices?.length > 0 && (
                      <LocationShoppingOptions
                        locPickupAndDeliveryServices={
                          data?.locPickupAndDeliveryServices
                        }
                      />
                    )}
                    {data?.locHours && (
                      <LocationHours
                        locHours={data?.locHours}
                        locTimezone={data?.locTimezone}
                      />
                    )}
                    <div className="flex flex-col gap-5">
                      {data?.locPhone && (
                        <div className="flex flex-col md:flex-row items-start md:items-center gap-2 md:gap-5">
                          <div className="text-lg md:text-xl font-bold">
                            Call Us
                          </div>
                          <a
                            href={`tel:${data?.locPhone}`}
                            className="text-base md:text-lg hover:text-mw-red-500 underline underline-offset-4"
                          >
                            {data?.locPhone}
                          </a>
                        </div>
                      )}
                      <div className="flex flex-col md:flex-row items-start md:items-center gap-2 md:gap-5">
                        <div className="text-lg md:text-xl font-bold">
                          Email Us
                        </div>
                        <a
                          href="mailto:info@mattresswarehouse.com"
                          className="text-baae md:text-lg hover:text-mw-red-500 underline underline-offset-4"
                        >
                          info@mattresswarehouse.com
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {data?.locProducts?.length > 0 && (
            <LocationPopularCategories locProducts={data?.locProducts} />
          )}
          <Suspense>
            <LocationCustomerReview locId={data?.locId} />
          </Suspense>
          {data?.locFrequentlyAskedQuestions?.length > 0 && (
            <LocationFrequentlyAskedQuestions
              locFrequentlyAskedQuestions={data?.locFrequentlyAskedQuestions}
            />
          )}
        </div>
      </Suspense>
    </div>
  );
}

const STORE_LOCATOR_QUERY = `#graphql
  query State($key: String!) {
    shop {
      metafield(namespace: "location_details", key: $key) {
        value
      }
    }
  }
`;
