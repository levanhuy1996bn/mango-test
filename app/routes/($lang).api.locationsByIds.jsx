import {loadAdminQueryInContext} from '~/hooks/useAdminQueryInContext';

export async function action({request, context}) {
  if (request.method !== 'POST') {
    return new Response('Method not allowed', {
      status: 405,
      headers: {Allow: 'POST'},
    });
  }

  const jsonBody = await request.json();

  let locationDetailsQuery = jsonBody?.locationDetailsQuery || '';

  if (!locationDetailsQuery) {
    return {};
  }

  const data = await loadAdminQueryInContext(
    context,
    getLocationQuery(locationDetailsQuery),
    {
      variables: {},
    },
  );

  return data?.locations ? data.locations : {};
}

const getLocationQuery = (query) => {
  return `#graphql
      query locationsByIds {
        locations: shop {
          ${query}
        }
      }
    `;
};
