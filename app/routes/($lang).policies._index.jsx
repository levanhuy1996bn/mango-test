import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {DataLayer, Heading, Link, PageHeader, Section} from '~/components';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';
export async function loader({context, request}) {
  const {storefront} = context;
  const data = await storefront.query(POLICIES_QUERY, {
    variables: {
      languageCode: storefront.i18n.language,
    },
    cache: storefront.CacheLong(),
  });

  if (!data?.shop) {
    throw new Response('page', {status: 404});
  }

  const {
    privacyPolicy,
    shippingPolicy,
    termsOfService,
    refundPolicy,
    subscriptionPolicy,
  } = data.shop;

  const policies = [
    privacyPolicy,
    shippingPolicy,
    termsOfService,
    refundPolicy,
    subscriptionPolicy,
  ];

  if (policies.every((element) => element === null)) {
    throw new Response('Not found', {status: 404});
  }

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: `Policies`,
        slug: 'policies',
        pageType: 'policies',
      },
    },
  };

  const seo = seoPayload.policies({policies, url: request.url});

  return defer({
    analytics: {
      shopify: {
        pageType: AnalyticsPageType.page,
      },
    },
    seo,
    policies,
    dataLayer,
  });
}

export default function Policies() {
  const data = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
      <Suspense>
        <PageHeader heading="Policies" />
        <Section padding="x" className="mb-24">
          {(data?.policies ?? []).map((policy) => {
            if (!policy) {
              return;
            }
            return (
              <Heading className="font-normal text-heading" key={policy.id}>
                <Link to={`/policies/${policy.handle}`}>{policy.title}</Link>
              </Heading>
            );
          })}
        </Section>
      </Suspense>
    </div>
  );
}

const POLICIES_QUERY = `#graphql
  fragment Policy on ShopPolicy {
    id
    title
    handle
  }

  query PoliciesQuery {
    shop {
      privacyPolicy {
        ...Policy
      }
      shippingPolicy {
        ...Policy
      }
      termsOfService {
        ...Policy
      }
      refundPolicy {
        ...Policy
      }
      subscriptionPolicy {
        id
        title
        handle
      }
    }
  }
`;
