import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {json} from '@shopify/remix-oxygen';
import {DataLayer} from '~/components';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';
import invariant from 'tiny-invariant';

export async function loader({request, params, context}) {
  const {handle} = params;
  invariant(handle, 'Missing page handle');

  const {page} = await context.storefront.query(PAGE_QUERY, {
    variables: {
      handle,
      language: context.storefront.i18n.language,
    },
  });

  if (!page) {
    throw new Response(null, {status: 404});
  }

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: page.title,
        slug: page.handle,
        pageType: 'pages',
      },
    },
  };

  const seo = seoPayload.page({page, url: request.url});

  return json({
    analytics: {
      shopify: {
        pageType: AnalyticsPageType.page,
        resourceId: page.id,
      },
    },
    page,
    seo,
    dataLayer,
  });
}

export default function Page() {
  const {page, dataLayer} = useLoaderData();

  return (
    <>
      <Suspense>
        <DataLayer dataLayer={dataLayer} />
      </Suspense>
      <Suspense>
        {page?.body && (
          <div id="mw-page" dangerouslySetInnerHTML={{__html: page.body}} />
        )}
      </Suspense>
    </>
  );
}

const PAGE_QUERY = `#graphql
  query PageDetails($language: LanguageCode, $handle: String!)
  @inContext(language: $language) {
    page(handle: $handle) {
      id
      title
      body
      seo {
        description
        title
      }
    }
  }
`;
