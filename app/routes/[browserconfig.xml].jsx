export async function loader() {
  return new Response(browserConfig(), {
    headers: {
      'content-type': 'application/xml',
    },
  });
}

function browserConfig() {
  return `
    <browserconfig>
      <msapplication>
          <tile>
              <square150x150logo src="/mstile-150x150.png"/>
              <TileColor>#da532c</TileColor>
          </tile>
      </msapplication>
    </browserconfig>
    `;
}
