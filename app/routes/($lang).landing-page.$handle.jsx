import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {DataLayer, MWLandingPage} from '~/components';
import {getLandingPageFeaturedProductData} from '~/lib/contentfulQuery.server';
import {LANDING_PAGE_CONTAINER_QUERY} from '~/lib/queries';
import {getLandingPage} from '~/routes/($lang)._index';
import {Suspense} from 'react';

export async function loader({context, params}) {
  const {handle} = params;
  const contentfulData = await getLandingPageFeaturedProductData(
    context,
    {
      handle,
    },
    LANDING_PAGE_CONTAINER_QUERY,
  );

  const contentfulItems = contentfulData?.landingPageContainerCollection?.items;
  const thisLandingPageContainer =
    contentfulItems?.length > 0 ? contentfulItems[0] : null;
  const landingPageItems =
    thisLandingPageContainer?.landingPageCollection?.items;
  const landingPages = await getLandingPage(landingPageItems, context);

  if (!landingPages?.length) {
    throw new Response('Not found', {status: 404});
  }

  return defer({
    analytics: {
      shopify: {
        canonicalPath: '/',
        pageType: AnalyticsPageType.home,
      },
    },
    landingPages,
    dataLayer: {
      mwDataLayer: {
        page: {
          currency: 'USD',
          pageName: 'Mattress Warehouse Home Page',
          slug: 'home',
          pageType: 'home',
        },
      },
    },
  });
}

export default function PublishedLandingPageContainerPreview() {
  const data = useLoaderData();
  return (
    <div className="max-w-page-width mx-auto p-0 md:p-4 mw-home-page-container">
      <Suspense>
        {(data?.landingPages ?? []).map((item) => {
          return (
            <div
              key={`${item?.slug}-landing-page`}
              className="mw-landing-page-container"
            >
              <div className="max-w-page-width mx-auto mw-landing-page p-4">
                {item?.landingPageData && (
                  <MWLandingPage
                    landingPage={item?.landingPageData}
                    landing2ndPage={item?.landing2ndPageData}
                  />
                )}
              </div>
            </div>
          );
        })}
      </Suspense>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
    </div>
  );
}
