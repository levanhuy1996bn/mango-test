import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType, flattenConnection} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {ArticleCard, DataLayer, Grid, PageHeader} from '~/components';
import {CACHE_SHORT, routeHeaders} from '~/data/cache';
import {PAGINATION_SIZE, getImageLoadingPriority} from '~/lib/const';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';

const BLOG_HANDLE = 'Journal';

export const headers = routeHeaders;

export async function loader({context, request}) {
  const {storefront} = context;
  const {language, country} = storefront.i18n;
  const {blog} = await storefront.query(BLOG_QUERY, {
    variables: {
      languageCode: storefront.i18n.language,
      blogHandle: BLOG_HANDLE,
      pageBy: PAGINATION_SIZE,
    },
  });

  if (!blog?.articles) {
    throw new Response('Not found', {status: 404});
  }

  const articles = flattenConnection(blog.articles).map((article) => {
    const {publishedAt} = article;
    return {
      ...article,
      publishedAt: new Intl.DateTimeFormat(`${language}-${country}`, {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
      }).format(new Date(publishedAt)),
    };
  });

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: 'All Journals',
        slug: 'journal',
        pageType: 'journal',
      },
    },
  };

  const seo = seoPayload.blog({blog, url: request.url, title: 'All Journals'});

  return defer(
    {
      analytics: {
        shopify: {
          pageType: AnalyticsPageType.page,
        },
      },
      articles,
      seo,
      dataLayer,
    },
    {
      headers: {
        'Cache-Control': CACHE_SHORT,
      },
    },
  );
}

export default function Policies() {
  const data = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
      <Suspense>
        <PageHeader heading={BLOG_HANDLE} className="gap-0">
          <Suspense>
            {data?.articles?.length > 0 ? (
              <Grid as="ol" layout="blog" gap="blog">
                {data?.articles.map((article, i) => {
                  return (
                    <ArticleCard
                      blogHandle={BLOG_HANDLE.toLowerCase()}
                      article={article}
                      key={article.id}
                      loading={getImageLoadingPriority(i, 2)}
                    />
                  );
                })}
              </Grid>
            ) : (
              <p>No articles found</p>
            )}
          </Suspense>
        </PageHeader>
      </Suspense>
    </div>
  );
}

const BLOG_QUERY = `#graphql
  query Blog(
    $language: LanguageCode
    $blogHandle: String!
    $pageBy: Int!
    $cursor: String
  ) @inContext(language: $language) {
    blog(handle: $blogHandle) {
      title
      seo {
        title
        description
      }
      articles(first: $pageBy, after: $cursor) {
        edges {
          node {
            author: authorV2 {
              name
            }
            contentHtml
            handle
            id
            image {
              id
              altText
              url
              width
              height
            }
            publishedAt
            title
          }
        }
      }
    }
  }
`;
