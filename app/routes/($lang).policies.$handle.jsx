import {useLoaderData} from '@remix-run/react';
import {AnalyticsPageType} from '@shopify/hydrogen';
import {defer} from '@shopify/remix-oxygen';
import {Button, DataLayer, PageHeader, Section} from '~/components';
import {seoPayload} from '~/lib/seo.server';
import {Suspense} from 'react';
import invariant from 'tiny-invariant';
export async function loader({context, request, params}) {
  const {handle} = params;
  invariant(handle, 'Missing policy handle');
  const {storefront} = context;
  // standard policy pages
  const policy = {
    privacyPolicy: handle === 'privacy-policy',
    shippingPolicy: handle === 'shipping-policy',
    termsOfService: handle === 'terms-of-service',
    refundPolicy: handle === 'refund-policy',
  };

  if (
    !policy.privacyPolicy &&
    !policy.shippingPolicy &&
    !policy.termsOfService &&
    !policy.refundPolicy
  ) {
    throw new Response('product', {status: 404});
  }

  const activePolicy = Object.keys(policy).find((key) => policy[key]);

  const data = await storefront.query(POLICIES_QUERY, {
    variables: {
      languageCode: storefront?.i18n?.language,
      ...policy,
    },
    cache: storefront.CacheLong(),
  });

  const policyData = data?.shop?.[activePolicy];

  if (!policyData) {
    throw new Response(null, {status: 404});
  }

  const dataLayer = {
    mwDataLayer: {
      page: {
        currency: 'USD',
        pageName: policyData.title,
        slug: policyData.handle,
        pageType: 'policies',
      },
    },
  };

  const seo = seoPayload.policy({policy: policyData, url: request.url});

  return defer({
    analytics: {
      shopify: {
        pageType: AnalyticsPageType.page,
        resourceId: policyData.id,
      },
    },
    seo,
    policy: policyData,
    product: data?.product,
    dataLayer,
  });
}

export default function Policy() {
  const data = useLoaderData();
  return (
    <div>
      <Suspense>
        <DataLayer dataLayer={data?.dataLayer} />
      </Suspense>
      <Suspense>
        <Section
          padding="all"
          display="flex"
          className="flex-col items-baseline w-full gap-8 md:flex-row"
        >
          <PageHeader
            heading={data?.policy?.title}
            className="grid items-start flex-grow gap-4 md:sticky top-36 md:w-5/12"
          >
            <Button
              className="justify-self-start"
              variant="inline"
              to={'/policies'}
            >
              &larr; Back to Policies
            </Button>
          </PageHeader>
          {data?.policy?.body !== undefined && (
            <div className="flex-grow w-full md:w-7/12">
              <div
                dangerouslySetInnerHTML={{__html: data?.policy.body}}
                className="prose dark:prose-invert"
              />
            </div>
          )}
        </Section>
      </Suspense>
    </div>
  );
}

const POLICIES_QUERY = `#graphql
  fragment Policy on ShopPolicy {
    body
    handle
    id
    title
    url
  }

  query PoliciesQuery(
    $languageCode: LanguageCode
    $privacyPolicy: Boolean!
    $shippingPolicy: Boolean!
    $termsOfService: Boolean!
    $refundPolicy: Boolean!
  ) @inContext(language: $languageCode) {
    shop {
      privacyPolicy @include(if: $privacyPolicy) {
        ...Policy
      }
      shippingPolicy @include(if: $shippingPolicy) {
        ...Policy
      }
      termsOfService @include(if: $termsOfService) {
        ...Policy
      }
      refundPolicy @include(if: $refundPolicy) {
        ...Policy
      }
    }
  }
`;
