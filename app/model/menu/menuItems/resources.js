function getMenuItemResourceDataQuery(resourceIds) {
  if (!resourceIds) {
    return '';
  }

  return `#graphql
    query getMenuItemResourceData {
      ${resourceIds.map((resourceId) => {
        const cleanResourceId =
          'resource_' + resourceId.replace(/[^a-zA-Z0-9_-]/g, '_');

        if (-1 !== resourceId.indexOf('Collection')) {
          return `#graphql
      ${cleanResourceId}: collection(id: "${resourceId}") {
        id
        handle
        title
        seo {
          title
          description
        }
        image {
          id
          url
          width
          height
          altText
        }
      }
      `;
        }

        return '';
      })}
    }
  `;
}

export default async function getMenuItemResourceData({
  resourceIds,
  storefront,
}) {
  // accumulate data in a single object
  let finalData = {};

  if (!resourceIds) {
    return finalData;
  }

  const data = await storefront.query(
    getMenuItemResourceDataQuery(resourceIds),
    {
      variables: {},
      cache: storefront.CacheLong(),
    },
  );

  Object.keys(data).map((cleanResourceId) => {
    const resourceNode = data[cleanResourceId];

    const resourceId = resourceNode?.id;

    if (resourceId) {
      finalData[resourceId] = resourceNode;
    }

    return cleanResourceId;
  });

  return finalData;
}
