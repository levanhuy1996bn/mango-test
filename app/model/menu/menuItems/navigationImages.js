function getMenuItemPageMetafieldsNavigationImagesQuery(resourceIds) {
  if (!resourceIds) {
    return '';
  }

  return `#graphql
    query getMenuItemPageMetafieldsNavigationImages {
      ${resourceIds.map((resourceId) => {
        const cleanResourceId =
          'resource_' + resourceId.replace(/[^a-zA-Z0-9_-]/g, '_');

        if (-1 !== resourceId.indexOf('Page')) {
          return `#graphql
          ${cleanResourceId}: page(id: "${resourceId}") {
            id
            handle
            metafield(
              namespace: "custom"
              key: "navigation_images"
            ) {
              key
              reference {
                ... on MediaImage {
                  image {
                    id
                    url
                    width
                    height
                    altText
                  }
                }
              }
            }
          }
          `;
        } else if (-1 !== resourceId.indexOf('Blog')) {
          return gql`
          ${cleanResourceId}: blog(id: "${resourceId}") {
            id
            handle
            metafield(
              namespace: "custom"
              key: "navigation_images"
            ) {
              key
              reference {
                ... on MediaImage {
                  image {
                    id
                    url
                    width
                    height
                    altText
                  }
                }
              }
            }
          }
          `;
        }
        return '';
      })}
    }
  `;
}

export default async function getMenuItemPageMetafieldsNavigationImages({
  resourceIds,
  storefront,
}) {
  // accumulate data in a single object
  let finalData = {};

  if (!resourceIds) {
    return finalData;
  }

  const data = await storefront.query(
    getMenuItemPageMetafieldsNavigationImagesQuery(resourceIds),
    {
      variables: {},
      cache: storefront.CacheLong(),
    },
  );
  Object.keys(data).map((cleanResourceId) => {
    const resourceNodeRaw = data[cleanResourceId];

    const resourceId = resourceNodeRaw?.id;

    const resourceNode = {
      id: resourceId,
      handle: resourceNodeRaw.handle,
      title: resourceNodeRaw.title,
      image: resourceNodeRaw.metafield?.reference?.image
        ? resourceNodeRaw.metafield.reference.image
        : null,
    };

    if (resourceNode.image?.altText === '') {
      resourceNode.image.altText = null; // match the default value with collection image
    }

    if (resourceId) {
      finalData[resourceId] = resourceNode;
    }

    return cleanResourceId;
  });

  return finalData;
}
