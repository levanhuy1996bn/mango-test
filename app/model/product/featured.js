import {PRODUCT_GRID_CARD_FRAGMENT} from '~/lib/fragments';

export const InjectFeaturedProductData = async (props) => {
  const contentfulData = props?.data?.data;
  const storefront = props.storefront;

  const featuredProducts = contentfulData?.featuredProductsCollection?.items[0];

  const productIds = featuredProducts?.products;

  let productList = null;

  const data = await storefront.query(PRODUCT_QUERY_BY_IDS, {
    variables: {
      ids: generateQueryFeaturedProductIds(productIds),
    },
    cache: storefront.CacheLong(),
  });
  productList = data?.nodes;

  if (!contentfulData?.featuredProductsCollection?.items?.length) {
    return props?.data;
  }

  const newData = {...props?.data};

  newData.data.featuredProductsCollection.items[0].featuredProductData =
    productList;

  return newData;
};

export const InjectLandingPageFeaturedProductData = async (props) => {
  const contentfulData = props?.data?.data;
  const storefront = props.storefront;

  const landingPage = contentfulData?.landingPageCollection?.items[0];

  let featuredProductsIndex = undefined;
  let productIds = [];
  landingPage?.widgetsCollection?.items?.map((i, index) => {
    if (i?.__typename === 'FeaturedProducts') {
      featuredProductsIndex = index;
      productIds = i?.products;
    }
    return i;
  });

  let productList = null;

  const data = await storefront.query(PRODUCT_QUERY_BY_IDS, {
    variables: {
      ids: generateQueryFeaturedProductIds(productIds),
    },
    cache: storefront.CacheLong(),
  });
  productList = data?.nodes;

  if (
    !contentfulData?.landingPageCollection?.items?.length ||
    typeof featuredProductsIndex === 'undefined'
  ) {
    return props?.data;
  }

  const newData = {...props?.data};

  newData.data.landingPageCollection.items[0].widgetsCollection.items[
    featuredProductsIndex
  ].featuredProductData = productList;

  return newData;
};

const generateQueryFeaturedProductIds = (productIds) => {
  let newIds = [];

  productIds?.map((encodedProductId, index) => {
    newIds[index] = atob(encodedProductId); // the Contentful product ID values are base64 encoded

    return encodedProductId;
  });

  return newIds;
};

const PRODUCT_QUERY_BY_IDS = `#graphql
  ${PRODUCT_GRID_CARD_FRAGMENT}
  query productByIds($ids: [ID!]!) {
    nodes(ids: $ids) {
      ... on Product {
        ...ProductGridCard
      }
    }
  }
`;
