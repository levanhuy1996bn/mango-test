import {CacheLong, gql, useShopQuery} from '@shopify/hydrogen';

const PRODUCT_VARIANT_METAFIELDS_INVENTORY_ADMIN_QUERY = gql`
  query getProductVariantMetafieldsInventory(
    $productId: ID!
    $productVariantsEndCursor: String
    $productVariantsPerPage: Int!
    $productVariantsMetafieldsPerPage: Int!
  ) {
    productVariantMetafieldsInventory: product(id: $productId) {
      id
      variants(
        first: $productVariantsPerPage
        after: $productVariantsEndCursor
      ) {
        edges {
          node {
            id
            metafields(
              namespace: "inventory"
              first: $productVariantsMetafieldsPerPage
            ) {
              edges {
                node {
                  key
                  value
                }
              }
              pageInfo {
                hasNextPage
                endCursor
              }
            }
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;

export default function getProductVariantMetafieldsInventory({product}) {
  // accumulate data in a single object
  let finalData = {};

  // keep track of cursors that need to be queried after the primary set of results is retrieved
  let processVariantMetafieldsEndCursors = {};

  // configure the various page values
  const productVariantsPerPage = 2;
  const productVariantsMetafieldsPerPage = 10;
  const variantMetafieldsPerPage = 10;

  // configure parts of the final data structure
  const dataKey = 'inventory';
  const defaultValue = 0;
  const metafieldNamespace = 'inventory';

  // at the start, get the initial page of data
  let productVariantsHasNextPage = false;
  let productVariantsEndCursor = null;
  do {
    const primaryResults = useShopQuery({
      query: PRODUCT_VARIANT_METAFIELDS_INVENTORY_ADMIN_QUERY,
      variables: {
        productId: product?.id,
        productVariantsEndCursor,
        productVariantsPerPage,
        productVariantsMetafieldsPerPage,
      },
      cache: CacheLong(),
      preload: '*',
    });

    // if there is another page in the primary results, then it will be retrieved before moving on to the secondary results
    productVariantsHasNextPage =
      primaryResults?.data?.productVariantMetafieldsInventory?.variants
        ?.pageInfo?.hasNextPage || false;
    productVariantsEndCursor =
      primaryResults?.data?.productVariantMetafieldsInventory?.variants
        ?.pageInfo?.endCursor || null;

    // accumulate data from each variant
    primaryResults?.data?.productVariantMetafieldsInventory?.variants?.edges?.map(
      (variantEdge) => {
        const productVariantId = variantEdge?.node?.id;

        const variantMetafieldsHasNextPage =
          variantEdge?.node?.metafields?.pageInfo?.hasNextPage || false;
        const variantMetafieldsEndCursor =
          variantEdge?.node?.metafields?.pageInfo?.endCursor || null;

        // if there are more pages, add the cursors to be processed in the secondary results
        if (variantMetafieldsHasNextPage && variantMetafieldsEndCursor) {
          processVariantMetafieldsEndCursors[productVariantId] =
            variantMetafieldsEndCursor;
        }

        // accumulate the metafield data
        variantEdge?.node?.metafields?.edges?.map((metafieldEdge) => {
          const metafieldKey = metafieldEdge?.node?.key;
          const metafieldValue = metafieldEdge?.node?.value;
          if (!finalData[productVariantId]) {
            finalData[productVariantId] = {};
          }
          if (!finalData[productVariantId][dataKey]) {
            finalData[productVariantId][dataKey] = {};
          }
          finalData[productVariantId][dataKey][metafieldKey] =
            metafieldValue || defaultValue;

          return metafieldEdge;
        });

        return variantEdge;
      },
    );
  } while (productVariantsHasNextPage && productVariantsEndCursor);

  // if there is no additional data to process, then just return early
  if (!Object.keys(processVariantMetafieldsEndCursors).length) {
    return finalData;
  }

  // now retrieve the nested collection data
  let counter = 0;
  do {
    // generate the query with hardcoded values, using a unique ID per result
    let secondaryResultsQuery =
      'query getVariantMetafieldsInventory' + counter + ' {';

    Object.keys(processVariantMetafieldsEndCursors).map((variantId) => {
      const variantMetafieldsEndCursor =
        processVariantMetafieldsEndCursors[variantId];

      // as the variant IDs are gathered, remove them from the list so that the process will stop eventually
      delete processVariantMetafieldsEndCursors[variantId];

      const cleanVariantId =
        'variant_' + variantId.replace(/[^a-zA-Z0-9_-]/g, '_');

      secondaryResultsQuery +=
        '\n  ' + cleanVariantId + ': productVariant(id: "' + variantId + '") {';
      secondaryResultsQuery += '\n    id';
      secondaryResultsQuery += '\n    metafields(';
      secondaryResultsQuery +=
        '\n      namespace: "' + metafieldNamespace + '"';
      secondaryResultsQuery += '\n      first: ' + variantMetafieldsPerPage;
      secondaryResultsQuery +=
        '\n      after: "' + variantMetafieldsEndCursor + '"';
      secondaryResultsQuery += '\n    ) {';
      secondaryResultsQuery += '\n      edges {';
      secondaryResultsQuery += '\n        node {';
      secondaryResultsQuery += '\n          key';
      secondaryResultsQuery += '\n          value';
      secondaryResultsQuery += '\n        }';
      secondaryResultsQuery += '\n      }';
      secondaryResultsQuery += '\n      pageInfo {';
      secondaryResultsQuery += '\n        hasNextPage';
      secondaryResultsQuery += '\n        endCursor';
      secondaryResultsQuery += '\n      }';
      secondaryResultsQuery += '\n    }';
      secondaryResultsQuery += '\n  }';

      return variantId;
    });

    secondaryResultsQuery += '\n}';

    const secondaryResults = useShopQuery({
      query: secondaryResultsQuery,
      variables: {},
    });

    // accumulate the data from each variant
    const secondaryResultsData = secondaryResults?.data || {};
    Object.keys(secondaryResultsData).map((cleanVariantId) => {
      const variantNode = secondaryResultsData[cleanVariantId];

      const productVariantId = variantNode?.id;

      const variantMetafieldsHasNextPage =
        variantNode?.metafields?.pageInfo?.hasNextPage || false;
      const variantMetafieldsEndCursor =
        variantNode?.metafields?.pageInfo?.endCursor || null;

      // if there are more pages, add the cursors to be processed in the next loop
      if (variantMetafieldsHasNextPage && variantMetafieldsEndCursor) {
        processVariantMetafieldsEndCursors[productVariantId] =
          variantMetafieldsEndCursor;
      }

      // accumulate the metafield data
      variantNode?.metafields?.edges?.map((metafieldEdge) => {
        const metafieldKey = metafieldEdge?.node?.key;
        const metafieldValue = metafieldEdge?.node?.value;
        if (!finalData[productVariantId]) {
          finalData[productVariantId] = {};
        }
        if (!finalData[productVariantId][dataKey]) {
          finalData[productVariantId][dataKey] = {};
        }
        finalData[productVariantId][dataKey][metafieldKey] =
          metafieldValue || defaultValue;

        return metafieldEdge;
      });

      return cleanVariantId;
    });

    ++counter;
  } while (
    counter < 100 &&
    Object.keys(processVariantMetafieldsEndCursors).length
  );

  return finalData;
}
