import {CacheLong, gql, useShopQuery} from '@shopify/hydrogen';

function getProductVariantMetafieldsInventoryForLocationQuery(metafieldKeys) {
  return gql`
    query getProductVariantMetafieldsInventoryForLocation(
      $productId: ID!
      $productVariantsPerPage: Int!
      $productVariantsEndCursor: String
    ) {
      productVariantMetafieldsInventory: product(id: $productId) {
        id
        variants(
          first: $productVariantsPerPage
          after: $productVariantsEndCursor
        ) {
          edges {
            node {
              id
              ${metafieldKeys.map(
                (metafieldKey) =>
                  metafieldKey &&
                  gql`
              metafield_inventory_${metafieldKey}: metafield(namespace: "inventory", key: "${metafieldKey}") {
                key
                value
              }
              `,
              )}
            }
          }
          pageInfo {
            hasNextPage
            endCursor
          }
        }
      }
    }
  `;
}

export default function getProductVariantMetafieldsInventoryForLocation({
  product,
  locationIds,
}) {
  // accumulate data in a single object
  let finalData = {};

  if (!locationIds) {
    return finalData;
  }

  // configure the page values
  const productVariantsPerPage = 6;

  // configure parts of the final data structure
  const dataKey = 'inventory';
  const defaultValue = 0;

  // at the start, get the initial page of data
  let productVariantsHasNextPage = false;
  let productVariantsEndCursor = null;
  do {
    const primaryResults = useShopQuery({
      query: getProductVariantMetafieldsInventoryForLocationQuery(locationIds),
      variables: {
        productId: product?.id,
        productVariantsEndCursor,
        productVariantsPerPage,
      },
      cache: CacheLong(),
      preload: '*',
    });

    // if there is another page in the primary results, then it will be retrieved
    productVariantsHasNextPage =
      primaryResults?.data?.productVariantMetafieldsInventory?.variants
        ?.pageInfo?.hasNextPage || false;
    productVariantsEndCursor =
      primaryResults?.data?.productVariantMetafieldsInventory?.variants
        ?.pageInfo?.endCursor || null;

    // accumulate data from each variant
    primaryResults?.data?.productVariantMetafieldsInventory?.variants?.edges?.map(
      (variantEdge) => {
        const productVariantId = variantEdge?.node?.id;

        // accumulate the metafield data
        const variantNodeData = variantEdge?.node;
        Object.keys(variantNodeData).map((variantField) => {
          if (-1 !== variantField.indexOf('metafield_')) {
            const variantNode = variantNodeData[variantField];
            const metafieldKey = variantNode?.key;
            const metafieldValue = variantNode?.value;

            // skip undefined data
            if (!metafieldKey) {
              return variantField;
            }

            if (!finalData[productVariantId]) {
              finalData[productVariantId] = {};
            }
            if (!finalData[productVariantId][dataKey]) {
              finalData[productVariantId][dataKey] = {};
            }
            finalData[productVariantId][dataKey][metafieldKey] =
              parseInt(metafieldValue) || defaultValue;
          }
          return variantField;
        });

        return variantEdge;
      },
    );
  } while (productVariantsHasNextPage && productVariantsEndCursor);

  return finalData;
}
