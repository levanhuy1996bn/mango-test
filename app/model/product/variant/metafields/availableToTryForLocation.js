import {CacheLong, gql, useShopQuery} from '@shopify/hydrogen';

function getProductVariantMetafieldsAvailableToTryForLocationQuery(
  metafieldKeys,
) {
  return gql`
    query getProductVariantMetafieldsAvailableToTryForLocation(
      $productId: ID!
      $productVariantsPerPage: Int!
      $productVariantsEndCursor: String
    ) {
      productVariantMetafieldsAvailableToTry: product(id: $productId) {
        id
        variants(
          first: $productVariantsPerPage
          after: $productVariantsEndCursor
        ) {
          edges {
            node {
              id
              ${metafieldKeys.map(
                (metafieldKey) =>
                  metafieldKey &&
                  gql`
              metafield_available_to_try_${metafieldKey}: metafield(namespace: "available_to_try", key: "${metafieldKey}") {
                key
                value
              }
              `,
              )}
            }
          }
          pageInfo {
            hasNextPage
            endCursor
          }
        }
      }
    }
  `;
}

export default function getProductVariantMetafieldsAvailableToTryForLocation({
  product,
  locationIds,
}) {
  // accumulate data in a single object
  let finalData = {};

  if (!locationIds) {
    return finalData;
  }

  // configure the page values
  const productVariantsPerPage = 6;

  // configure parts of the final data structure
  const dataKey = 'availableToTry';
  const defaultValue = false;

  // at the start, get the initial page of data
  let productVariantsHasNextPage = false;
  let productVariantsEndCursor = null;
  do {
    const primaryResults = useShopQuery({
      query:
        getProductVariantMetafieldsAvailableToTryForLocationQuery(locationIds),
      variables: {
        productId: product?.id,
        productVariantsEndCursor,
        productVariantsPerPage,
      },
      cache: CacheLong(),
      preload: '*',
    });

    // if there is another page in the primary results, then it will be retrieved
    productVariantsHasNextPage =
      primaryResults?.data?.productVariantMetafieldsAvailableToTry?.variants
        ?.pageInfo?.hasNextPage || false;
    productVariantsEndCursor =
      primaryResults?.data?.productVariantMetafieldsAvailableToTry?.variants
        ?.pageInfo?.endCursor || null;

    // accumulate data from each variant
    primaryResults?.data?.productVariantMetafieldsAvailableToTry?.variants?.edges?.map(
      (variantEdge) => {
        const productVariantId = variantEdge?.node?.id;

        // accumulate the metafield data
        const variantNodeData = variantEdge?.node;
        Object.keys(variantNodeData).map((variantField) => {
          if (-1 !== variantField.indexOf('metafield_')) {
            const variantNode = variantNodeData[variantField];
            const metafieldKey = variantNode?.key;
            const metafieldValue = variantNode?.value;

            // skip undefined data
            if (!metafieldKey) {
              return variantField;
            }

            if (!finalData[productVariantId]) {
              finalData[productVariantId] = {};
            }
            if (!finalData[productVariantId][dataKey]) {
              finalData[productVariantId][dataKey] = {};
            }
            finalData[productVariantId][dataKey][metafieldKey] =
              (metafieldValue === 'true' ? true : false) || defaultValue;
          }
          return variantField;
        });

        return variantEdge;
      },
    );
  } while (productVariantsHasNextPage && productVariantsEndCursor);

  return finalData;
}
