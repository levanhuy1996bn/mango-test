export function fetch(url, options) {
  const {body} = options;
  // @see app/hooks/useAdminMutationInContext.jsx
  // @see app/hooks/useAdminQueryInContext.jsx
  if (
    !!body &&
    (-1 !== body.indexOf('isUseAdminMutation') ||
      -1 !== body.indexOf('isUseAdminQuery'))
  ) {
    url = url.replace('/api/', '/admin/api/');
    delete options.headers['X-SDK-Variant'];
    delete options.headers['X-SDK-Variant-Source'];
    delete options.headers['X-SDK-Version'];
    delete options.headers['Shopify-Storefront-Private-Token'];
    delete options.headers['X-Shopify-Storefront-Access-Token'];
    if (!options.headers?.['X-Shopify-Access-Token']) {
      // it seems the following variables will not usually be defined,
      // so this header should be provided in the main request when possible
      options.headers['X-Shopify-Access-Token'] =
        typeof Oxygen !== 'undefined'
          ? Oxygen?.env?.PRIVATE_SHOPIFY_ADMIN_API_TOKEN
          : typeof MiniOxygen !== 'undefined'
          ? MiniOxygen?.env?.PRIVATE_SHOPIFY_ADMIN_API_TOKEN
          : typeof process !== 'undefined'
          ? process?.env?.PRIVATE_SHOPIFY_ADMIN_API_TOKEN
          : '';
    }
  }

  return globalThis.fetch(url, options);
}
