import moment from 'moment';
import Carousel from 'nuka-carousel';

export function BlogFeaturesCarousel({articlesInfo2}) {
  if (!articlesInfo2) {
    return <></>;
  }
  return (
    <Carousel
      wrapAround={true}
      renderCenterLeftControls={({previousSlide, previousDisabled}) => (
        <button
          onClick={previousSlide}
          disabled={previousDisabled}
          className="bg-transparent border-none cursor-pointer disabled:cursor-not-allowed appearance-none flex items-center m-3 text-black opacity-70 hover:opacity-100 disabled:opacity-30 relative bottom-12 md:bottom-0"
          aria-label="Go to next slide"
        >
          <svg
            stroke="currentColor"
            fill="currentColor"
            strokeWidth="0"
            viewBox="0 0 16 16"
            height="32"
            width="32"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z" />
          </svg>
        </button>
      )}
      renderCenterRightControls={({nextSlide, nextDisabled}) => (
        <button
          onClick={nextSlide}
          disabled={nextDisabled}
          className="bg-transparent border-none cursor-pointer disabled:cursor-not-allowed appearance-none flex items-center m-3 text-black opacity-70 hover:opacity-100 disabled:opacity-30 relative bottom-12 md:bottom-0"
          aria-label="Go to previous slide"
        >
          <svg
            stroke="currentColor"
            fill="currentColor"
            strokeWidth="0"
            viewBox="0 0 16 16"
            height="32"
            width="32"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z" />
          </svg>
        </button>
      )}
      renderBottomCenterControls={({slideCount, currentSlide, goToSlide}) => (
        <div className="slider-control-bottomcenter">
          {[...Array(slideCount)].map((e, key) => (
            <div key={key} className="inline-block">
              <button
                type="button"
                aria-label="slide 1 bullet"
                onClick={() => goToSlide(key)}
              >
                <svg width="20" height="20">
                  <circle
                    cx="10"
                    cy="10"
                    r="5"
                    stroke={currentSlide == key ? '#000' : 'grey'}
                    strokeWidth="3"
                    fill={currentSlide == key ? '#000' : 'grey'}
                  />
                </svg>
              </button>
            </div>
          ))}
        </div>
      )}
    >
      {(Object.values(articlesInfo2) || []).map((article2, index) => (
        <div
          key={index}
          className="h-auto md:h-80 bg-gray-200 rounded-lg flex flex-col md:flex-row overflow-hidden mb-10"
        >
          <div className="md:w-2/4">
            <img
              className="object-cover h-80 w-full"
              alt="Featured Story"
              src={article2?.image?.url}
            />
          </div>
          <div className="md:w-2/4 py-2 px-4">
            <div className="text-2xl lg:text-3xl font-bold">
              {article2?.title}
            </div>
            <div className="text-xl lg:text-2xl py-5 pr-5">
              {article2?.content}
              <a
                className="block font-medium"
                href={`/blogs/${article2.blog?.handle?.toLowerCase()}/${article2.handle?.toLowerCase()}`}
              >
                Read More
              </a>
            </div>
            <div className="text-sm">
              {moment(article2?.publishedAt).format('MMMM DD, YYYY')}
            </div>
          </div>
        </div>
      ))}
    </Carousel>
  );
}
