import {Link} from '~/components';

export function BlogPagination({blogHandle, pageInfo}) {
  const previousPage = pageInfo?.hasPreviousPage || false;
  const nextPage = pageInfo?.hasNextPage || false;
  const startCursor = pageInfo?.startCursor || '';
  const endCursor = pageInfo?.endCursor || '';

  return (
    <nav>
      <div className="inline-flex space-x-2">
        {previousPage && (
          <div>
            <Link
              to={`/blogs/${blogHandle}?startCursor=${startCursor}`}
              className="border inline-block text-center bg-mw-red-500 text-white border-mw-red-500 hover:text-mw-red-500 hover:bg-white py-2 px-4 rounded-lg font-bold text-base"
            >
              Previous
            </Link>
          </div>
        )}
        {nextPage && (
          <div>
            <Link
              to={`/blogs/${blogHandle}?endCursor=${endCursor}`}
              className="border inline-block text-center bg-mw-red-500 text-white border-mw-red-500 hover:text-mw-red-500 hover:bg-white py-2 px-4 rounded-lg font-bold text-base"
            >
              Next
            </Link>
          </div>
        )}
      </div>
    </nav>
  );
}
