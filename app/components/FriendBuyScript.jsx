import {initializeFriendBuyScript} from '~/lib/friendBuy';
import {useEffect} from 'react';

export function FriendBuyScript() {
  useEffect(() => {
    const loadFriendBuyScript = () => {
      const script = initializeFriendBuyScript();
      document.body.appendChild(script);
    };

    if (document.readyState === 'complete') {
      loadFriendBuyScript();
    } else {
      window.addEventListener('load', loadFriendBuyScript);
    }

    return () => {
      window.removeEventListener('load', loadFriendBuyScript);
    };
  }, []);
}
