export function FourGrid({data}) {
  return (
    <>
      <div className="four-grid-container my-10">
        {data?.sectionTitle ? (
          <h2 className="mw-decorated-headline text-center font-bold leading-none mb-10 block">
            {data?.sectionTitle}
          </h2>
        ) : (
          ''
        )}
        <div className="grid grid-cols-2 md:grid-cols-4 gap-5">
          <div className="relative banner-1 w-full">
            {data?.banner1CtaUrl ? (
              <a
                href={data?.banner1CtaUrl}
                className="absolute top-0 left-0 right-0 bottom-0"
              >
                <span className="sr-only">{data?.title} Link 1</span>
              </a>
            ) : (
              ''
            )}
            {data?.banner1Desktop ? (
              <img
                src={data?.banner1Desktop?.url}
                title={data?.banner1Desktop?.title}
                alt={data?.banner1Desktop?.title}
                width={data?.banner1Desktop?.width}
                height={data?.banner1Desktop?.height}
                className="w-full hidden lg:block"
              />
            ) : (
              ''
            )}
            {data?.banner1Mobile ? (
              <img
                src={data?.banner1Mobile?.url}
                title={data?.banner1Mobile?.title}
                alt={data?.banner1Mobile?.title}
                width={data?.banner1Mobile?.width}
                height={data?.banner1Mobile?.height}
                className="w-full block lg:hidden"
              />
            ) : (
              ''
            )}
          </div>
          <div className="relative banner-2 w-full">
            {data?.banner2CtaUrl ? (
              <a
                href={data?.banner2CtaUrl}
                className="absolute top-0 left-0 right-0 bottom-0"
              >
                <span className="sr-only">{data?.title} Link 2</span>
              </a>
            ) : (
              ''
            )}
            {data?.banner2Desktop ? (
              <img
                src={data?.banner2Desktop?.url}
                title={data?.banner2Desktop?.title}
                alt={data?.banner2Desktop?.title}
                width={data?.banner2Desktop?.width}
                height={data?.banner2Desktop?.height}
                className="w-full hidden lg:block"
              />
            ) : (
              ''
            )}
            {data?.banner2Mobile ? (
              <img
                src={data?.banner2Mobile?.url}
                title={data?.banner2Mobile?.title}
                alt={data?.banner2Mobile?.title}
                width={data?.banner2Mobile?.width}
                height={data?.banner2Mobile?.height}
                className="w-full block lg:hidden"
              />
            ) : (
              ''
            )}
          </div>
          <div className="relative banner-3 w-full">
            {data?.banner3CtaUrl ? (
              <a
                href={data?.banner3CtaUrl}
                className="absolute top-0 left-0 right-0 bottom-0"
              >
                <span className="sr-only">{data?.title} Link 3</span>
              </a>
            ) : (
              ''
            )}
            {data?.banner3Desktop ? (
              <img
                src={data?.banner3Desktop?.url}
                title={data?.banner3Desktop?.title}
                alt={data?.banner3Desktop?.title}
                width={data?.banner3Desktop?.width}
                height={data?.banner3Desktop?.height}
                className="w-full hidden lg:block"
              />
            ) : (
              ''
            )}
            {data?.banner3Mobile ? (
              <img
                src={data?.banner3Mobile?.url}
                title={data?.banner3Mobile?.title}
                alt={data?.banner3Mobile?.title}
                width={data?.banner3Mobile?.width}
                height={data?.banner3Mobile?.height}
                className="w-full block lg:hidden"
              />
            ) : (
              ''
            )}
          </div>
          <div className="relative banner-4 w-full">
            {data?.banner4CtaUrl ? (
              <a
                href={data?.banner4CtaUrl}
                className="absolute top-0 left-0 right-0 bottom-0"
              >
                <span className="sr-only">{data?.title} Link 4</span>
              </a>
            ) : (
              ''
            )}
            {data?.banner4Desktop ? (
              <img
                src={data?.banner4Desktop?.url}
                title={data?.banner4Desktop?.title}
                alt={data?.banner4Desktop?.title}
                width={data?.banner4Desktop?.width}
                height={data?.banner4Desktop?.height}
                className="w-full hidden lg:block"
              />
            ) : (
              ''
            )}
            {data?.banner4Mobile ? (
              <img
                src={data?.banner4Mobile?.url}
                title={data?.banner4Mobile?.title}
                alt={data?.banner4Mobile?.title}
                width={data?.banner4Mobile?.width}
                height={data?.banner4Mobile?.height}
                className="w-full block lg:hidden"
              />
            ) : (
              ''
            )}
          </div>
        </div>
      </div>
    </>
  );
}
