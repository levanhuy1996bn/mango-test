// import {Carousel} from 'flowbite-react';
import Carousel from 'nuka-carousel';

import {MWMultiImageSlide} from './MultiImageSlide';

export function MWImageCarousel({imageCarousel}) {
  if (!imageCarousel?.slidesCollection?.items?.length) {
    return <></>;
  }

  const isSingleSlide = imageCarousel?.slidesCollection?.items?.length === 1;
  const params = isSingleSlide
    ? {
        defaultControlsConfig: {
          prevButtonText: '',
          prevButtonStyle: {
            display: 'none',
          },
          nextButtonText: '',
          nextButtonStyle: {
            display: 'none',
          },
          pagingDotsStyle: {
            display: 'none',
          },
        },
      }
    : {
        autoplay: true,
        autoplayInterval: 3200,
        speed: 1000,
        wrapAround: true,
        defaultControlsConfig: {
          prevButtonText: '',
          prevButtonStyle: {
            display: 'none',
          },
          nextButtonText: '',
          nextButtonStyle: {
            display: 'none',
          },
          pagingDotsContainerClassName: 'mw-image-carousel__paging-dots',
        },
      };

  return (
    <Carousel {...params}>
      {imageCarousel?.slidesCollection?.items?.map((i) => {
        return <MWMultiImageSlide key={i?.title} multiImageSlide={i} />;
      })}
    </Carousel>
  );
}
