import {Link} from '~/components';

export function BannerSingleCta({data}) {
  function renderBanner(data) {
    return (
      <>
        <div className="absolute flex-initial py-5 px-5 sm:py-7 sm:px-10 lg:py-10 lg:px-20 flex flex-col justify-center gap-4 lg:gap-6">
          {data?.foregroundImage ? (
            <img
              src={data?.foregroundImage?.url}
              title={data?.foregroundImage?.title}
              alt={data?.foregroundImage?.title}
              width={data?.foregroundImage?.width}
              height={data?.foregroundImage?.height}
              className="mw-foreground-image w-auto md:max-w-md lg:max-w-xl"
            />
          ) : (
            <div>foregroundImage</div>
          )}
          <div className="text-center">
            <div
              className={`${
                data?.inverseCtaButtonColors ? 'mw-inverse' : ''
              } inline-block font-bold rounded-lg whitespace-nowrap bg-mw-red-500 text-white px-7 lg:px-9 py-3 lg:py-4 text-sm lg:text-lg`}
            >
              {data?.ctaButtonLabel}
            </div>
          </div>
        </div>
        {data?.backgroundImageMobile ? (
          <img
            src={data?.backgroundImageMobile?.url}
            title={data?.backgroundImageMobile?.title}
            alt={data?.backgroundImageMobile?.title}
            width={data?.backgroundImageMobile?.width}
            height={data?.backgroundImageMobile?.height}
            className="mw-background-mobile w-auto"
          />
        ) : (
          <div>backgroundImageMobile</div>
        )}
        {data?.backgroundImageDesktop ? (
          <img
            src={data?.backgroundImageDesktop?.url}
            title={data?.backgroundImageDesktop?.title}
            alt={data?.backgroundImageDesktop?.title}
            width={data?.backgroundImageDesktop?.width}
            height={data?.backgroundImageDesktop?.height}
            className="mw-background-desktop w-auto"
          />
        ) : (
          <div>backgroundImageDesktop</div>
        )}
      </>
    );
  }

  const bannerLink = data?.ctaUrl
    ? data?.ctaUrl
    : data?.link
    ? data?.link
    : null;
  return bannerLink ? (
    <Link
      to={bannerLink}
      className="mb-5 relative flex justify-center items-start"
    >
      {renderBanner(data)}
    </Link>
  ) : (
    <div className="mb-5 relative flex justify-center items-start">
      {renderBanner(data)}
    </div>
  );
}
