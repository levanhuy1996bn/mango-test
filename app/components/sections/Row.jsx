import {MWPromoStack} from './PromoStack';
import {MWSlider} from './Slider';

export function MWRow({row}) {
  const rowOutput = row?.widgetsCollection?.items?.map((i) => {
    if (i?.__typename === 'PromoStack') {
      return (
        <div
          className="w-auto md:w-full overflow-x-scroll whitespace-nowrap md:overflow-auto md:whitespace-normal p-0 flex md:flex-col gap-3 md:gap-5"
          key={i?.slug}
        >
          <MWPromoStack promoCardsCollection={i} />
        </div>
      );
    }
    if (i?.__typename === 'Slideshow') {
      return (
        <div className="w-full h-full mw-slider-container p-0" key={i?.slug}>
          <MWSlider slider={i} />
        </div>
      );
    }
  });
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 gap-5 pt-5 mb-5 md:mb-10">
      {rowOutput}
    </div>
  );
}
