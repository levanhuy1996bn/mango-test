import {documentToReactComponents} from '@contentful/rich-text-react-renderer';
import {Link} from '~/components';

export function MWNarrowBanner({narrowBanner}) {
  const bannerOneCtaUrl = narrowBanner?.banner1CtaUrl
    ? narrowBanner?.banner1CtaUrl
    : narrowBanner?.cta1Url
    ? narrowBanner?.cta1Url
    : null;
  const bannerTwoCtaUrl = narrowBanner?.banner2CtaUrl
    ? narrowBanner?.banner2CtaUrl
    : narrowBanner?.cta2Url
    ? narrowBanner?.cta2Url
    : null;
  return (
    <div className="mw-narrow-banner flex flex-row md:flex-nowrap gap-0 mb-10">
      <div
        className="basis-1/3 md:basis-1/4 mw-left-column bg-no-repeat bg-contain bg-bottom"
        style={{
          backgroundImage: 'url(' + narrowBanner?.image?.url + ')',
        }}
      ></div>
      <div className="basis-2/3 md:basis-3/4 mw-right-column p-5">
        <div className="mw-foreground-html-block text-center">
          <div className="mw-narrow-banner-headline text-xl md:text-3xl font-bold mb-3">
            {documentToReactComponents(narrowBanner?.headline?.json)}
          </div>
          <div className="narrow-banner-links">
            {(() => {
              if (bannerOneCtaUrl) {
                if (bannerOneCtaUrl === 'open_chat') {
                  return (
                    <button className="mw-inline-link js-toggle-chat">
                      {narrowBanner?.cta1Text}
                    </button>
                  );
                }
                if (bannerOneCtaUrl === 'open_store_locator') {
                  return (
                    <button className="mw-inline-link js-toggle-store-locator">
                      {narrowBanner?.cta1Text}
                    </button>
                  );
                }
                return (
                  <Link to={bannerOneCtaUrl} className="mw-inline-link">
                    {narrowBanner?.cta1Text}
                  </Link>
                );
              }
            })()}
            {(() => {
              if (bannerTwoCtaUrl) {
                if (bannerTwoCtaUrl === 'open_chat') {
                  return (
                    <button className="mw-inline-link js-toggle-chat">
                      {narrowBanner?.cta2Text}
                    </button>
                  );
                }
                if (bannerTwoCtaUrl === 'open_store_locator') {
                  return (
                    <button className="mw-inline-link js-toggle-store-locator">
                      {narrowBanner?.cta2Text}
                    </button>
                  );
                }
                return (
                  <Link to={bannerTwoCtaUrl} className="mw-inline-link">
                    {narrowBanner?.cta2Text}
                  </Link>
                );
              }
            })()}
          </div>
        </div>
      </div>
    </div>
  );
}
