import {Link} from '~/components';

import {MWButton} from './Button';

export function MWButtonGrid({buttonGrid}) {
  return (
    <>
      <div className="mw-button-grid grid gap-5 grid-cols-2 md:grid-cols-3 px-8 lg:px-0">
        {buttonGrid?.buttonsCollection?.items?.map((i) => {
          return <MWButton key={i?.slug} button={i} />;
        })}
      </div>
      <div className="px-3 text-center">
        {(() => {
          if (buttonGrid?.callToActionUrl) {
            return (
              <Link
                to={buttonGrid?.callToActionUrl}
                className="mw-cms-button block md:inline-block mx-auto rounded-lg font-bold text-center p-6 border-2 md:underline md:border-0 border-mw-red-500 text-mw-red-500 md:text-black text-3xl md:text-xl hover:bg-mw-red-500 md:hover:bg-white hover:text-white md:hover:text-mw-red-500"
              >
                {buttonGrid?.callToActionText}
              </Link>
            );
          }
        })()}
      </div>
    </>
  );
}
