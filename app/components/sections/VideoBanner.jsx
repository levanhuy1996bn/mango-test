import {Link} from '~/components';

export function VideoBanner({data}) {
  const videoLink = data?.ctaUrl
    ? data?.ctaUrl
    : data?.link
    ? data?.link
    : null;
  return videoLink ? (
    <Link to={videoLink} className="inline-block mb-5">
      <video
        width="1920"
        height="870"
        controls={false}
        muted
        loop
        autoPlay
        poster={data?.poster?.url}
        src={data?.video?.url}
      />
    </Link>
  ) : (
    <div className="inline-block mb-5">
      <video
        width="1920"
        height="870"
        controls={false}
        muted
        loop
        autoPlay
        poster={data?.poster?.url}
        src={data?.video?.url}
      />
    </div>
  );
}
