export function TwoGrid({data}) {
  return (
    <>
      <div className="two-grid-container my-10">
        {data?.sectionTitle ? (
          <h2 className="mw-decorated-headline text-center font-bold leading-none mb-10 block">
            {data?.sectionTitle}
          </h2>
        ) : (
          ''
        )}
        <div className="grid grid-cols-1 md:grid-cols-2 gap-5">
          <div className="relative banner-1 w-full">
            {data?.banner1CtaUrl ? (
              <a
                href={data?.banner1CtaUrl}
                className="absolute top-0 left-0 right-0 bottom-0"
              >
                <span className="sr-only">{data?.title} Link 1</span>
              </a>
            ) : (
              ''
            )}
            {data?.banner1Desktop ? (
              <img
                src={data?.banner1Desktop?.url}
                title={data?.banner1Desktop?.title}
                alt={data?.banner1Desktop?.title}
                width={data?.banner1Desktop?.width}
                height={data?.banner1Desktop?.height}
                className="w-full hidden lg:block"
              />
            ) : (
              ''
            )}
            {data?.banner1Mobile ? (
              <img
                src={data?.banner1Mobile?.url}
                title={data?.banner1Mobile?.title}
                alt={data?.banner1Mobile?.title}
                width={data?.banner1Mobile?.width}
                height={data?.banner1Mobile?.height}
                className="w-full block lg:hidden"
              />
            ) : (
              ''
            )}
          </div>
          <div className="relative banner-2 w-full">
            {data?.banner2CtaUrl ? (
              <a
                href={data?.banner2CtaUrl}
                className="absolute top-0 left-0 right-0 bottom-0"
              >
                <span className="sr-only">{data?.title} Link 2</span>
              </a>
            ) : (
              ''
            )}
            {data?.banner2Desktop ? (
              <img
                src={data?.banner2Desktop?.url}
                title={data?.banner2Desktop?.title}
                alt={data?.banner2Desktop?.title}
                width={data?.banner2Desktop?.width}
                height={data?.banner2Desktop?.height}
                className="w-full hidden lg:block"
              />
            ) : (
              ''
            )}
            {data?.banner2Mobile ? (
              <img
                src={data?.banner2Mobile?.url}
                title={data?.banner2Mobile?.title}
                alt={data?.banner2Mobile?.title}
                width={data?.banner2Mobile?.width}
                height={data?.banner2Mobile?.height}
                className="w-full block lg:hidden"
              />
            ) : (
              ''
            )}
          </div>
        </div>
      </div>
    </>
  );
}
