import {documentToReactComponents} from '@contentful/rich-text-react-renderer';

import {FeaturedProductCard} from './FeaturedProductCard';

export function MWFeaturedProducts({featuredProducts}) {
  /*
    featuredProducts = {
      __typename: 'FeaturedProducts',
      title: 'Launch Featured Products',
      slug: 'launch-featured-products',
      layoutStyle: true,
      showPrices: true,
      showPromoBadges: true,
      showRatingSummaries: false,
      callToActionUrl: 'https://www.google.com/',
      callToActionText: 'See All Sale Products',
      products: [
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTI4MTcxMjM1ODU=',
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTI4MTY5NTk3NDU=',
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTI4MTY2MzIwNjU=',
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTIxMTA3NDM4MDk=',
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTIxMTA0NDg4OTc=',
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTIxMTAxNTM5ODU=',
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTIxMDczMzU5Mzc=',
        'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTE4OTUwMzIwNjU='
      ],
      featuredProductData: [
        { id: ... },
        { id: ... },
        ...
      ]
    }

    // the featuredProductData is appended by the local system and is not part of Contentful

    const encodedProductId = 'Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0Lzc5OTI4MTcxMjM1ODU=';
    const productId = atob(encodedProductId);  // productId is decoded value:   gid://shopify/Product/7992817123585
  */

  //console.log('featured', featuredProducts);

  return (
    <div>
      <div className="mw-featured-products-headline text-center font-bold leading-none text-4xl">
        {documentToReactComponents(featuredProducts?.headline?.json)}
      </div>
      <div
        className={
          featuredProducts?.layoutStyle === true
            ? 'grid-flow-row grid gap-3 grid-cols-1 sm:grid-cols-2 lg:grid-cols-4'
            : 'mw-featured-products-gallery'
        }
      >
        <div className="grid grid-flow-col grid-cols-[repeat(12,_minmax(200px,_1fr))] md:grid-cols-[repeat(12,_minmax(230px,_1fr))] lg:grid-cols-[repeat(12,_minmax(300px,_1fr))] gap-5">
          {featuredProducts?.featuredProductData?.map((productData, index) => {
            return (
              <div
                key={'featured_product_' + index}
                className="bg-white flex border border-mw-grayscale-300 rounded-lg hover:shadow-md"
              >
                <FeaturedProductCard
                  product={productData}
                  // loading={getImageLoadingPriority(i)}
                />
              </div>
              // <ProductGridCard product={productData} key={'featured_product_' + index} />
            );
          })}
          {}
        </div>
      </div>
    </div>
  );
}
