import moment from 'moment-timezone';

export function FeaturedProductDiscounts({applicableDiscounts}) {
  const renderProductDiscounts = () => {
    let discountHtml = [];
    let validDiscount = false;
    if (applicableDiscounts) {
      const discountValues = JSON.parse(applicableDiscounts.value || '[]');
      discountValues.map((item, index) => {
        const endsAt = item.endsAt
          ? moment.tz(item.endsAt, item.timezone)
          : null;
        const startsAt = item.startsAt
          ? moment.tz(item.startsAt, item.timezone)
          : null;
        const nowDate = moment.tz(moment(), item.timezone);
        if (
          !validDiscount &&
          (!endsAt || endsAt >= nowDate) &&
          startsAt &&
          startsAt <= nowDate
        ) {
          validDiscount = true;
          discountHtml.push(
            <div
              key={index}
              className="text-mw-red-500 text-xs bg-white font-bold border border-mw-red-500 py-1 px-3 w-fit rounded mb-1.5"
            >
              {item.discountTitle}
            </div>,
          );
        }
      });
    }

    return discountHtml;
  };

  return renderProductDiscounts();
}
