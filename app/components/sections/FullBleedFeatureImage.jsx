import {documentToReactComponents} from '@contentful/rich-text-react-renderer';

export function MWFullBleedFeatureImage({featureImage}) {
  // const backgroundUrl = window.innerWidth >= 768 ? featureImage.backgroundImageDesktop.url : featureImage.backgroundImageMobile.url;
  // const backgroundImage = 'image-set("' + featureImage.backgroundImageMobile.url + '" 1x, "' + featureImage.backgroundImageDesktop.url + '" 2x)';
  return featureImage?.foregroundAlignment === 'Left' ||
    featureImage?.foregroundAlignment === 'Right' ? (
    <div
      className={
        'mw-full-bleed-feature-image mb-10 relative flex aspect-[3/1.8] md:aspect-video ' +
        (featureImage?.foregroundAlignment === 'Left'
          ? 'justify-start'
          : 'justify-end')
      }
    >
      <div className="mw-foreground-content relative flex-initial p-5 md:p-10 w-[55%] md:w-1/2 flex flex-col justify-center">
        <img
          src={featureImage?.foregroundImage?.url}
          title={featureImage?.foregroundImage?.title}
          alt={featureImage?.foregroundImage?.title}
          width={featureImage?.foregroundImage?.width}
          height={featureImage?.foregroundImage?.height}
          className="mw-foreground-image w-auto"
        />
        <div className="mw-foreground-html-block text-center">
          {documentToReactComponents(featureImage?.foregroundHtmlBlock?.json)}
        </div>
      </div>
      <div
        className="mw-background-mobile h-full w-full bg-no-repeat bg-cover bg-center absolute top-0 left-0"
        style={{
          backgroundImage:
            'url(' + featureImage?.backgroundImageMobile?.url + ')',
        }}
      />
      <div
        className="mw-background-desktop h-full w-full bg-no-repeat bg-cover bg-center absolute top-0 left-0"
        style={{
          backgroundImage:
            'url(' + featureImage?.backgroundImageDesktop?.url + ')',
        }}
      />
    </div>
  ) : (
    <div className="mw-full-bleed-feature-image mb-10 relative flex justify-center items-start">
      <div
        className={`${
          featureImage?.inverseCtaButtonColors && 'mw-inverse'
        } mw-foreground-content absolute flex-initial py-5 px-5 sm:py-7 sm:px-10 lg:py-10 lg:px-20 flex flex-col justify-center`}
      >
        <img
          src={featureImage?.foregroundImage?.url}
          title={featureImage?.foregroundImage?.title}
          alt={featureImage?.foregroundImage?.title}
          width={featureImage?.foregroundImage?.width}
          height={featureImage?.foregroundImage?.height}
          className="mw-foreground-image w-auto md:max-w-md lg:max-w-xl"
        />
        <div className="mw-foreground-html-block text-center">
          {documentToReactComponents(featureImage?.foregroundHtmlBlock?.json)}
        </div>
      </div>
      <img
        src={featureImage?.backgroundImageMobile?.url}
        title={featureImage?.backgroundImageMobile?.title}
        alt={featureImage?.backgroundImageMobile?.title}
        width={featureImage?.backgroundImageMobile?.width}
        height={featureImage?.backgroundImageMobile?.height}
        className="mw-background-mobile w-auto"
      />
      <img
        src={featureImage?.backgroundImageDesktop?.url}
        title={featureImage?.backgroundImageDesktop?.title}
        alt={featureImage?.backgroundImageDesktop?.title}
        width={featureImage?.backgroundImageDesktop?.width}
        height={featureImage?.backgroundImageDesktop?.height}
        className="mw-background-desktop w-auto"
      />
    </div>
  );
}
