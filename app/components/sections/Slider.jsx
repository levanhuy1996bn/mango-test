import {documentToReactComponents} from '@contentful/rich-text-react-renderer';
import {Carousel} from 'flowbite-react';

export function MWSlider({slider}) {
  if (!slider?.slidesCollection?.items?.length) {
    return <></>;
  }
  return (
    <Carousel
      indicators={true}
      slide={true}
      leftControl={<div className="hidden" />}
      rightControl={<div className="hidden" />}
    >
      {slider?.slidesCollection?.items?.map((i, index) => {
        return (
          <div
            className={
              'mw-slide h-full mx-auto mb-5 md:mb-10 relative flex ' +
              (i?.foregroundAlignment === 'Left'
                ? 'justify-start'
                : i?.foregroundAlignment === 'Right'
                ? 'justify-end'
                : 'justify-center')
            }
            key={index}
          >
            <div className="mw-foreground-content relative flex-initial p-5 md:pt-10 w-4/5 md:w-1/2">
              <img
                src={i?.foregroundImage?.url}
                title={i?.foregroundImage?.title}
                width={i?.foregroundImage?.width}
                height={i?.foregroundImage?.height}
                alt={i?.foregroundImage?.title}
                className="mw-foreground-image lg:w-auto mb-2.5 md:mb-5 mx-auto"
              />
              <div className="mw-foreground-html-block text-center">
                {documentToReactComponents(i?.foregroundHtmlBlock?.json)}
              </div>
            </div>
            <div
              className="mw-background-mobile h-full w-full bg-no-repeat bg-cover bg-center absolute top-0 left-0"
              style={{
                backgroundImage: 'url(' + i?.backgroundImageMobile?.url + ')',
              }}
            />
            <div
              className="mw-background-desktop h-full w-full bg-no-repeat bg-cover bg-center absolute top-0 left-0"
              style={{
                backgroundImage: 'url(' + i?.backgroundImageDesktop?.url + ')',
              }}
            />
          </div>
        );
      })}
    </Carousel>
  );
}
