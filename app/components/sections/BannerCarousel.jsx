import {Carousel} from 'flowbite-react';

// import {MWMultiImageSlide} from './MultiImageSlide';
import {BannerSingleCta} from './BannerSingleCta';

export function BannerCarousel({data}) {
  if (!data?.bannersCollection?.items?.length) {
    return <></>;
  }
  return (
    <Carousel
      indicators={data?.bannersCollection?.items?.length === 1 ? false : true}
      slide={true}
      leftControl={<div className="hidden"></div>}
      rightControl={<div className="hidden"></div>}
    >
      {data?.bannersCollection?.items?.map((i) => {
        return <BannerSingleCta key={i?.title} data={i} />;
      })}
    </Carousel>
  );
}
