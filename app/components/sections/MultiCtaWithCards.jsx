import {Link} from '~/components';

export function MultiCtaWithCards({data}) {
  const cardBadgeLink = data?.badgeCtaUrl
    ? data?.badgeCtaUrl
    : data?.badgeLink
    ? data?.badgeLink
    : null;
  const cardCtaOneLink = data?.card1CtaUrl
    ? data?.card1CtaUrl
    : data?.ctaOneLink
    ? data?.ctaOneLink
    : null;
  const cardCtaTwoLink = data?.card2CtaUrl
    ? data?.card2CtaUrl
    : data?.ctaTwoLink
    ? data?.ctaTwoLink
    : null;
  const cardCtaThreeLink = data?.card3CtaUrl
    ? data?.card3CtaUrl
    : data?.ctaThreeLink
    ? data?.ctaThreeLink
    : null;
  return (
    <div className="mb-5 relative flex justify-center items-start">
      <div className="block md:hidden relative">
        <div className="absolute flex flex-col justify-center w-full px-6">
          {cardBadgeLink ? (
            data?.badgeImageMobile ? (
              <Link to={cardBadgeLink} className="flex justify-center">
                <img
                  src={data?.badgeImageMobile?.url}
                  title={data?.badgeImageMobile?.title}
                  alt={data?.badgeImageMobile?.title}
                  width={data?.badgeImageMobile?.width}
                  height={data?.badgeImageMobile?.height}
                  className="w-auto"
                />
              </Link>
            ) : (
              <Link
                to={cardBadgeLink}
                className="flex justify-center items-center p-3"
              >
                badgeImageMobile
              </Link>
            )
          ) : data?.badgeImageMobile ? (
            <img
              src={data?.badgeImageMobile?.url}
              title={data?.badgeImageMobile?.title}
              alt={data?.badgeImageMobile?.title}
              width={data?.badgeImageMobile?.width}
              height={data?.badgeImageMobile?.height}
              className="w-auto"
            />
          ) : (
            <div className="flex justify-center items-center p-3">
              badgeImageMobile
            </div>
          )}
          <div className="grid grid-cols-3">
            {cardCtaOneLink ? (
              data?.ctaOneImage ? (
                <Link to={cardCtaOneLink}>
                  <img
                    src={data?.ctaOneImage?.url}
                    title={data?.ctaOneImage?.title}
                    alt={data?.ctaOneImage?.title}
                    width={data?.ctaOneImage?.width}
                    height={data?.ctaOneImage?.height}
                    className="w-auto"
                  />
                </Link>
              ) : (
                <Link
                  to={cardCtaOneLink}
                  className="flex justify-center items-center p-3"
                >
                  ctaOneImage
                </Link>
              )
            ) : data?.ctaOneImage ? (
              <img
                src={data?.ctaOneImage?.url}
                title={data?.ctaOneImage?.title}
                alt={data?.ctaOneImage?.title}
                width={data?.ctaOneImage?.width}
                height={data?.ctaOneImage?.height}
                className="w-auto"
              />
            ) : (
              <div className="flex justify-center items-center p-3">
                ctaOneImage
              </div>
            )}
            {cardCtaTwoLink ? (
              data?.ctaTwoImage ? (
                <Link to={cardCtaTwoLink}>
                  <img
                    src={data?.ctaTwoImage?.url}
                    title={data?.ctaTwoImage?.title}
                    alt={data?.ctaTwoImage?.title}
                    width={data?.ctaTwoImage?.width}
                    height={data?.ctaTwoImage?.height}
                    className="w-auto"
                  />
                </Link>
              ) : (
                <Link
                  to={cardCtaTwoLink}
                  className="flex justify-center items-center p-3"
                >
                  ctaTwoImage
                </Link>
              )
            ) : data?.ctaTwoImage ? (
              <img
                src={data?.ctaTwoImage?.url}
                title={data?.ctaTwoImage?.title}
                alt={data?.ctaTwoImage?.title}
                width={data?.ctaTwoImage?.width}
                height={data?.ctaTwoImage?.height}
                className="w-auto"
              />
            ) : (
              <div className="flex justify-center items-center p-3">
                ctaTwoImage
              </div>
            )}
            {cardCtaThreeLink ? (
              data?.ctaThreeImage ? (
                <Link to={cardCtaThreeLink}>
                  <img
                    src={data?.ctaThreeImage?.url}
                    title={data?.ctaThreeImage?.title}
                    alt={data?.ctaThreeImage?.title}
                    width={data?.ctaThreeImage?.width}
                    height={data?.ctaThreeImage?.height}
                    className="w-auto"
                  />
                </Link>
              ) : (
                <Link
                  to={cardCtaThreeLink}
                  className="flex justify-center items-center p-3"
                >
                  ctaThreeImage
                </Link>
              )
            ) : data?.ctaThreeImage ? (
              <img
                src={data?.ctaThreeImage?.url}
                title={data?.ctaThreeImage?.title}
                alt={data?.ctaThreeImage?.title}
                width={data?.ctaThreeImage?.width}
                height={data?.ctaThreeImage?.height}
                className="w-auto"
              />
            ) : (
              <div className="flex justify-center items-center p-3">
                ctaThreeImage
              </div>
            )}
          </div>
        </div>
        {data?.backgroundImageMobile ? (
          <img
            src={data?.backgroundImageMobile?.url}
            title={data?.backgroundImageMobile?.title}
            alt={data?.backgroundImageMobile?.title}
            width={data?.backgroundImageMobile?.width}
            height={data?.backgroundImageMobile?.height}
            className="w-auto"
          />
        ) : (
          <div className="flex justify-center items-center p-3">
            backgroundImageMobile
          </div>
        )}
      </div>
      <div className="hidden md:block relative">
        <div className="absolute flex justify-center w-full px-8 lg:px-12">
          <div className="basis-1/3">
            {cardBadgeLink ? (
              data?.badgeImageDesktop ? (
                <Link to={cardBadgeLink}>
                  <img
                    src={data?.badgeImageDesktop?.url}
                    title={data?.badgeImageDesktop?.title}
                    alt={data?.badgeImageDesktop?.title}
                    width={data?.badgeImageDesktop?.width}
                    height={data?.badgeImageDesktop?.height}
                    className="w-auto"
                  />
                </Link>
              ) : (
                <Link
                  to={cardBadgeLink}
                  className="flex justify-center items-center p-3"
                >
                  badgeImageDesktop
                </Link>
              )
            ) : data?.badgeImageDesktop ? (
              <img
                src={data?.badgeImageDesktop?.url}
                title={data?.badgeImageDesktop?.title}
                alt={data?.badgeImageDesktop?.title}
                width={data?.badgeImageDesktop?.width}
                height={data?.badgeImageDesktop?.height}
                className="w-auto"
              />
            ) : (
              <div className="flex justify-center items-center p-3">
                badgeImageDesktop
              </div>
            )}
          </div>
          <div className="basis-2/3 grid grid-cols-3 self-end">
            {cardCtaOneLink ? (
              data?.ctaOneImage ? (
                <Link to={cardCtaOneLink}>
                  <img
                    src={data?.ctaOneImage?.url}
                    title={data?.ctaOneImage?.title}
                    alt={data?.ctaOneImage?.title}
                    width={data?.ctaOneImage?.width}
                    height={data?.ctaOneImage?.height}
                    className="w-auto"
                  />
                </Link>
              ) : (
                <Link
                  to={cardCtaOneLink}
                  className="flex justify-center items-center p-3"
                >
                  ctaOneImage
                </Link>
              )
            ) : data?.ctaOneImage ? (
              <img
                src={data?.ctaOneImage?.url}
                title={data?.ctaOneImage?.title}
                alt={data?.ctaOneImage?.title}
                width={data?.ctaOneImage?.width}
                height={data?.ctaOneImage?.height}
                className="w-auto"
              />
            ) : (
              <div className="flex justify-center items-center p-3">
                ctaOneImage
              </div>
            )}
            {cardCtaTwoLink ? (
              data?.ctaTwoImage ? (
                <Link to={cardCtaTwoLink}>
                  <img
                    src={data?.ctaTwoImage?.url}
                    title={data?.ctaTwoImage?.title}
                    alt={data?.ctaTwoImage?.title}
                    width={data?.ctaTwoImage?.width}
                    height={data?.ctaTwoImage?.height}
                    className="w-auto"
                  />
                </Link>
              ) : (
                <Link
                  to={cardCtaTwoLink}
                  className="flex justify-center items-center p-3"
                >
                  ctaTwoImage
                </Link>
              )
            ) : data?.ctaTwoImage ? (
              <img
                src={data?.ctaTwoImage?.url}
                title={data?.ctaTwoImage?.title}
                alt={data?.ctaTwoImage?.title}
                width={data?.ctaTwoImage?.width}
                height={data?.ctaTwoImage?.height}
                className="w-auto"
              />
            ) : (
              <div className="flex justify-center items-center p-3">
                ctaTwoImage
              </div>
            )}
            {cardCtaThreeLink ? (
              data?.ctaThreeImage ? (
                <Link to={cardCtaThreeLink}>
                  <img
                    src={data?.ctaThreeImage?.url}
                    title={data?.ctaThreeImage?.title}
                    alt={data?.ctaThreeImage?.title}
                    width={data?.ctaThreeImage?.width}
                    height={data?.ctaThreeImage?.height}
                    className="w-auto"
                  />
                </Link>
              ) : (
                <Link
                  to={cardCtaThreeLink}
                  className="flex justify-center items-center p-3"
                >
                  ctaThreeImage
                </Link>
              )
            ) : data?.ctaThreeImage ? (
              <img
                src={data?.ctaThreeImage?.url}
                title={data?.ctaThreeImage?.title}
                alt={data?.ctaThreeImage?.title}
                width={data?.ctaThreeImage?.width}
                height={data?.ctaThreeImage?.height}
                className="w-auto"
              />
            ) : (
              <div className="flex justify-center items-center p-3">
                ctaThreeImage
              </div>
            )}
          </div>
        </div>
        {data?.backgroundImageDesktop ? (
          <img
            src={data?.backgroundImageDesktop?.url}
            title={data?.backgroundImageDesktop?.title}
            alt={data?.backgroundImageDesktop?.title}
            width={data?.backgroundImageDesktop?.width}
            height={data?.backgroundImageDesktop?.height}
            className="w-auto"
          />
        ) : (
          <div>backgroundImageDesktop</div>
        )}
      </div>
    </div>
  );
}
