import {Link} from '~/components';

export function MWSimpleImage({simpleImage}) {
  if (
    simpleImage?.link === 'open_chat' ||
    simpleImage?.link === 'open_store_locator'
  ) {
    return (
      <button
        className={
          simpleImage?.link === 'open_chat'
            ? 'js-toggle-chat'
            : 'js-toggle-store-locator'
        }
      >
        <img
          src={simpleImage.image.url}
          title={simpleImage.image.title}
          alt={simpleImage.image.title}
          width={simpleImage.image.width}
          height={simpleImage.image.height}
          className="mw-simple-image-desktop"
        />
        <img
          src={simpleImage.mobileImage.url}
          title={simpleImage.mobileImage.title}
          alt={simpleImage.mobileImage.title}
          width={simpleImage.mobileImage.width}
          height={simpleImage.mobileImage.height}
          className="mw-simple-image-mobile"
        />
      </button>
    );
  }

  if (simpleImage?.link?.startsWith('tel:')) {
    return (
      <a href={simpleImage?.link}>
        <img
          src={simpleImage.image.url}
          title={simpleImage.image.title}
          alt={simpleImage.image.title}
          width={simpleImage.image.width}
          height={simpleImage.image.height}
          className="mw-simple-image-desktop"
        />
        <img
          src={simpleImage.mobileImage.url}
          title={simpleImage.mobileImage.title}
          alt={simpleImage.mobileImage.title}
          width={simpleImage.mobileImage.width}
          height={simpleImage.mobileImage.height}
          className="mw-simple-image-mobile"
        />
      </a>
    );
  }

  const imageLink = simpleImage?.ctaUrl
    ? simpleImage?.ctaUrl
    : simpleImage?.link
    ? simpleImage?.link
    : null;
  return (
    <div className="mw-simple-image">
      {imageLink ? (
        <Link to={imageLink}>
          <img
            src={simpleImage.image.url}
            title={simpleImage.image.title}
            alt={simpleImage.image.title}
            width={simpleImage.image.width}
            height={simpleImage.image.height}
            className="mw-simple-image-desktop"
          />
          <img
            src={simpleImage.mobileImage.url}
            title={simpleImage.mobileImage.title}
            alt={simpleImage.mobileImage.title}
            width={simpleImage.mobileImage.width}
            height={simpleImage.mobileImage.height}
            className="mw-simple-image-mobile"
          />
        </Link>
      ) : (
        <>
          <img
            src={simpleImage.image.url}
            title={simpleImage.image.title}
            alt={simpleImage.image.title}
            width={simpleImage.image.width}
            height={simpleImage.image.height}
            className="mw-simple-image-desktop"
          />
          <img
            src={simpleImage.mobileImage.url}
            title={simpleImage.mobileImage.title}
            alt={simpleImage.mobileImage.title}
            width={simpleImage.mobileImage.width}
            height={simpleImage.mobileImage.height}
            className="mw-simple-image-mobile"
          />
        </>
      )}
    </div>
  );
}
