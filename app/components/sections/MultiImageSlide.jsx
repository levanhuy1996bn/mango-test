import {MWSimpleImage} from './SimpleImage.jsx';

export function MWMultiImageSlide({multiImageSlide}) {
  return (
    <div className="mw-multi-image-slide flex flex-row w-full justify-around px-5">
      {multiImageSlide.imagesCollection.items.map((i) => {
        return (
          <div
            key={i.title}
            className={`${i?.mobileOnly ? 'block md:hidden' : ''} basis-2/6`}
          >
            <MWSimpleImage simpleImage={i} />
          </div>
        );
      })}
    </div>
  );
}
