import {Money} from '@shopify/hydrogen';
import {Link} from '~/components';
import * as components from '~/components';
import {saveAmount, triggerSearchActionEvent} from '~/lib/utils';

export function FeaturedProductCard({product, loading, isSearchPage = false}) {
  const minPrice = product.priceRange.minVariantPrice;
  const minCompareAtPrice = product.compareAtPriceRange.minVariantPrice;

  const renderExtraMoneyBadge = () => {
    if (saveAmount(minCompareAtPrice, minPrice) > 0) {
      return (
        <components.MoneySavingBadge
          className={`flex text-mw-red-500 text-xs bg-mw-white font-bold border border-mw-red-500 py-1 px-3 w-fit rounded mt-1`}
          currencyCode={minPrice.currencyCode}
          saveAmount={saveAmount(minCompareAtPrice, minPrice)}
        />
      );
    }
  };
  return (
    <div className="relative w-full">
      <components.ProductBadge
        isMWSelect={product.isMWSelect?.value}
        isBestseller={product.isBestseller?.value}
      />
      <Link
        onClick={() =>
          isSearchPage === true && triggerSearchActionEvent(product.title, true)
        }
        className="flex flex-col gap-2 p-3 h-full"
        to={`/products/${product.handle}`}
      >
        {product?.featuredImage && (
          <img
            data={product?.featuredImage}
            alt={
              product?.featuredImage.altText || `Picture of ${product.title}`
            }
            loading={loading}
            widths={[300]}
            sizes="300px"
            loaderOptions={{
              crop: 'center',
              scale: 1,
              width: 300,
              height: 300,
            }}
          />
        )}
        {renderExtraMoneyBadge()}
        <div className="text-mw-grayscale-800 text-base md:text-lg font-bold whitespace-normal">
          {product.title}
        </div>
        <div className="flex items-center flex-wrap mt-auto">
          <span className="inline-block text-sm font-light mr-1">From</span>
          {saveAmount(minCompareAtPrice, minPrice) > 0 && (
            <div className="flex text-sm mr-1">
              <Money
                className="font-light line-through"
                withoutTrailingZeros
                data={product.compareAtPriceRange.minVariantPrice}
              />
            </div>
          )}
          <Money
            className={`${
              saveAmount(minCompareAtPrice, minPrice) > 0
                ? `text-mw-red-500 font-bold`
                : ``
            } text-sm md:text-base`}
            withoutTrailingZeros
            data={product.priceRange.minVariantPrice}
          />
        </div>
      </Link>
    </div>
  );
}
