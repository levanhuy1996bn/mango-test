import {BannerCarousel} from './BannerCarousel';
import {BannerSingleCta} from './BannerSingleCta';
import {MWButtonGrid} from './ButtonGrid';
// import {MWSimpleBanner} from './SimpleBanner';
// import {MWHeroBanner} from './BannerWithBackgroundImage';
// import {SimpleBannerSlider} from './SimpleBannerSlider.client';
// import {MWCardRow} from './CardRow';
import {MWDecoratedHeadline} from './DecoratedHeadline';
import {MWFeaturedProducts} from './FeaturedProducts';
import {FourGrid} from './FourGrid';
import {MWImageCarousel} from './ImageCarousel';
import {MultiCtaWithCards} from './MultiCtaWithCards';
import {MWPromoStack} from './PromoStack';
import {MWRow} from './Row';
import {MWSimpleImage} from './SimpleImage';
import {MWSlider} from './Slider';
import {TwoGrid} from './TwoGrid';
import {TwoPromoCardsRow} from './TwoPromoCardsRow';
import {VideoBanner} from './VideoBanner';

export function MWLandingPage({landingPage, landing2ndPage}) {
  const landingPageOutput = landingPage?.widgetsCollection?.items?.map(
    (currentItem, index) => {
      const item2ndPage = landing2ndPage?.widgetsCollection?.items[index];
      let i = {};
      if (
        typeof currentItem === 'object' &&
        Object.keys(currentItem).length > 0
      ) {
        i = currentItem;
      } else if (
        typeof item2ndPage === 'object' &&
        Object.keys(item2ndPage).length > 0 &&
        !Array.isArray(item2ndPage)
      ) {
        i = item2ndPage;
      }
      if (i?.__typename === 'DecoratedHeadline') {
        return (
          <div
            className="clear-both mw-decorated-headline-container"
            key={i?.slug}
          >
            <MWDecoratedHeadline decoratedHeadline={i} />
          </div>
        );
      }
      if (i?.__typename === 'ButtonGrid') {
        return (
          <div
            className="mw-button-grid-container max-w-lg md:max-w-2xl lg:max-w-3xl mx-auto mt-10 mb-12"
            key={i?.slug}
          >
            <MWButtonGrid buttonGrid={i} />
          </div>
        );
      }
      if (i?.__typename === 'CardPortrait') {
        return (
          <div className="mw-simple-image-container mb-5" key={i?.slug}>
            <MWSimpleImage simpleImage={i} />
          </div>
        );
      }
      if (i?.__typename === 'ImageCarousel') {
        return (
          <div
            key={i?.slug}
            className={`${
              i?.slidesCollection?.items?.length === 1
                ? 'mb-10 md:mb-12'
                : 'mb-20 md:mb-28'
            } max-w-3xl mx-auto mt-10 mw-image-carousel-container`}
          >
            <MWImageCarousel imageCarousel={i} />
          </div>
        );
      }
      if (i?.__typename === 'FeaturedProducts') {
        return (
          <div className="mb-10 featured-products-container" key={i?.slug}>
            <MWFeaturedProducts featuredProducts={i} />
          </div>
        );
      }
      if (i?.__typename === 'PromoStack') {
        return (
          <div
            className="w-auto md:w-full overflow-x-scroll whitespace-nowrap md:overflow-auto md:whitespace-normal p-0 flex md:flex-col gap-3 md:gap-5"
            key={i?.slug}
          >
            <MWPromoStack promoCardsCollection={i} />
          </div>
        );
      }
      if (i?.__typename === 'Slideshow') {
        return (
          <div
            className="w-full md:w-1/2 mw-slider-container p-0"
            key={i?.slug}
          >
            <MWSlider slider={i} />
          </div>
        );
      }
      if (i?.__typename === 'Row') {
        return <MWRow row={i} key={i?.slug} />;
      }
      if (i?.__typename === 'BannerSingleCta') {
        return <BannerSingleCta data={i} key={i?.slug} />;
      }
      if (i?.__typename === 'BannerCarousel') {
        return (
          <div
            key={i?.slug}
            className="mw-contentful-banner-carousel pb-5 mb-10"
          >
            <BannerCarousel data={i} />
          </div>
        );
      }
      if (i?.__typename === 'TwoPromoCardsRow') {
        return <TwoPromoCardsRow data={i} key={i?.slug} />;
      }
      if (i?.__typename === 'VideoBanner') {
        return <VideoBanner data={i} key={i?.slug} />;
      }
      if (i?.__typename === 'MultiCtaWithCards') {
        return <MultiCtaWithCards data={i} key={i?.slug} />;
      }
      if (i?.__typename === 'TwoGrid') {
        return <TwoGrid data={i} key={i?.slug} />;
      }
      if (i?.__typename === 'FourGrid') {
        return <FourGrid data={i} key={i?.slug} />;
      }
    },
  );
  return landingPageOutput;
}
