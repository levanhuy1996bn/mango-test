import {Link} from '~/components';

export function MWButton({button}) {
  const buttonLink = button?.ctaUrl
    ? button?.ctaUrl
    : button?.buttonUrl
    ? button?.buttonUrl
    : null;
  return (
    <Link
      to={buttonLink}
      className="mw-cms-button inline-block rounded-lg font-bold text-center p-3 sm:p-4 md:p-5 lg:p-6 border-2 border-mw-red-500 text-mw-red-500 text-lg sm:text-2xl md:text-3xl hover:bg-mw-red-500 hover:text-white"
    >
      {button?.buttonText}
    </Link>
  );
}
