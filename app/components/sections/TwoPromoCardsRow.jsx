import {PromoCardClient} from './PromoCardClient';

export function TwoPromoCardsRow({data}) {
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 gap-5 mb-5">
      {data?.promoCardEntriesCollection?.items?.map((i) => {
        return <PromoCardClient key={i?.title} promoCard={i} />;
      })}
    </div>
  );
}
