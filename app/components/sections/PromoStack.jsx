import {MWPromoCard} from './PromoCard';

export function MWPromoStack({promoCardsCollection}) {
  return promoCardsCollection?.promoCardsCollection?.items?.map((i) => {
    return <MWPromoCard key={i?.title} promoCard={i} />;
  });
}
