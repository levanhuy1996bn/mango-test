import {Link} from '~/components';

export function MWPromoCard({promoCard}) {
  const cardOneCtaUrl = promoCard?.card1CtaUrl
    ? promoCard?.card1CtaUrl
    : promoCard?.cta1Url
    ? promoCard?.cta1Url
    : '';
  const cardTwoCtaUrl = promoCard?.card2CtaUrl
    ? promoCard?.card2CtaUrl
    : promoCard?.cta2Url
    ? promoCard?.cta2Url
    : '';
  return (
    <div className="w-full bg-mw-grayscale-100 inline-flex md:flex-row md:flex-nowrap gap-2.5 md:gap-3 lg:gap-5 p-2.5 md:p-3 lg:p-5">
      <div className="md:basis-1/3 w-[160px] sm:w-[200px] md:w-auto">
        <img
          src={promoCard?.image?.url}
          title={promoCard?.image?.title}
          alt={promoCard?.image?.title}
          width={promoCard?.image?.width}
          height={promoCard?.image?.height}
          className="w-full"
        />
      </div>
      <div className="md:basis-2/3 flex flex-col gap-2 lg:gap-3">
        {(() => {
          if (promoCard?.brandLogo) {
            return (
              <img
                src={promoCard?.brandLogo?.url}
                title={promoCard?.brandLogo?.title}
                alt={promoCard?.brandLogo?.title}
                width={promoCard?.brandLogo?.width}
                height={promoCard?.brandLogo?.height}
                className="w-2/3 md:w-1/3 mb-3"
              />
            );
          }
        })()}
        <div className="flex flex-col gap-0.5 md:gap-1">
          <div className="font-bold text-base sm:text-lg md:text-2xl lg:text-3xl uppercase whitespace-normal">
            {promoCard?.headline}
          </div>
          <div className="sm:font-bold text-sm sm:text-base md:text-xl lg:text-2xl whitespace-normal leading-none">
            {promoCard?.subheadline}
          </div>
        </div>
        <div className="hidden sm:block text-sm lg:text-base whitespace-normal">
          {promoCard?.description}
        </div>
        <div className="flex justify-start mt-2">
          <Link
            to={cardOneCtaUrl}
            className="cursor-pointer flex items-center min-w-56 uppercase text-xs lg:text-sm px-3.5 lg:px-5 py-1.5 lg:py-2 font-bold text-white rounded-md border border-mw-red-500 bg-mw-red-500 hover:text-mw-red-500 hover:bg-white"
          >
            {promoCard?.cta1Text}
          </Link>
          {(() => {
            if (cardTwoCtaUrl) {
              return (
                <Link
                  to={cardTwoCtaUrl}
                  className="cursor-pointer flex items-center min-w-56 uppercase text-xs lg:text-sm px-3.5 lg:px-5 py-1.5 lg:py-2 font-bold text-white rounded-md border border-mw-red-500 bg-mw-red-500 hover:text-mw-red-500 hover:bg-white"
                >
                  {promoCard?.cta2Text}
                </Link>
              );
            }
          })()}
        </div>
      </div>
    </div>
  );
}
