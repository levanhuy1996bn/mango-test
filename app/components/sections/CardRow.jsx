import {MWSimpleCard} from './SimpleCard';

export function MWCardRow({cardsCollection}) {
  return (
    <div className="justify-between grid grid-cols-3 gap-10">
      {cardsCollection.cardsCollection.items.map((i) => {
        return (
          <div key={i.title} className="inline-block rounded-lg">
            <MWSimpleCard simpleCard={i} />
          </div>
        );
      })}
    </div>
  );
}
