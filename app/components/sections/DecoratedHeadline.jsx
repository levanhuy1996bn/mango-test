import {documentToReactComponents} from '@contentful/rich-text-react-renderer';

export function MWDecoratedHeadline({decoratedHeadline}) {
  return (
    <div className="mw-decorated-headline text-center font-bold leading-none mb-10">
      {documentToReactComponents(decoratedHeadline?.headline?.json)}
    </div>
  );
}
