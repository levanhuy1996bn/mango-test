/**
 * A shared component that specifies the Star Half icon
 */
export function StarHalfIcon() {
  return (
    <svg
      aria-hidden="true"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M18,0h-8v2.5l2,4.9l5.3,0.4l-4,3.5l1.2,5.2L10,13.7V20h8c1.1,0,2-0.9,2-2V2C20,0.9,19.1,0,18,0"
        fill="#D7D6D5"
      />
      <path
        d="M2,0h8v2.5L8,7.4L2.6,7.8l4,3.5l-1.2,5.2l4.5-2.8V20H2c-1.1,0-2-0.9-2-2V2C0,0.9,0.9,0,2,0"
        fill="#161615"
      />
    </svg>
  );
}
