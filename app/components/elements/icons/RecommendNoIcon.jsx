/**
 * A shared component that specifies the Recommend No icon
 */
export function RecommendNoIcon() {
  return (
    <svg
      aria-hidden="true"
      width="18"
      height="18"
      viewBox="0 0 18 18"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9,0C4,0,0,4,0,9s4,9,9,9s9-4,9-9S14,0,9,0 M13.4,11.8c0.2,0.2,0.2,0.5,0,0.6c0,0,0,0,0,0l-1,1
        c-0.1,0.1-0.2,0.1-0.3,0.1c-0.1,0-0.2,0-0.3-0.1L9,10.6l-2.8,2.8c-0.2,0.2-0.5,0.2-0.6,0c0,0,0,0,0,0l-1-1c-0.2-0.2-0.2-0.5,0-0.6
        c0,0,0,0,0,0L7.4,9L4.6,6.2c-0.2-0.2-0.2-0.5,0-0.6c0,0,0,0,0,0l1-1c0.2-0.2,0.5-0.2,0.6,0L9,7.4l2.8-2.8c0.1-0.1,0.2-0.1,0.3-0.1
        c0.1,0,0.2,0,0.3,0.1l1,1c0.2,0.2,0.2,0.5,0,0.6c0,0,0,0,0,0L10.6,9L13.4,11.8z"
        fill="#161615"
      />
    </svg>
  );
}
