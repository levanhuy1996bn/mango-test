/**
 * A shared component that specifies the Star Empty icon
 */
export function StarEmptyIcon() {
  return (
    <svg
      aria-hidden="true"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M18,0H2C0.9,0,0,0.9,0,2v16c0,1.1,0.9,2,2,2h16c1.1,0,2-0.9,2-2V2C20,0.9,19.1,0,18,0 M14.5,16.5 L10,13.7l-4.5,2.8l1.2-5.2l-4-3.5L8,7.4l2-4.9l2,4.9l5.3,0.4l-4,3.5L14.5,16.5z"
        fill="#D7D6D5"
      />
    </svg>
  );
}
