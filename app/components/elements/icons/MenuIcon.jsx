/**
 * A shared component that specifies the Menu icon
 */
export function MenuIcon() {
  return (
    <div className="flex flex-col gap-[3px]">
      <div className="bg-mw-grayscale-800 w-[18px] h-1 rounded" />
      <div className="bg-mw-grayscale-800 w-[18px] h-1 rounded" />
      <div className="bg-mw-grayscale-800 w-[18px] h-1 rounded" />
    </div>
  );
}
