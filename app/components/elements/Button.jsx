import {Link} from '~/components';
import {missingClass} from '~/lib/utils';
import clsx from 'clsx';

export function Button({
  as = 'button',
  className = '',
  variant = 'primary',
  width = 'auto',
  ...props
}) {
  const Component = props?.to ? Link : as;

  const baseButtonClasses =
    'inline-block rounded-lg font-medium text-center py-3 px-6';

  const variants = {
    primary: `${baseButtonClasses} bg-mw-red-500 text-contrast hover:bg-white hover:text-mw-red-500 border border-mw-red-500`,
    secondary: `${baseButtonClasses} border border-primary/10 bg-contrast text-primary`,
    inline: 'border-b border-primary/10 leading-none pb-1',
  };

  const widths = {
    auto: 'w-auto',
    full: 'w-full',
  };

  const styles = clsx(
    missingClass(className, 'bg-') && variants[variant],
    missingClass(className, 'w-') && widths[width],
    className,
  );

  return <Component className={styles} {...props} />;
}
