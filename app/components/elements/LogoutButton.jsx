export function LogoutButton(props) {
  const logout = () => {
    fetch('/account/logout', {method: 'POST'}).then(() => {
      if (typeof props?.onClick === 'function') {
        props.onClick();
      }
      window.location.href = '/';
    });
  };

  return (
    <button
      className="text-mw-grayscale-800 px-[18px] font-bold text-base py-3 rounded-lg border-mw-grayscale-300 border"
      {...props}
      onClick={logout}
    >
      Sign Out
    </button>
  );
}
