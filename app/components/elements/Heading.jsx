import {formatText, missingClass} from '~/lib/utils';
import clsx from 'clsx';

export function Heading({
  as: Component = 'h2',
  children,
  className = '',
  format,
  size = 'heading',
  width = 'default',
  ...props
}) {
  const sizes = {
    display: 'font-bold text-display',
    heading: 'font-bold text-heading',
    headingResponsive: 'font-bold text-xl md:text-heading',
    subheading: 'text-2xl font-bold',
    subheading2: 'text-xl font-bold',
    lead: 'font-bold text-lead',
    copy: 'font-medium text-copy',
    none: '',
  };

  const widths = {
    default: 'max-w-prose',
    narrow: 'max-w-prose-narrow',
    wide: 'max-w-prose-wide',
  };

  const styles = clsx(
    missingClass(className, 'whitespace-') && 'whitespace-pre-wrap',
    missingClass(className, 'max-w-') && widths[width],
    missingClass(className, 'font-') && sizes[size],
    'text-mw-grayscale-800',
    className,
  );

  return (
    <Component {...props} className={styles}>
      {format ? formatText(children) : children}
    </Component>
  );
}
