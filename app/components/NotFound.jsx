import {useMatches} from '@remix-run/react';
import {Button, FeaturedSection, PageHeader, Text} from '~/components';
import {Suspense} from 'react';

export function NotFound({type = 'page'}) {
  const [root] = useMatches();
  const featuredCollections = root?.data?.featuredCollections;
  const featuredProducts = root?.data?.featuredProducts;
  const heading = `We’ve lost this ${type}`;
  const description = `We couldn’t find the ${type} you’re looking for. Try checking the URL or heading back to the home page.`;

  return (
    <>
      <PageHeader heading={heading}>
        <Text width="narrow" as="p">
          {description}
        </Text>
        <Button width="auto" variant="secondary" to={'/'}>
          Take me to the home page
        </Button>
      </PageHeader>
      <Suspense>
        <FeaturedSection
          featuredCollections={featuredCollections}
          featuredProducts={featuredProducts}
        />
      </Suspense>
    </>
  );
}
