import {Heading, Link} from '~/components';

/**
 * A server component that specifies the content of the footer on the website
 */
export function FooterMenuDesktop({
  firstRowLeft = [],
  firstRowRight = [],
  secondRow = [],
  thirdRow = [],
  fourthRow = [],
  others = [],
}) {
  const styles = {
    section: 'flex flex-col gap-6',
    nav: 'grid gap-3.5 pb-8',
    navSmall: 'grid gap-3.5 pb-6',
  };

  return (
    <footer
      role="contentinfo"
      className="min-h-[20rem] w-full gap-6 pb-8 px-12 overflow-hidden hidden lg:grid max-w-page-width mx-auto"
    >
      <div className="border-t border-mw-grayscale-200" />
      <div className="flex flex-wrap justify-between items-center gap-6">
        <div className="flex justify-start items-center gap-8">
          {(firstRowLeft || []).map((item) => (
            <Link
              key={item.id}
              to={item.to}
              target={item.target}
              className="text-subheading2 font-bold inline"
            >
              {item.title}
            </Link>
          ))}
        </div>
        <div className="flex justify-end items-center gap-8">
          {(firstRowRight || []).map((item) => (
            <Link
              key={item.id}
              to={item.to}
              target={item.target}
              className="text-subheading2 font-bold inline"
            >
              {item.title}
            </Link>
          ))}
        </div>
      </div>
      {fourthRow?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div className="grid grid-cols-3 lg:grid-cols-6 gap-6">
            {(fourthRow || []).map((item) => (
              <section key={item.id} className={styles.section}>
                <div className="text-left cursor-default">
                  <Heading
                    className={`${
                      item.isClone && 'invisible'
                    } flex justify-between text-lead font-bold`}
                    size="none"
                    as="h3"
                  >
                    {item.title}
                  </Heading>
                </div>
                {item?.items?.length > 0 && (
                  <div className="h-fit overflow-hidden">
                    <nav className={styles.nav}>
                      {item.items.map((subItem) => (
                        <Link
                          key={subItem.id}
                          to={subItem.to}
                          target={subItem.target}
                        >
                          {subItem.title}
                        </Link>
                      ))}
                    </nav>
                  </div>
                )}
              </section>
            ))}
          </div>
        </>
      )}
      {secondRow?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div>
            {(secondRow || []).map((parentItem) => (
              <div className="grid gap-6" key={parentItem.id}>
                <div className="text-lead font-bold">
                  <Link
                    key={parentItem.id}
                    to={parentItem.to}
                    target={parentItem.target}
                    className="text-subheading2 font-bold inline"
                  >
                    {parentItem.title}
                  </Link>
                </div>
                <div className="grid grid-cols-3 lg:grid-cols-6 gap-6">
                  {(parentItem.items || []).map((item, index) => {
                    return index + 1 === parentItem.items.length &&
                      item?.title?.startsWith('All ') ? null : (
                      <section key={item.id} className={styles.section}>
                        <div className="text-left cursor-default">
                          <Heading
                            className={`${
                              item.isClone && 'invisible'
                            } flex justify-between uppercase text-xs font-bold`}
                            size="none"
                            as="h3"
                          >
                            {item.title}
                          </Heading>
                        </div>
                        {item?.items?.length > 0 && (
                          <div className="h-fit overflow-hidden">
                            <nav className={styles.navSmall}>
                              {item.items.map((subItem, index) => {
                                return index + 1 === item.items.length &&
                                  subItem?.title?.startsWith('All ') ? null : (
                                  <Link
                                    key={subItem.id}
                                    to={subItem.to}
                                    target={subItem.target}
                                  >
                                    {subItem.title}
                                  </Link>
                                );
                              })}
                            </nav>
                          </div>
                        )}
                      </section>
                    );
                  })}
                </div>
              </div>
            ))}
          </div>
        </>
      )}
      {thirdRow?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div className="grid grid-cols-3 lg:grid-cols-6 gap-6">
            {(thirdRow || []).map((item) => (
              <section key={item.id} className={styles.section}>
                <div className="text-left cursor-default">
                  <Link
                    key={item.id}
                    to={item.to}
                    target={item.target}
                    className={`${
                      item.isClone && 'invisible'
                    } text-lead font-bold inline`}
                  >
                    {item.title}
                  </Link>
                </div>
                {item?.items?.length > 0 && (
                  <div className="h-fit overflow-hidden">
                    <nav className={styles.nav}>
                      {item.items.map((subItem, index) => {
                        return index + 1 === item.items.length &&
                          subItem?.title?.startsWith('All ') ? null : (
                          <Link
                            key={subItem.id}
                            to={subItem.to}
                            target={subItem.target}
                          >
                            {subItem.title}
                          </Link>
                        );
                      })}
                    </nav>
                  </div>
                )}
              </section>
            ))}
          </div>
        </>
      )}
      {others?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div className="grid grid-cols-3 lg:grid-cols-6 gap-6">
            {(others || []).map((item) => (
              <section key={item.id} className={styles.section}>
                <div className="text-left cursor-default">
                  <Heading className="flex justify-between" size="lead" as="h3">
                    {item.title}
                  </Heading>
                </div>
                {item?.items?.length > 0 && (
                  <div className="h-fit overflow-hidden">
                    <nav className={styles.nav}>
                      {item.items.map((subItem) => (
                        <Link
                          key={subItem.id}
                          to={subItem.to}
                          target={subItem.target}
                        >
                          {subItem.title}
                        </Link>
                      ))}
                    </nav>
                  </div>
                )}
              </section>
            ))}
          </div>
        </>
      )}
    </footer>
  );
}
