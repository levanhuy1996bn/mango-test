import {MediaFile} from '@shopify/hydrogen';
import logoImage from '~/assets/logo.png';
import bedPostImage from '~/assets/sleep-blog.png';
import {Drawer, Link, Text} from '~/components';
import {triggerPhoneNumberEvent} from '~/lib/utils';
import {useRef, useState} from 'react';
import {HiArrowLeft, HiChevronRight} from 'react-icons/hi';
import {useScroll} from 'react-use';

export function MenuDrawer({isOpen, onClose, menu, pathname}) {
  return (
    <Drawer
      open={isOpen}
      onClose={onClose}
      openFrom="left"
      heading={
        logoImage ? (
          <Link
            className="flex items-center self-stretch justify-center flex-growl"
            to="/"
            onClick={onClose}
          >
            <img
              src={logoImage}
              width="1200"
              height="354"
              alt="Mattress Warehouse"
              className="w-[124px]"
            />
          </Link>
        ) : (
          <Link
            className="flex items-center self-stretch justify-center flex-grow w-full h-full"
            to="/"
            onClick={onClose}
          >
            Mattress Warehouse
          </Link>
        )
      }
    >
      <div className="grid">
        <MenuMobileNav menu={menu} onClose={onClose} pathname={pathname} />
      </div>
    </Drawer>
  );
}

function MenuMobileNav({menu, onClose, pathname}) {
  const [activeMenu, setActiveMenu] = useState(menu);
  const [parentId, setParentId] = useState(null);
  const [parentLabel, setParentLabel] = useState(null);

  const scrollRef = useRef(null);
  const {y} = useScroll(scrollRef);

  const topMenuId = menu.id;

  const HAS_CHILDREN = 'has_children';
  const ALL_MENU = 'all_menu';
  const BRAND = 'brand';
  const BRAND_NO_IMAGE = 'brand_no_image';
  const IMAGE_WITH_LINK = 'image_with_link';
  const TEXT_LINK = 'text_link';

  function findMenuById({menuObj, targetId}) {
    const res = {
      menu: {},
      parentId: null,
      parentLabel: null,
    };
    const recursion = (o, parentId, parentLabel) => {
      if (o.id === targetId) {
        res.menu = o;
        res.parentId = parentId;
        res.parentLabel = parentLabel;
        return null; // stop recursion
      }
      const newParentId = o.id;
      if (!o.items) {
        return null;
      } else {
        return o.items.forEach((o2) => {
          const newParentLabel = o2.title;
          return recursion(o2, newParentId, newParentLabel);
        });
      }
    };
    recursion(menuObj, null, null); // start recursion
    return res;
  }

  function handleMenuClick({menuId}) {
    const newMenuId = menuId ? menuId : topMenuId; // show default menu if no menuId is passed
    const result = findMenuById({
      menuObj: menu,
      targetId: newMenuId,
    });
    setActiveMenu(result.menu);
    setParentId(result.parentId);
    setParentLabel(result.parentLabel);
  }

  function handleBackClick() {
    const newMenuId = parentId ? parentId : topMenuId; // show default menu if parentId is null
    const result = findMenuById({
      menuObj: menu,
      targetId: newMenuId,
    });
    setActiveMenu(result.menu);
    setParentId(result.parentId);
    setParentLabel(result.parentLabel);
  }

  return (
    <nav
      ref={scrollRef}
      aria-labelledby="mobile-menu-heading"
      className={`content-start h-screen-no-nav overflow-y-scroll transition ${
        y > 0 ? 'border-t' : ''
      }`}
    >
      <div className="bg-mw-grayscale-100 px-6 py-4">
        {parentId ? (
          <button
            type="button"
            onClick={() => handleBackClick()}
            className="flex items-center gap-2"
          >
            <HiArrowLeft className="text-sm" />
            <span className="font-bold text-sm">{parentLabel}</span>
          </button>
        ) : (
          <div className="h-5" />
        )}
      </div>
      <div
        className={`${
          parentLabel === 'Shop by Brand'
            ? 'grid-cols-2 md:grid-cols-3 px-8'
            : 'px-6 md:px-12'
        } grid gap-6 pt-6 pb-16`}
      >
        {(activeMenu?.items || []).map((item, index) => {
          if (item.title === 'Customer Support' || item.title === 'About Us') {
            return; // hide Customer Support and About Us
          }
          const hasImage = !!item.resourceData?.image;
          const dataMedia = hasImage && {
            image: {
              ...item.resourceData?.image,
              altText: item.title,
            },
            mediaContentType: 'IMAGE',
          };

          let menuItemType = null;
          if (item?.items?.length > 0) {
            menuItemType = HAS_CHILDREN;
          } else {
            if (item?.title?.startsWith('All ')) {
              menuItemType = ALL_MENU;
            } else if (parentLabel === 'Shop by Brand') {
              if (hasImage) {
                menuItemType = BRAND;
              } else {
                menuItemType = BRAND_NO_IMAGE;
              }
            } else {
              if (hasImage) {
                menuItemType = IMAGE_WITH_LINK;
              } else {
                menuItemType = TEXT_LINK; // default
              }
            }
          }

          let result = null;
          switch (menuItemType) {
            case HAS_CHILDREN:
              result = (
                <button
                  key={item.id}
                  type="button"
                  onClick={() => handleMenuClick({menuId: item.id})}
                  className="flex justify-between items-center text-mw-grayscale-800 font-bold"
                >
                  <Text as="span" size="copy">
                    {item.title}
                  </Text>
                  <HiChevronRight />
                </button>
              );
              break;
            case ALL_MENU:
              result = (
                <Link
                  key={item.id}
                  to={item.to}
                  target={item.target}
                  onClick={onClose}
                  className={`${
                    parentLabel === 'Shop by Brand' &&
                    'col-span-2 md:col-span-3'
                  } cursor-pointer flex justify-center items-center px-6 py-3 font-bold text-white rounded-md bg-mw-red-500  w-full mt-2`}
                >
                  {item.title}
                </Link>
              );
              break;
            case BRAND:
              result = (
                <Link
                  key={item.id}
                  to={item.to}
                  target={item.target}
                  onClick={onClose}
                  className={`${
                    item.to === pathname
                      ? 'border-mw-grayscale-700'
                      : 'border-mw-grayscale-300'
                  } border rounded-md p-5`}
                >
                  <div className="w-auto h-12 flex justify-center items-center">
                    <MediaFile
                      id={null}
                      data={dataMedia}
                      className="w-auto max-h-[3rem]"
                      options={{
                        crop: 'center',
                      }}
                    />
                  </div>
                </Link>
              );
              break;
            case BRAND_NO_IMAGE:
              result = (
                <Link
                  key={item.id}
                  to={item.to}
                  target={item.target}
                  onClick={onClose}
                  className="flex justify-center items-center"
                >
                  <div className="">{item.title}</div>
                </Link>
              );
              break;
            case IMAGE_WITH_LINK:
              result = (
                <Link
                  key={item.id}
                  to={item.to}
                  target={item.target}
                  onClick={onClose}
                  className={`${
                    item.to === pathname
                      ? 'border-mw-grayscale-700'
                      : 'border-mw-grayscale-300'
                  } border rounded-md px-5 py-2 flex justify-between items-center`}
                >
                  <div className="w-[72px] h-[72px] flex justify-center items-center">
                    <MediaFile
                      id={null}
                      data={dataMedia}
                      className="w-auto max-h-[72px]"
                      options={{
                        crop: 'center',
                      }}
                    />
                  </div>
                  <div
                    className={
                      'text-mw-grayscale-800 text-lead grow text-center'
                    }
                  >
                    {item.title}
                  </div>
                </Link>
              );
              break;
            case TEXT_LINK:
            default: {
              const isBedPost = item?.type === 'HTTP' && item?.to === '/blogs';
              result = isBedPost ? (
                <Link
                  key={item.id}
                  to={item.to}
                  target={item.target}
                  onClick={onClose}
                  className={`${
                    item.to === pathname
                      ? 'border-mw-grayscale-700'
                      : 'border-mw-grayscale-300'
                  } border rounded-md px-5 py-2 flex justify-between items-center`}
                >
                  <div className="w-[76px] h-[76px] rounded-full shrink-0">
                    <img
                      src={bedPostImage}
                      width="76"
                      height="76"
                      alt="The Bed Post"
                      className="w-[76px]"
                    />
                  </div>
                  <div
                    className={
                      'text-mw-grayscale-800 text-lead grow text-center'
                    }
                  >
                    {item.title}
                  </div>
                </Link>
              ) : (
                <Link
                  key={item.id}
                  to={item.to}
                  target={item.target}
                  onClick={onClose}
                >
                  <div className={'text-mw-grayscale-800 font-bold'}>
                    {item.title}
                  </div>
                </Link>
              );
              break;
            }
          }

          const isParentCustomerSupport = parentLabel === 'Customer Support';
          const isParentSleepGuide = parentLabel === 'Sleep Guide';
          const isSectionDivider =
            (isParentCustomerSupport || isParentSleepGuide) && index === 3;

          if (isSectionDivider) {
            result = (
              <>
                <div className="flex flex-col border-b border-mw-grayscale-300 pb-3 pt-3">
                  {isParentCustomerSupport && <div>POLICIES</div>}
                  {isParentSleepGuide && <div>GUIDES</div>}
                </div>
                {result}
              </>
            );
          }

          return result;
        })}
        {!parentId && (
          <>
            <div className="border-t border-mw-grayscale-300 mt-3" />
            <Link to="/account/orders/track" onClick={onClose}>
              Track My Order
            </Link>
            <Link to="/contact/appointment" onClick={onClose}>
              Book Appointment
            </Link>
            <Link to="/contact" onClick={onClose}>
              Contact Us
            </Link>
            <a
              onClick={() => triggerPhoneNumberEvent('18774907511')}
              href={`tel:18774907511`}
              className="font-bold"
            >
              1-877-490-7511
            </a>
          </>
        )}
      </div>
    </nav>
  );
}
