import {Link} from '~/components';
import {GOOGLE_GEO_API_KEY} from '~/lib/const';
import {YEXT_ACCOUNT_ID, YEXT_API_KEY, YEXT_API_VER} from '~/lib/const';
import {checkCookie, getCookie, triggerPhoneNumberEvent} from '~/lib/utils';
import {Suspense, useEffect, useState} from 'react';
import {HiChevronUp} from 'react-icons/hi';

/**
 * A client component that shows the selected store and provides the drawer trigger.
 */
export function HeaderTopLocation({
  openCart,
  isOpen,
  location,
  setLocation,
  loadingLocation,
  setLoadingLocation,
}) {
  const [selectedLoc, setSelectedLoc] = useState(
    'Enter Location or Browse by State',
  );

  useEffect(() => {
    const getNearestLocations = async (loc) => {
      const nearestLocationsResponse = await fetch('/api/nearestLocations', {
        method: 'POST',
        body: loc,
      });
      const nearestLocations = await nearestLocationsResponse.json();
      return nearestLocations;
    };

    if (checkCookie('location')) {
      const locationFromCookieJson = getCookie('location');
      const locationFromCookie = JSON.parse(locationFromCookieJson || '{}');
      setLocation(locationFromCookie);
      setLoadingLocation(true);
      getNearestLocations(locationFromCookieJson)
        .then(() => {})
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          setLoadingLocation(false);
        });
    }
  }, [setLoadingLocation, setLocation]);

  useEffect(() => {
    if (location) {
      const {locality, state, zip} = location;
      setSelectedLoc(`${locality}, ${state} ${zip}`);
    }
  }, [location]);

  return (
    <>
      <Suspense fallback={<div className="p-2">Loading…</div>}>
        <div className="flex justify-between items-center">
          <div className="flex items-center gap-1.5 text-sm">
            <div>Find Your Store:</div>
            <button
              id="select-a-store-trigger"
              onClick={openCart}
              className="relative flex items-center justify-center gap-1"
              aria-label="Enter Location or Browse by State"
            >
              <div
                className={`${
                  // (loadingLocation || pending) && 'animate-pulse'
                  loadingLocation && 'animate-pulse'
                } font-bold text-mw-grayscale-800`}
              >
                {selectedLoc}
              </div>
              <HiChevronUp
                className={`${
                  isOpen ? '' : 'rotate-[180deg]'
                } transition-transform transform-gpu duration-200 text-lead text-mw-grayscale-800`}
              />
            </button>
          </div>
          <div className="hidden lg:flex items-center gap-6 text-sm">
            <Link to="/account/orders/track">Track My Order</Link>
            <Link to="/contact/appointment">Book Appointment</Link>
            <div className="border-l border-mw-grayscale-300 w-[1px] h-[18px]"></div>
            <Link to="/contact">Contact Us</Link>
            <button className="js-toggle-chat">Chat</button>
            <a
              onClick={() => triggerPhoneNumberEvent('18774907511')}
              href={`tel:18774907511`}
              className="font-bold"
            >
              1-877-490-7511
            </a>
          </div>
        </div>
      </Suspense>
    </>
  );
}

/**
 * A helper component that uses the browser's geolocation API to get the user's location.
 * @returns {Promise} position
 */
export function UseBrowserGeolocator() {
  return new Promise((resolve, reject) => {
    if (!navigator.geolocation) {
      reject(new Error('Geolocation is not supported.'));
    }
    navigator?.geolocation?.getCurrentPosition(
      (position) => {
        resolve(position);
      },
      () => {
        reject(new Error('Unable to retrieve your location.'));
      },
    );
  });
}

/**
 * A helper function to fetch the store location by a zip code.
 * @param {string} zipCode e.g. '27104'
 * @returns {string} location object JSON string e.g. '{"latitude":36.099860,"longitude":-80.244216,"locality":"Winston-Salem","state":"NC","zip":"27104"}'
 */
export async function geoLookupByZip(zipCode) {
  if (!zipCode) {
    return;
  }
  const response = await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?address=${zipCode},US&key=${GOOGLE_GEO_API_KEY}`,
  );
  if (!response.ok) {
    throw new Error('error response');
  }
  const responseObj = await response?.json();
  const found = responseObj?.results?.find((entry) =>
    entry?.types?.includes('postal_code'),
  );
  if (!found?.geometry?.location) {
    throw new Error('location not found');
  }
  const latitude = found?.geometry?.location?.lat;
  const longitude = found?.geometry?.location?.lng;
  if (!latitude || !longitude) {
    throw new Error('latitude or longitude not found');
  }
  if (!found?.address_components) {
    throw new Error('address_components not found');
  }
  const locality =
    found?.address_components?.find((component) =>
      component?.types?.includes('locality'),
    ) ??
    found?.address_components?.find((component) =>
      component?.types?.includes('neighborhood'),
    );

  const state = found?.address_components?.find((component) =>
    component?.types?.includes('administrative_area_level_1'),
  );
  if (!locality.short_name || !state.short_name) {
    throw new Error('locality or state not found');
  }
  const location = {
    latitude,
    longitude,
    locality: locality.short_name,
    state: state.short_name,
    zip: zipCode,
  };
  return location;
}

/**
 * A helper function to fetch the store location by latitude and longitude.
 * @param {object} {latitude, longitude} object e.g. {latitude: "36.099860", longitude: "-80.244216"}
 * @returns {string} location object JSON string e.g. '{"latitude":36.099860,"longitude":-80.244216,"locality":"Winston-Salem","state":"NC","zip":"27104"}'
 */
export async function geoLookupByLatLng({
  latitude,
  longitude,
  locationInput = '',
}) {
  if (!latitude || !longitude) {
    return;
  }
  let addressComponents = null;
  const response = await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${GOOGLE_GEO_API_KEY}&result_type=locality|postal_code`,
  );
  if (!response.ok) {
    throw new Error('error response');
  }
  const responseObj = await response?.json();
  let zipData = responseObj?.results?.find((entry) =>
    entry?.types?.includes('postal_code'),
  );

  let locationData = responseObj?.results?.find((entry) =>
    entry?.types?.includes('locality'),
  );

  if (!locationData) {
    addressComponents = zipData?.address_components;
  } else if (locationInput?.length) {
    const matchLocationDataCount = matchingLocationCount(
      locationData,
      locationInput,
    );
    const matchZipDataCount = matchingLocationCount(zipData, locationInput);
    addressComponents =
      matchZipDataCount > matchLocationDataCount
        ? zipData?.address_components
        : locationData?.address_components;
  } else {
    addressComponents = locationData?.address_components;
  }

  if (!addressComponents || !zipData?.address_components) {
    throw new Error('address_components not found');
  }
  const locality = addressComponents?.find(
    (component) =>
      component?.types?.includes('locality') ||
      component?.types?.includes('neighborhood'),
  );
  const state = addressComponents?.find((component) =>
    component?.types?.includes('administrative_area_level_1'),
  );
  const zip = zipData?.address_components?.find((component) =>
    component?.types?.includes('postal_code'),
  );
  const country = addressComponents?.find((component) =>
    component?.types?.includes('country'),
  );
  if (!locality?.short_name || !state?.short_name || !zip?.short_name) {
    throw new Error('locality or state or zip not found');
  }
  return {
    latitude,
    longitude,
    locality: locality.short_name,
    state: state.short_name,
    locationInput,
    zip: zip.short_name,
    country: country.short_name,
  };
}

const matchingLocationCount = (data, locationInput) => {
  let count = 0;
  if (data?.address_components?.length > 0) {
    data?.address_components.map((item) => {
      if (
        locationInput.includes(item.long_name) ||
        locationInput.includes(item.short_name)
      ) {
        count++;
      }
    });
  }

  return count;
};

/**
 * A helper function to fetch store details from Yext.
 * @param {array} locIds e.g. ['0266','0267']
 * @returns {string} location details object
 */
export async function yextLocationDetailsByIds({locIds}) {
  if (!locIds) {
    return;
  }
  const filterIds = (locIds || []).map((id) => `id__in=${id}`).join('&');
  const response = await fetch(
    `https://cdn.yextapis.com/v2/accounts/${YEXT_ACCOUNT_ID}/content/locations?${filterIds}&api_key=${YEXT_API_KEY}&v=${YEXT_API_VER}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );
  if (!response.ok) {
    throw new Error('error response');
  }
  const responseObj = await response?.json();
  return responseObj;
}
