import {Disclosure} from '@headlessui/react';
import {Heading, IconCaret, Link} from '~/components';

/**
 * A server component that specifies the content of the footer on the website
 */
export function FooterMenuMobile({
  firstRowLeft = [],
  firstRowRight = [],
  secondRow = [],
  thirdRow = [],
  fourthRow = [],
  others = [],
}) {
  const styles = {
    section: 'flex flex-col gap-6',
    nav: 'grid gap-6 pb-10',
    navSmall: 'grid gap-6 pb-8',
  };

  return (
    <footer
      role="contentinfo"
      className="min-h-[20rem] w-full gap-10 pb-8 px-6 md:px-8 overflow-hidden grid lg:hidden"
    >
      <div className="border-t border-mw-grayscale-200" />
      <div className="grid md:flex flex-wrap justify-between items-center gap-6">
        <div className="grid md:flex justify-start items-center gap-6">
          {(firstRowLeft || []).map((item) => (
            <Link
              key={item.id}
              to={item.to}
              target={item.target}
              className="text-subheading2 font-bold block md:inline"
            >
              {item.title}
            </Link>
          ))}
        </div>
        <div className="grid md:flex md:justify-end items-center gap-6">
          {(firstRowRight || []).map((item) => (
            <Link
              key={item.id}
              to={item.to}
              target={item.target}
              className="text-subheading2 font-bold block md:inline"
            >
              {item.title}
            </Link>
          ))}
        </div>
      </div>
      {fourthRow?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div className="block md:grid md:grid-cols-3 gap-6">
            {(fourthRow || []).map((item) => (
              <section key={item.id} className={styles.section}>
                <Disclosure>
                  {/* @ts-expect-error @headlessui/react incompatibility with node16 resolution */}
                  {({open}) => (
                    <>
                      <Disclosure.Button className="text-left md:cursor-default">
                        <Heading
                          className="flex justify-between text-lead font-bold"
                          size="none"
                          as="h3"
                        >
                          {item.title}
                          {item?.items?.length > 0 && (
                            <span className="md:hidden">
                              <IconCaret direction={open ? 'up' : 'down'} />
                            </span>
                          )}
                        </Heading>
                      </Disclosure.Button>
                      {item?.items?.length > 0 && (
                        <div
                          className={`${
                            open ? `h-fit` : `max-h-0 md:max-h-fit`
                          } overflow-hidden transition-all duration-300`}
                        >
                          <Disclosure.Panel static>
                            <nav className={styles.nav}>
                              {item.items.map((subItem) => (
                                <Link
                                  key={subItem.id}
                                  to={subItem.to}
                                  target={subItem.target}
                                >
                                  {subItem.title}
                                </Link>
                              ))}
                            </nav>
                          </Disclosure.Panel>
                        </div>
                      )}
                    </>
                  )}
                </Disclosure>
              </section>
            ))}
          </div>
        </>
      )}
      {secondRow?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div>
            {(secondRow || []).map((parentItem) => (
              <div className="grid gap-6" key={parentItem.id}>
                <div className="text-lead font-bold">
                  <Link
                    key={parentItem.id}
                    to={parentItem.to}
                    target={parentItem.target}
                    className="text-subheading2 font-bold block md:inline"
                  >
                    {parentItem.title}
                  </Link>
                </div>
                <div className="block md:grid md:grid-cols-3 gap-6">
                  {(parentItem.items || []).map((item) => (
                    <section key={item.id} className={styles.section}>
                      <Disclosure>
                        {/* @ts-expect-error @headlessui/react incompatibility with node16 resolution */}
                        {({open}) => (
                          <>
                            <Disclosure.Button className="text-left md:cursor-default">
                              <Heading
                                className="flex justify-between uppercase text-sm font-bold"
                                size="none"
                                as="h3"
                              >
                                {item.title}
                                {item?.items?.length > 0 && (
                                  <span className="md:hidden">
                                    <IconCaret
                                      direction={open ? 'up' : 'down'}
                                    />
                                  </span>
                                )}
                              </Heading>
                            </Disclosure.Button>
                            {item?.items?.length > 0 && (
                              <div
                                className={`${
                                  open ? `h-fit` : `max-h-0 md:max-h-fit`
                                } overflow-hidden transition-all duration-300`}
                              >
                                <Disclosure.Panel static>
                                  <nav className={styles.navSmall}>
                                    {item.items.map((subItem) => (
                                      <Link
                                        key={subItem.id}
                                        to={subItem.to}
                                        target={subItem.target}
                                      >
                                        {subItem.title}
                                      </Link>
                                    ))}
                                  </nav>
                                </Disclosure.Panel>
                              </div>
                            )}
                          </>
                        )}
                      </Disclosure>
                    </section>
                  ))}
                </div>
              </div>
            ))}
          </div>
        </>
      )}
      {thirdRow?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div className="block md:grid md:grid-cols-3 gap-6">
            {(thirdRow || []).map((item) => (
              <section key={item.id} className={styles.section}>
                <Disclosure>
                  {/* @ts-expect-error @headlessui/react incompatibility with node16 resolution */}
                  {({open}) => (
                    <>
                      <Disclosure.Button className="text-left md:cursor-default">
                        <Heading
                          className="flex justify-between"
                          size="lead"
                          as="h3"
                        >
                          {item.title}
                          {item?.items?.length > 0 && (
                            <span className="md:hidden">
                              <IconCaret direction={open ? 'up' : 'down'} />
                            </span>
                          )}
                        </Heading>
                      </Disclosure.Button>
                      {item?.items?.length > 0 && (
                        <div
                          className={`${
                            open ? `h-fit` : `max-h-0 md:max-h-fit`
                          } overflow-hidden transition-all duration-300`}
                        >
                          <Disclosure.Panel static>
                            <nav className={styles.nav}>
                              {item.items.map((subItem) => (
                                <Link
                                  key={subItem.id}
                                  to={subItem.to}
                                  target={subItem.target}
                                >
                                  {subItem.title}
                                </Link>
                              ))}
                            </nav>
                          </Disclosure.Panel>
                        </div>
                      )}
                    </>
                  )}
                </Disclosure>
              </section>
            ))}
          </div>
        </>
      )}
      {others?.length > 0 && (
        <>
          <div className="border-t border-mw-grayscale-200" />
          <div className="block md:grid md:grid-cols-3 gap-6">
            {(others || []).map((item) => (
              <section key={item.id} className={styles.section}>
                <Disclosure>
                  {/* @ts-expect-error @headlessui/react incompatibility with node16 resolution */}
                  {({open}) => (
                    <>
                      <Disclosure.Button className="text-left md:cursor-default">
                        <Heading
                          className="flex justify-between"
                          size="lead"
                          as="h3"
                        >
                          {item.title}
                          {item?.items?.length > 0 && (
                            <span className="md:hidden">
                              <IconCaret direction={open ? 'up' : 'down'} />
                            </span>
                          )}
                        </Heading>
                      </Disclosure.Button>
                      {item?.items?.length > 0 && (
                        <div
                          className={`${
                            open ? `h-fit` : `max-h-0 md:max-h-fit`
                          } overflow-hidden transition-all duration-300`}
                        >
                          <Disclosure.Panel static>
                            <nav className={styles.nav}>
                              {item.items.map((subItem) => (
                                <Link
                                  key={subItem.id}
                                  to={subItem.to}
                                  target={subItem.target}
                                >
                                  {subItem.title}
                                </Link>
                              ))}
                            </nav>
                          </Disclosure.Panel>
                        </div>
                      )}
                    </>
                  )}
                </Disclosure>
              </section>
            ))}
          </div>
        </>
      )}
    </footer>
  );
}
