import {Popover, Tab} from '@headlessui/react';
import {useLocation} from '@remix-run/react';
import {MediaFile} from '@shopify/hydrogen';
import logoImage from '~/assets/logo.png';
import bedPostImage from '~/assets/sleep-blog.png';
import {
  CartIcon,
  FreeDeliveryIcon,
  HeaderTopLocation,
  Heading,
  IconSearch,
  Link,
  MenuDrawer,
  MenuIcon,
  NearestStoresDetails,
  NearestStoresDrawer,
  Section,
  useDrawer,
} from '~/components';
import {ID_SELECT_A_STORE_TRIGGER} from '~/lib/const';
import {searchaniseWidgets} from '~/lib/searchaniseWidgets';
import {
  attachListrakListeners,
  removeSynchronyTag,
  toggleChat,
  toggleSelectAStore,
  triggerCartViewEvent,
  triggerSearchActionEvent,
} from '~/lib/utils';
import {useEffect, useState} from 'react';
import {FaMapMarkerAlt, FaUserCircle} from 'react-icons/fa';
import {HiChevronUp} from 'react-icons/hi';
import {useWindowScroll} from 'react-use';

// import {CartDrawer} from './CartDrawer.client';

export const ID_CART_DRAWER_TRIGGER = 'cart-drawer-trigger';

function openSelectAStore(event) {
  event?.preventDefault();
  document?.getElementById(ID_SELECT_A_STORE_TRIGGER)?.click();
}

/**
 * A client component that specifies the content of the header on the website
 */
export function Header({
  title,
  customerAccessToken,
  menu,
  promoBar,
  cartPromo,
  cartShippingNotice,
  mobileShippingPromo,
  nearestStoreLocations,
  warrantyVariant,
  carbonOffsetVariant,
  userLocation,
}) {
  const [location, setLocation] = useState(userLocation || '');
  const [loadingLocation, setLoadingLocation] = useState(false);
  const [nearestStoreLocs, setNearestStoreLocs] = useState(
    nearestStoreLocations || {},
  );

  const {pathname} = useLocation();

  const localeMatch = /^\/([a-z]{2})(\/|$)/i.exec(pathname);
  const countryCode = localeMatch ? localeMatch[1] : undefined;

  const isHome = pathname === `/${countryCode ? countryCode + '/' : ''}`;

  useEffect(() => {
    document.addEventListener('click', toggleChat);
    document.addEventListener('click', toggleSelectAStore);
    attachListrakListeners();
    if (!localStorage?.getItem('shopifyCartId')) {
      localStorage?.setItem('buyXGetYAttributes', '[]');
    } else {
      if (!localStorage?.getItem('buyXGetYAttributes')) {
        localStorage?.setItem('buyXGetYAttributes', '[]');
      }
    }
  }, []);

  useEffect(() => {
    if (!pathname?.startsWith('/products/') && !pathname?.startsWith('/cart')) {
      removeSynchronyTag();
    }

    const searchaniseInputWidgets = document.getElementById(
      'searchanise-widgets',
    );
    if (searchaniseInputWidgets) {
      searchaniseInputWidgets.remove();
    }

    if (window.innerWidth >= 1024) {
      const searchaniseDesktopWidgets = searchaniseWidgets(
        'searchanise-desktop-input',
        'searchanise-desktop-widgets',
      );
      if (searchaniseDesktopWidgets) {
        document.head.appendChild(searchaniseDesktopWidgets);
      }
    } else {
      const searchaniseMobileWidgets = searchaniseWidgets(
        'searchanise-mobile-input',
        'searchanise-mobile-widgets',
      );
      if (searchaniseMobileWidgets) {
        document.head.appendChild(searchaniseMobileWidgets);
      }
    }
  }, [pathname]);

  // useEffect(() => {
  //   window.addEventListener('mousemove', loadAdobe);
  //   window.addEventListener('scroll', loadAdobe);
  //
  //   return () => {
  //     window.removeEventListener('mousemove', loadAdobe);
  //     window.removeEventListener('scroll', loadAdobe);
  //   };
  // }, []);
  //
  // const loadAdobe = () => {
  //   const script = document.createElement('script');
  //   script.setAttribute(
  //     'src',
  //     'https://assets.adobedtm.com/543162830c7e/9961f8716735/launch-884682da5e0e.min.js',
  //   );
  //   document.head.appendChild(script);
  // };

  // const {
  //   isOpen: isCartOpen,
  //   openDrawer: openCart,
  //   closeDrawer: closeCart,
  // } = useDrawer();

  const {
    isOpen: isMenuOpen,
    openDrawer: openMenu,
    closeDrawer: closeMenu,
  } = useDrawer();

  const {
    isOpen: isNearestStoresOpen,
    openDrawer: openNearestStores,
    closeDrawer: closeNearestStores,
  } = useDrawer();

  const openDrawerCartAfterAddingToCart = (event) => {
    const btnAddToCart = event.target.closest('.btn-add-to-cart');
    if (btnAddToCart) {
      setTimeout(function () {
        openCartEvent();
      }, 700);
    }
  };

  useEffect(() => {
    window.addEventListener('click', openDrawerCartAfterAddingToCart);

    return () => {
      window.removeEventListener('click', openDrawerCartAfterAddingToCart);
    };
  }, []);

  const openCartEvent = () => {
    triggerCartViewEvent();
    // openCart();
  };

  return (
    <>
      <HeaderTop>
        <HeaderTopLocation
          openCart={openNearestStores}
          isOpen={isNearestStoresOpen}
          location={location}
          setLocation={setLocation}
          loadingLocation={loadingLocation}
          setLoadingLocation={setLoadingLocation}
        />
      </HeaderTop>
      {/*<CartDrawer*/}
      {/*  isOpen={isCartOpen}*/}
      {/*  onClose={closeCart}*/}
      {/*  cartPromo={cartPromo}*/}
      {/*  warrantyVariant={warrantyVariant}*/}
      {/*  cartShippingNotice={cartShippingNotice}*/}
      {/*  carbonOffsetVariant={carbonOffsetVariant}*/}
      {/*/>*/}
      <MenuDrawer
        isOpen={isMenuOpen}
        onClose={closeMenu}
        menu={menu}
        pathname={pathname}
      />
      <NearestStoresDrawer
        isOpen={isNearestStoresOpen}
        onClose={closeNearestStores}
      >
        <NearestStoresDetails
          location={location}
          setLocation={setLocation}
          loadingLocation={loadingLocation}
          setLoadingLocation={setLoadingLocation}
          setNearestStoreLocs={setNearestStoreLocs}
          nearestStoreLocs={nearestStoreLocs}
          onClose={closeNearestStores}
        />
      </NearestStoresDrawer>
      <DesktopHeader
        countryCode={countryCode}
        isHome={isHome}
        customerAccessToken={customerAccessToken}
        title={title}
        menu={menu}
        openCartEvent={openCartEvent}
        pathname={pathname}
      />
      <MobileHeader
        customerAccessToken={customerAccessToken}
        countryCode={countryCode}
        isHome={isHome}
        title={title}
        openCartEvent={openCartEvent}
        openMenu={openMenu}
        mobileShippingPromo={mobileShippingPromo}
      />
      <HeaderBottom promoBar={promoBar} />
    </>
  );
}

function HeaderTop({children}) {
  return (
    <header
      role="banner"
      className="w-full leading-none px-6 py-3 md:py-[0.938rem] bg-mw-grayscale-100"
    >
      <Section
        padding="none"
        className="px-0 md:px-8 lg:px-12 max-w-page-width mx-auto"
      >
        {children}
      </Section>
    </header>
  );
}

function HeaderBottom({promoBar}) {
  return (
    <header id="section-header-bottom" role="banner">
      <div className="hidden lg:block">
        <div className="w-full leading-none px-6 py-3 md:py-[0.938rem] bg-mw-grayscale-100">
          <Section
            padding="none"
            className="px-0 md:px-8 lg:px-12 max-w-page-width mx-auto"
          >
            {promoBar ? (
              <div
                className="flex justify-center gap-3"
                dangerouslySetInnerHTML={{__html: promoBar.body}}
              />
            ) : (
              <div>&nbsp;</div>
            )}
          </Section>
        </div>
      </div>
      <div className="block lg:hidden">
        <div className="w-full leading-none px-6 py-3 md:py-[0.938rem] bg-mw-grayscale-100">
          <Section
            padding="none"
            className="px-0 md:px-8 lg:px-12 max-w-page-width mx-auto"
          >
            {promoBar ? (
              <div
                className="flex justify-center gap-3"
                dangerouslySetInnerHTML={{__html: promoBar.body}}
              />
            ) : (
              <div>&nbsp;</div>
            )}
          </Section>
        </div>
      </div>
    </header>
  );
}

function MobileHeader({
  countryCode,
  title,
  isHome,
  openCartEvent,
  openMenu,
  customerAccessToken,
  mobileShippingPromo,
}) {
  const {y} = useWindowScroll();
  const [searchInputValue, setSearchInputValue] = useState('');

  const stickyHeight = 250;
  const styles = {
    button: 'relative flex items-center justify-center w-8 h-8',
    // container:
    //   'bg-contrast/80 text-primary flex lg:hidden items-center h-16 sticky backdrop-blur-lg z-40 top-0 justify-between w-full leading-none gap-4 px-4 md:px-8',
    container: `${
      y > stickyHeight ? 'shadow-borderHeader bg-red fixed' : ''
    } bg-contrast/80 text-primary flex lg:hidden items-center h-16 backdrop-blur-lg z-40 top-0 justify-between w-full leading-none gap-4 px-4 md:px-8`,
    stickyScrollPadding: `${y > stickyHeight ? 'block h-[64px]' : 'hidden'}`,
    stickyLogoSmall: `${
      y > stickyHeight ? 'hidden md:flex lg:hidden' : 'hidden'
    } font-bold items-center`,
  };

  return (
    <>
      <div className={styles.stickyScrollPadding}>&nbsp;</div>
      <header
        role="banner"
        className="flex lg:hidden justify-between items-center px-6 py-2 border-b border-mw-grayscale-300"
      >
        {logoImage ? (
          <>
            <Link
              className="flex items-center self-stretch justify-center flex-growl"
              to="/"
            >
              <Heading
                as={isHome ? 'h1' : 'h2'}
                className="w-full flex justify-between"
              >
                <img
                  src={logoImage}
                  width="1200"
                  height="354"
                  alt="Mattress Warehouse"
                  className="w-[124px]"
                />
              </Heading>
            </Link>
            <div className="flex flex-col items-center gap-1">
              <FreeDeliveryIcon />
              {mobileShippingPromo?.body !== undefined && (
                <div
                  className="font-bold text-sm"
                  dangerouslySetInnerHTML={{__html: mobileShippingPromo.body}}
                />
              )}
            </div>
          </>
        ) : (
          <Link
            className="flex items-center self-stretch justify-center flex-grow w-full h-full"
            to="/"
          >
            <Heading
              className="font-bold text-center"
              as={isHome ? 'h1' : 'h2'}
            >
              {title}
            </Heading>
          </Link>
        )}
      </header>
      <header
        id="section-mobile-header"
        role="banner"
        className={styles.container}
      >
        <div className="flex items-center justify-start gap-6 shrink-0">
          <button
            onClick={openMenu}
            className={`relative flex items-center justify-center w-8 h-8 border rounded-md border-mw-grayscale-300`}
          >
            <MenuIcon className="text-mw-grayscale-800" />
          </button>
          <Link className={styles.stickyLogoSmall} to="/">
            {logoImage ? (
              <img
                src={logoImage}
                width="1200"
                height="354"
                alt="Mattress Warehouse"
                className="w-[96px]"
              />
            ) : (
              {title}
            )}
          </Link>
        </div>
        <div className="flex items-center justify-end w-full gap-6 pr-2">
          <form
            action={`/${countryCode ? countryCode + '/' : ''}search`}
            onSubmit={() => triggerSearchActionEvent(searchInputValue, false)}
            className="flex items-center gap-2 relative border rounded-md border-mw-grayscale-300 flex-grow justify-end mr-2 max-w-sm"
          >
            <input
              autoComplete="off"
              className={`bg-transparent inline-block border-b transition border-transparent -mb-px border-x-0 border-t-0 appearance-none px-3 py-1 focus:ring-transparent w-full placeholder:text-mw-grayscale-700
                ${
                  isHome
                    ? 'focus:border-contrast/20 dark:focus:border-primary/20'
                    : 'focus:border-primary/20'
                }`}
              type="search"
              placeholder="Search the store..."
              id="searchanise-mobile-input"
              name="q"
              onChange={(event) => setSearchInputValue(event?.target?.value)}
            />
            <button type="submit" className={styles.button}>
              <IconSearch />
            </button>
          </form>
          <button
            onClick={openSelectAStore}
            className="relative items-center justify-center w-8 h-8 hidden md:flex"
            aria-label="Open Store Locator"
          >
            <FaMapMarkerAlt className="text-[2rem] text-mw-grayscale-800" />
          </button>
          <Link
            to={customerAccessToken ? '/account' : '/account/login'}
            className={styles.button}
          >
            <FaUserCircle className="text-4xl text-mw-grayscale-800" />
          </Link>
          <button onClick={openCartEvent} className={styles.button}>
            <CartIcon />
            <CartBadge dark={isHome} />
          </button>
        </div>
      </header>
    </>
  );
}

function DesktopHeader({
  countryCode,
  isHome,
  menu,
  openCartEvent,
  title,
  pathname,
  customerAccessToken,
}) {
  const {y} = useWindowScroll();
  const [searchInputValue, setSearchInputValue] = useState('');

  const styles = {
    button:
      'relative flex items-center justify-center w-8 h-8 focus:ring-primary/5',
    container: `${
      y > 50 ? 'shadow-borderHeader fixed' : ''
    } bg-contrast/80 text-primary hidden lg:flex items-center backdrop-blur-lg z-40 top-0 justify-between w-full leading-none`,
    stickyScrollPadding: `${y > 50 ? 'block h-[134px]' : 'hidden'}`,
  };

  const renderLv2Menu = (item) => {
    const isSleepGuide = item.title === 'Sleep Guide';
    return isSleepGuide ? (
      <div className="flex flex-col gap-5">
        <div className="flex flex-col gap-5">
          <div className="flex justify-center items-center gap-5">
            {(item?.items || []).map((itemLv2, index) => {
              const hasImage = !!itemLv2.resourceData?.image;
              const dataMedia = hasImage && {
                image: {
                  ...itemLv2.resourceData?.image,
                  altText: itemLv2.title,
                },
                mediaContentType: 'IMAGE',
              };
              const isBedPost =
                itemLv2?.type === 'HTTP' && itemLv2?.to === '/blogs'
                  ? true
                  : false;

              return index === 0 || index === 1 || index === 2 ? ( // show first 3 items
                <Popover.Button
                  as={Link}
                  key={itemLv2.id}
                  to={itemLv2.to}
                  target={itemLv2.target}
                  className={`${
                    itemLv2.to === pathname
                      ? 'border-mw-grayscale-700'
                      : 'border-transparent  hover:border-mw-grayscale-300 '
                  } cursor-pointer flex gap-1 items-center h-28 px-3 bg-white rounded-md basis-1/5 border hover:shadow-md hover:shadow-black/16`}
                >
                  {hasImage && (
                    <div className="w-[76px] h-[76px] rounded-full shrink-0">
                      <MediaFile
                        id={null}
                        data={dataMedia}
                        className="w-full rounded-lg"
                        options={{
                          crop: 'center',
                        }}
                      />
                    </div>
                  )}
                  {/* hard code menu image for The Bed Post */}
                  {isBedPost && (
                    <div className="w-[76px] h-[76px] rounded-full shrink-0">
                      <img
                        src={bedPostImage}
                        width="76"
                        height="76"
                        alt="The Bed Post"
                        className="w-[76px]"
                      />
                    </div>
                  )}
                  <div className="grow text-center text-lead">
                    {itemLv2.title}
                  </div>
                </Popover.Button>
              ) : null;
            })}
          </div>
        </div>
        <div className="flex flex-col border-b border-mw-grayscale-300 pb-3">
          <div>GUIDES</div>
        </div>
        <div className="flex flex-col gap-5">
          <div className="flex justify-center items-center gap-5">
            {(item?.items || []).map((itemLv2, index) => {
              return index === 0 || index === 1 || index === 2 ? null : ( // show the rest
                <Popover.Button
                  as={Link}
                  key={itemLv2.id}
                  to={itemLv2.to}
                  target={itemLv2.target}
                  className={`${
                    itemLv2.to === pathname
                      ? 'border-mw-grayscale-700'
                      : 'border-transparent  hover:border-mw-grayscale-300 '
                  } cursor-pointer flex gap-1 items-center h-28 px-3 bg-white rounded-md basis-1/5 border hover:shadow-md hover:shadow-black/16`}
                >
                  <div className="grow text-center text-lead">
                    {itemLv2.title}
                  </div>
                </Popover.Button>
              );
            })}
          </div>
        </div>
      </div>
    ) : (
      <>
        <div className="flex flex-col gap-5">
          <div className="flex justify-center items-center gap-5">
            {(item?.items || []).map((itemLv2, index) => {
              const hasImage = !!itemLv2.resourceData?.image;
              const dataMedia = hasImage && {
                image: {
                  ...itemLv2.resourceData?.image,
                  altText: itemLv2.title,
                },
                mediaContentType: 'IMAGE',
              };

              return index + 1 === item.items.length &&
                itemLv2?.title?.startsWith('All ') ? null : ( // hide last item at lv2 if it starts with 'All '
                <Popover.Button
                  as={Link}
                  key={itemLv2.id}
                  to={itemLv2.to}
                  target={itemLv2.target}
                  className={`${
                    itemLv2.to === pathname
                      ? 'border-mw-grayscale-700'
                      : 'border-transparent  hover:border-mw-grayscale-300 '
                  } cursor-pointer flex gap-1 items-center h-28 px-3 bg-white rounded-md basis-1/5 border hover:shadow-md hover:shadow-black/16`}
                >
                  {hasImage && (
                    <div className="w-[76px] h-[76px] rounded-full shrink-0">
                      <MediaFile
                        id={null}
                        data={dataMedia}
                        className="w-full rounded-lg"
                        options={{
                          crop: 'center',
                        }}
                      />
                    </div>
                  )}
                  <div className="grow text-center text-lead">
                    {itemLv2.title}
                  </div>
                </Popover.Button>
              );
            })}
          </div>
        </div>
        {(item?.items || []).map((itemLv2, index) => {
          return index + 1 === item.items.length &&
            itemLv2?.title?.startsWith('All ') ? ( // show last item at lv2 if it starts with 'All '
            <div className="flex justify-center" key={itemLv2.id}>
              <Popover.Button
                as={Link}
                to={itemLv2.to}
                target={itemLv2.target}
                className="cursor-pointer flex items-center px-6 py-3 font-bold text-white rounded-md border border-mw-red-500 bg-mw-red-500 hover:text-mw-red-500 hover:bg-white"
              >
                {itemLv2.title}
              </Popover.Button>
            </div>
          ) : null;
        })}
      </>
    );
  };

  const renderLv3Menu = (item) => {
    const tabList = [];
    const tabPanels = [];

    (item?.items || []).map((itemLv2, index) => {
      if (
        index + 1 === item.items.length &&
        itemLv2?.title?.startsWith('All ')
      ) {
        return; // hide last item at lv2 if it starts with 'All '
      } else {
        tabList.push({
          title: itemLv2.title,
          id: itemLv2.id,
        });
        const isShopByBrand = itemLv2.title === 'Shop by Brand';
        const isShopBySize = itemLv2.title === 'Shop by Size';
        tabPanels.push({
          items: itemLv2.items,
          id: itemLv2.id,
          isShopByBrand,
          isShopBySize,
        });
      }
    });

    return (
      <>
        <Tab.Group>
          <Tab.List className="flex justify-center items-center border-b border-mw-grayscale-200">
            {tabList.map((tab) => (
              <Tab className="basis-1/6" key={tab.id}>
                {({selected}) => (
                  <div
                    className={`${
                      selected ? 'bg-mw-grayscale-200' : ''
                    } w-full py-6 rounded-t-md text-mw-grayscale-800 font-bold`}
                  >
                    {tab.title}
                  </div>
                )}
              </Tab>
            ))}
          </Tab.List>
          <Tab.Panels>
            {tabPanels.map((panel) => (
              <Tab.Panel key={panel.id} className="flex flex-col gap-6">
                <div
                  className={`${
                    panel.isShopBySize ? 'gap-3' : 'gap-5'
                  } justify-center flex flex-wrap`}
                >
                  {(panel.items || []).map((itemLv3) => {
                    const hasImage = !!itemLv3.resourceData?.image;
                    const dataMedia = hasImage && {
                      image: {
                        ...itemLv3.resourceData?.image,
                        altText: itemLv3.title,
                      },
                      mediaContentType: 'IMAGE',
                    };
                    return itemLv3?.title?.startsWith(
                      'All ',
                    ) ? null : panel.isShopByBrand ? (
                      <Popover.Button
                        as={Link}
                        key={itemLv3.id}
                        to={itemLv3.to}
                        target={itemLv3.target}
                        className={`${
                          itemLv3.to === pathname
                            ? 'border-mw-grayscale-700'
                            : 'border-transparent  hover:border-mw-grayscale-300 '
                        } flex justify-center items-center h-16 p-3 bg-white rounded-md basis-[calc(16%-16px)] border hover:shadow-md hover:shadow-black/16`}
                      >
                        {hasImage ? (
                          <div className="flex justify-center items-center w-32 h-12">
                            <MediaFile
                              id={null}
                              data={dataMedia}
                              className="w-auto max-h-12"
                              options={{
                                crop: 'center',
                              }}
                            />
                          </div>
                        ) : (
                          <div className="flex justify-center items-center w-32 h-12 text-lead">
                            {itemLv3.title}
                          </div>
                        )}
                      </Popover.Button>
                    ) : (
                      <Popover.Button
                        as={Link}
                        key={itemLv3.id}
                        to={itemLv3.to}
                        target={itemLv3.target}
                        className={`${
                          panel.isShopBySize
                            ? 'basis-[calc(16%-8px)]'
                            : 'basis-[calc(20%-16px)]'
                        } ${
                          itemLv3.to === pathname
                            ? 'border-mw-grayscale-700'
                            : 'border-transparent  hover:border-mw-grayscale-300 '
                        } flex justify-center items-center h-28 px-3 bg-white rounded-md border border-transparent hover:border-mw-grayscale-300 hover:shadow-md hover:shadow-black/16`}
                      >
                        {hasImage && (
                          <div className="flex justify-center items-center w-[76px] h-[76px] shrink-0">
                            <MediaFile
                              id={null}
                              data={dataMedia}
                              className="w-auto max-h-[76px]"
                              options={{
                                crop: 'center',
                              }}
                            />
                          </div>
                        )}
                        <div className="grow text-center text-lead">
                          {itemLv3.title}
                        </div>
                      </Popover.Button>
                    );
                  })}
                </div>
                <div className="flex justify-center">
                  {(panel.items || []).map((itemLv3) => {
                    return itemLv3?.title?.startsWith('All ') ? (
                      <Popover.Button
                        as={Link}
                        key={itemLv3.id}
                        to={itemLv3.to}
                        target={itemLv3.target}
                        className="cursor-pointer flex items-center px-6 py-3 font-bold text-white rounded-md border border-mw-red-500 bg-mw-red-500 hover:text-mw-red-500 hover:bg-white"
                      >
                        {itemLv3.title}
                      </Popover.Button>
                    ) : null;
                  })}
                </div>
              </Tab.Panel>
            ))}
          </Tab.Panels>
        </Tab.Group>
      </>
    );
  };

  return (
    <>
      <div className={styles.stickyScrollPadding}>&nbsp;</div>
      <header role="banner" className={styles.container}>
        <div className="w-full" id="section-desktop-header">
          <div className="border-b border-mw-grayscale-300">
            <Section
              padding="none"
              className="px-0 md:px-8 lg:px-12 max-w-page-width mx-auto"
            >
              <div className="flex justify-between items-center gap-6">
                <div className="flex">
                  <Link className={`font-bold flex items-center py-4`} to="/">
                    {logoImage ? (
                      <img
                        src={logoImage}
                        width="1200"
                        height="354"
                        alt="Mattress Warehouse"
                        className="w-[150px]"
                      />
                    ) : (
                      {title}
                    )}
                  </Link>
                </div>
                <div className="flex items-center gap-4 grow justify-end">
                  <form
                    action={`/${countryCode ? countryCode + '/' : ''}search`}
                    onSubmit={() =>
                      triggerSearchActionEvent(searchInputValue, false)
                    }
                    className="flex items-center gap-2 relative border rounded-md border-mw-grayscale-300 flex-grow justify-end mr-2 max-w-sm"
                  >
                    <input
                      autoComplete="off"
                      className={`bg-transparent hidden md:inline-block border-b transition border-transparent -mb-px border-x-0 border-t-0 appearance-none px-3 py-1 focus:ring-transparent w-full placeholder:text-mw-grayscale-700
                        ${
                          isHome
                            ? 'focus:border-contrast/20 dark:focus:border-primary/20'
                            : 'focus:border-primary/20'
                        }`}
                      type="search"
                      placeholder="Search the store..."
                      id="searchanise-desktop-input"
                      name="q"
                      onChange={(event) =>
                        setSearchInputValue(event?.target?.value)
                      }
                    />
                    <button type="submit" className={styles.button}>
                      <IconSearch />
                    </button>
                  </form>
                  <button
                    onClick={openSelectAStore}
                    className="relative items-center justify-center w-8 h-8 hidden md:flex"
                    aria-label="Open Store Locator"
                  >
                    <FaMapMarkerAlt className="text-[2rem] text-mw-grayscale-800" />
                  </button>
                  <Link
                    to={customerAccessToken ? '/account' : '/account/login'}
                    className={styles.button}
                    arial-label="Account"
                  >
                    <FaUserCircle className="text-4xl text-mw-grayscale-800" />
                  </Link>
                  <button
                    id={ID_CART_DRAWER_TRIGGER}
                    onClick={openCartEvent}
                    className={styles.button}
                    aria-label="Cart"
                  >
                    <CartIcon />
                    <CartBadge dark={isHome} />
                  </button>
                </div>
              </div>
            </Section>
          </div>
          <nav className="flex justify-center">
            <section className="px-0 md:px-8 lg:px-12 max-w-page-width mx-auto">
              <Popover.Group className="flex flex-wrap gap-1">
                {/*Top level menu items*/}
                {(menu?.items || []).map((item) => {
                  if (
                    item.title === 'Customer Support' ||
                    item.title === 'About Us'
                  ) {
                    return; // hide Shop Info and About
                  }
                  const hasLv2Menu = !!item.items?.length;
                  const hasLv3Menu = (item.items || []).reduce(
                    (total, itemLv3) => {
                      return total || !!itemLv3.items?.length;
                    },
                    false,
                  );
                  return hasLv2Menu ? (
                    <Popover key={item.id}>
                      {({open}) => (
                        <>
                          <Popover.Button
                            className={`${
                              open ? 'bg-mw-grayscale-100' : ''
                            } flex items-center font-bold h-14 px-4`}
                          >
                            {item.title}
                            <HiChevronUp
                              className={`${
                                open ? '' : 'rotate-[180deg]'
                              } transition-transform transform-gpu duration-200 text-lead text-mw-grayscale-800`}
                            />
                          </Popover.Button>
                          <Popover.Panel className="absolute left-0 z-40 w-full bg-mw-grayscale-100">
                            <Section
                              padding="none"
                              className="px-0 md:px-8 lg:px-12 max-w-page-width mx-auto py-8"
                            >
                              {hasLv3Menu
                                ? renderLv3Menu(item)
                                : renderLv2Menu(item)}
                            </Section>
                          </Popover.Panel>
                        </>
                      )}
                    </Popover>
                  ) : (
                    <Link
                      key={item.id}
                      to={item.to}
                      target={item.target}
                      className="cursor-pointer flex items-center h-14 px-4 font-bold"
                    >
                      {item.title}
                    </Link>
                  );
                })}
              </Popover.Group>
            </section>
          </nav>
        </div>
      </header>
    </>
  );
}

function CartBadge({dark}) {
  // const {totalQuantity} = useCart();
  const totalQuantity = 0;
  const [refreshTotalQuantity, setRefreshTotalQuantity] =
    useState(totalQuantity);

  const refreshCountItem = (event) => {
    setRefreshTotalQuantity(event?.detail?.totalQuantity);
  };

  useEffect(() => {
    document.addEventListener('refreshCountItem', refreshCountItem);

    return () => {
      document.removeEventListener('refreshCountItem', refreshCountItem);
    };
  }, []);

  if (refreshTotalQuantity < 1) {
    return null;
  }
  return (
    <div
      className={`${
        dark ? 'text-contrast bg-mw-red-500' : 'text-contrast bg-mw-red-500'
      } absolute top-0 left-0 text-sm font-medium subpixel-antialiased h-5 min-w-[1.25rem] flex items-center justify-center leading-none text-center rounded-md w-auto px-[0.125rem] pb-px outline outline-1`}
    >
      <span>{refreshTotalQuantity}</span>
    </div>
  );
}
