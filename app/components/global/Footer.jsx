import logoImage from '~/assets/logo.png';
import {
  FooterForm,
  FooterMenuDesktop,
  FooterMenuMobile,
  Link,
  Section,
  SocialFacebookIcon,
  SocialInstagramIcon,
  SocialTwitterIcon,
  SocialYouTubeIcon,
} from '~/components';

/**
 * A helper function to prepare data for mobile and desktop footers.
 */
function FooterMenu({menu, customerAccessToken}) {
  let firstRowLeft = [];
  let secondRow = [];
  let thirdRow = [];
  let fourthRow = [];
  let others = [];
  (menu?.items || []).map((m) => {
    switch (m.title) {
      case 'Sale':
        // firstRowLeft.push(m); // hide from footer
        break;
      case 'Mattresses':
        secondRow.push(m);
        break;
      case 'Box Springs & Adjustable Beds':
      case 'Beds':
      case 'Bedding':
      case 'Accessories':
        thirdRow.push(m);
        break;
      case 'Customer Support':
      case 'Sleep Guide':
      case 'About Us':
        fourthRow.push(m);
        break;
      default:
        others.push(m);
        break;
    }
  });
  const moreFirstRowLeft = [
    {
      id: 'track-my-order',
      title: 'Track My Order',
      to: '/account/orders/track',
      target: '_self',
    },
    {
      id: 'delivery-information',
      title: 'Delivery Information',
      to: '/pages/shipping-and-delivery-policies',
      target: '_self',
    },
    {
      id: 'book-appointment',
      title: 'Book Appointment',
      to: '/contact/appointment',
      target: '_self',
    },
    {
      id: 'my-account',
      title: 'My Account',
      to: `${customerAccessToken ? '/account' : 'account/login'}`,
      target: '_self',
    },
  ];
  const firstRowRight = [
    {
      id: 'store-locator',
      title: 'Store Locator',
      to: '/store-locator',
      target: '_self',
    },
    {id: 'contact-us', title: 'Contact Us', to: '/contact', target: '_self'},
  ];
  firstRowLeft = firstRowLeft.concat(moreFirstRowLeft);
  // recreate the "Sale" menu item
  const moreThirdRow = [
    {
      id: 'sale-footer',
      title: 'Sale',
      url: '',
      items: [],
      to: '/collections/shop-mattress-sales',
    },
  ];
  thirdRow = thirdRow.concat(moreThirdRow);

  let secondRowMobile = JSON.parse(JSON.stringify(secondRow)); // deep copy
  const populateSecondRowMobile = () => {
    if (secondRowMobile.length > 0) {
      (secondRowMobile || []).map((parentItem) => {
        const lastItem = parentItem.items[parentItem.items?.length - 1];
        const lastItemTitle = lastItem.title || '';
        if (lastItemTitle?.startsWith('All ')) {
          parentItem.items?.splice(parentItem.items?.length - 1, 1);
        }
        (parentItem.items || []).map((item) => {
          const lastItem = item.items[item.items?.length - 1];
          const lastItemTitle = lastItem.title || '';
          if (lastItemTitle?.startsWith('All ')) {
            item.items?.unshift(item.items?.pop());
          }
        });
      });
    }
  };
  populateSecondRowMobile();

  let thirdRowMobile = JSON.parse(JSON.stringify(thirdRow)); // deep copy
  const populateThirdRowMobile = () => {
    (thirdRowMobile || []).map((item) => {
      const lastItem = item.items[item.items?.length - 1];
      const lastItemTitle = lastItem?.title || '';
      if (lastItemTitle?.startsWith('All ')) {
        item.items?.unshift(item.items?.pop());
      }
    });
  };
  populateThirdRowMobile();

  let secondRowDesktop = JSON.parse(JSON.stringify(secondRow)); // deep copy
  const populateSecondRowDesktop = () => {
    let doPopulate = false;
    let indexParentItem = null;
    let insertAt = null;
    let duplicateItem = null;
    if (secondRowDesktop.length > 0) {
      (secondRowDesktop || []).map((parentItem, indexParent) => {
        if (parentItem.title === 'Mattresses') {
          indexParentItem = indexParent;
          (parentItem.items || []).map((item, index) => {
            if (item.title === 'Shop by Brand' && item.items?.length > 5) {
              doPopulate = true;
              insertAt = index + 1;
              const lengthMinusOne = item.items.length - 1; // subtract 1 to account for the "All" link item
              const middle = Math.ceil(lengthMinusOne / 2);
              const firstHalf = item.items.slice(0, middle);
              const secondHalf = item.items.slice(middle);
              item.items = firstHalf;
              item.isClone = false;
              duplicateItem = {
                ...item,
                id: `${item.id}-${index}-duplicate`,
                items: secondHalf,
                isClone: true,
              };
            }
          });
        }
      });
    }
    if (doPopulate) {
      secondRowDesktop[indexParentItem].items.splice(
        insertAt,
        0,
        duplicateItem,
      );
      doPopulate = false;
    }
  };
  populateSecondRowDesktop();

  let thirdRowDesktop = JSON.parse(JSON.stringify(thirdRow)); // deep copy
  const populateThirdRowDesktop = (matchTitle) => {
    let doPopulate = false;
    let insertAt = null;
    let duplicateItem = null;
    if (thirdRowDesktop.length > 0) {
      (thirdRowDesktop || []).map((item, index) => {
        if (item.title === matchTitle && item.items?.length > 1) {
          doPopulate = true;
          insertAt = index + 1;
          const lengthMinusOne = item.items.length - 1; // subtract 1 to account for the "All" link item
          const middle = Math.ceil(lengthMinusOne / 2);
          const firstHalf = item.items.slice(0, middle);
          const secondHalf = item.items.slice(middle);
          item.items = firstHalf;
          item.isClone = false;
          duplicateItem = {
            ...item,
            id: `${item.id}-${index}-duplicate`,
            items: secondHalf,
            isClone: true,
          };
        }
      });
    }
    if (doPopulate) {
      thirdRowDesktop.splice(insertAt, 0, duplicateItem);
      doPopulate = false;
    }
  };
  populateThirdRowDesktop('Beds');
  populateThirdRowDesktop('Bedding');

  let fourthRowDesktop = JSON.parse(JSON.stringify(fourthRow)); // deep copy
  const populateFourthRowDesktop = (matchTitle) => {
    let doPopulate = false;
    let insertAt = null;
    let duplicateItem = null;
    if (fourthRowDesktop.length > 0) {
      (fourthRowDesktop || []).map((item, index) => {
        if (item.title === matchTitle && item.items?.length > 1) {
          doPopulate = true;
          insertAt = index + 1;
          const middle = Math.ceil(item.items.length / 2);
          const firstHalf = item.items.slice(0, middle);
          const secondHalf = item.items.slice(middle);
          item.items = firstHalf;
          item.isClone = false;
          duplicateItem = {
            ...item,
            id: `${item.id}-${index}-duplicate`,
            items: secondHalf,
            isClone: true,
          };
        }
      });
    }
    if (doPopulate) {
      fourthRowDesktop.splice(insertAt, 0, duplicateItem);
      doPopulate = false;
    }
  };
  populateFourthRowDesktop('Customer Support');
  populateFourthRowDesktop('Sleep Guide');
  populateFourthRowDesktop('About Us');

  return (
    <>
      <FooterMenuMobile
        firstRowLeft={firstRowLeft}
        firstRowRight={firstRowRight}
        secondRow={secondRowMobile}
        thirdRow={thirdRowMobile}
        fourthRow={fourthRow}
        others={others}
      />
      <FooterMenuDesktop
        firstRowLeft={firstRowLeft}
        firstRowRight={firstRowRight}
        secondRow={secondRowDesktop}
        thirdRow={thirdRowDesktop}
        fourthRow={fourthRowDesktop}
        others={others}
      />
    </>
  );
}

/**
 * A server component that specifies the content of the footer on the website
 */
export function Footer({menu, customerAccessToken}) {
  return (
    <>
      <FooterForm />
      <div className="bg-mw-grayscale-100">
        <Section
          as="footer"
          role="contentinfo"
          display="flex"
          padding="none"
          className="py-10 lg:py-8 px-6 md:px-8 lg:px-12 overflow-hidden flex-wrap gap-10 justify-between max-w-page-width mx-auto"
        >
          {logoImage ? (
            <Link
              className="flex items-center self-stretch justify-center flex-growl"
              to="/"
            >
              <img
                src={logoImage}
                width="1200"
                height="354"
                alt="Mattress Warehouse"
                className="w-[209px]"
              />
            </Link>
          ) : (
            <Link
              className="flex items-center self-stretch justify-center flex-grow w-full h-full"
              to="/"
            >
              Mattress Warehouse
            </Link>
          )}
          <div className="flex justify-center items-center gap-2.5">
            <Link to="https://twitter.com/MWarehouseLLC">
              <SocialTwitterIcon />
            </Link>
            <Link to="https://www.facebook.com/MattressWarehouseLLC">
              <SocialFacebookIcon />
            </Link>
            <Link to="https://www.instagram.com/mattresswarehousellc/">
              <SocialInstagramIcon />
            </Link>
            <Link to="https://www.youtube.com/c/mattresswarehouse">
              <SocialYouTubeIcon />
            </Link>
          </div>
        </Section>
        <FooterMenu menu={menu} customerAccessToken={customerAccessToken} />
        <Section
          as="footer"
          role="contentinfo"
          padding="none"
          className="bg-mw-red-600 text-white font-bold text-center py-5"
        >
          &copy; {new Date().getFullYear()} Mattress Warehouse
          <div className="flex justify-center items-center gap-2.5">
            <Link to={'/pages/privacy-policy'}>Privacy Policy</Link> |
            <Link to={'/pages/submission-policy'}>Submission Policy</Link>
          </div>
        </Section>
      </div>
    </>
  );
}
