import {KLAVIYO_PUBLIC_API_KEY, SIGNIFYD_URL} from '~/lib/const';
import {useEffect, useState} from 'react';

export function ScriptLauncher({pathname, storeDomain}) {
  useEffect(() => {
    const loadAdobeLaunch = () => {
      const existingScript = document.getElementById('adobeLaunch');
      if (!existingScript) {
        const script = document.createElement('script');
        script.src =
          'https://assets.adobedtm.com/543162830c7e/9961f8716735/launch-884682da5e0e.min.js';
        script.id = 'adobeLaunch';
        document.body.appendChild(script);
      }
    };

    if (document.readyState === 'complete') {
      loadAdobeLaunch();
    } else {
      window.addEventListener('load', loadAdobeLaunch);
    }

    return () => {
      window.removeEventListener('load', loadAdobeLaunch);
    };
  }, []);

  const createShopifyShop = () => {
    const script = document.createElement('script');
    script.innerHTML = `window.Shopify = { shop: "${storeDomain}"}`;
    document.head.appendChild(script);
  };

  useEffect(() => {
    let signifydScript = document.getElementById('signifydScript');
    let scriptTagTmx = document.getElementById('script-tag-tmx');
    if (!window?.Shopify?.shop) {
      createShopifyShop();
    }

    if (pathname?.startsWith('/products/')) {
      if (!signifydScript) {
        signifydScript = document.createElement('script');
        signifydScript.type = 'text/javascript';
        signifydScript.defer = true;
        signifydScript.id = 'signifydScript';
        signifydScript.src = SIGNIFYD_URL;
        document.head.appendChild(signifydScript);
      }
    } else {
      if (signifydScript) {
        document.head.removeChild(signifydScript);
      }

      if (scriptTagTmx) {
        document.head.removeChild(scriptTagTmx);
      }
    }
  }, [pathname]);

  const [triggered, setTriggered] = useState(false);

  useEffect(() => {
    if (triggered) {
      return;
    }
    const triggerInitialUserAction = () => {
      console.log('initial action');
      document.dispatchEvent(new CustomEvent('initialUserAction'));
      setTriggered(true);
    };
    window.addEventListener('mousemove', triggerInitialUserAction);
    window.addEventListener('scroll', triggerInitialUserAction);

    return () => {
      window.removeEventListener('mousemove', triggerInitialUserAction);
      window.removeEventListener('scroll', triggerInitialUserAction);
    };
  }, [triggered]);

  // load Klaviyo object
  useEffect(() => {
    const loadKlaviyoObj = () => {
      const existingScript = document.getElementById('klaviyo-obj');
      if (!existingScript) {
        const script = document.createElement('script');
        script.id = 'klaviyo-obj';
        script.type = 'text/javascript';
        const code =
          '!function(){if(!window.klaviyo){window._klOnsite=window._klOnsite||[];try{window.klaviyo=new Proxy({},{get:function(n,i){return"push"===i?function(){var n;(n=window._klOnsite).push.apply(n,arguments)}:function(){for(var n=arguments.length,o=new Array(n),w=0;w<n;w++)o[w]=arguments[w];var t="function"==typeof o[o.length-1]?o.pop():void 0,e=new Promise((function(n){window._klOnsite.push([i].concat(o,[function(i){t&&t(i),n(i)}]))}));return e}}})}catch(n){window.klaviyo=window.klaviyo||[],window.klaviyo.push=function(){var n;(n=window._klOnsite).push.apply(n,arguments)}}}}();';
        script.appendChild(document.createTextNode(code));
        document.body.appendChild(script);
      }
    };
    if (document.readyState === 'complete') {
      loadKlaviyoObj();
    } else {
      window.addEventListener('load', loadKlaviyoObj);
    }
    return () => {
      window.removeEventListener('load', loadKlaviyoObj);
    };
  }, []);

  // load Klaviyo.js
  useEffect(() => {
    const loadKlaviyoObj = () => {
      const existingScript = document.getElementById('klaviyo-js-init');
      if (!existingScript) {
        const script = document.createElement('script');
        script.id = 'klaviyo-js-init';
        script.src = `//static.klaviyo.com/onsite/js/klaviyo.js?company_id=${KLAVIYO_PUBLIC_API_KEY}`;
        document.body.appendChild(script);
      }
    };
    if (document.readyState === 'complete') {
      loadKlaviyoObj();
    } else {
      window.addEventListener('load', loadKlaviyoObj);
    }
    return () => {
      window.removeEventListener('load', loadKlaviyoObj);
    };
  }, []);
}
