import {Drawer} from '~/components';

export function NearestStoresDrawer({isOpen, onClose, children}) {
  return (
    <Drawer
      open={isOpen}
      onClose={onClose}
      heading="Stores Nearest You"
      openFrom="left"
    >
      <div className="grid">{children}</div>
    </Drawer>
  );
}
