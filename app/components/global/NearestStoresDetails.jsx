import 'moment-timezone';

import {Disclosure} from '@headlessui/react';
import {useNavigate} from '@remix-run/react';
import loadingImage from '~/assets/loading.gif';
import {Heading} from '~/components';
import {
  Link,
  UseBrowserGeolocator,
  geoLookupByLatLng,
  yextLocationDetailsByIds,
} from '~/components';
import {YEXT_API_DEFAULT_TIMEZONE} from '~/lib/const';
import {
  MW_DEFAULT_PHONE_NUMBER,
  convert24To12,
  formatPhoneNumber,
  generateGoogleMapLink,
  setCookie,
} from '~/lib/utils';
import moment from 'moment';
import {Suspense, useEffect, useRef, useState} from 'react';
import {HiChevronUp, HiLocationMarker, HiXCircle} from 'react-icons/hi';
import {useScroll} from 'react-use';

const getLocationDetails = async (locationDetailsQuery) => {
  const locationDetailsResponse = await fetch('/api/locationsByIds', {
    method: 'POST',
    body: JSON.stringify({locationDetailsQuery}),
  });
  const locationDetails = await locationDetailsResponse.json();
  return locationDetails;
};

const generateLocationDetailsQuery = ({nearestStoreLocs}) => {
  return (
    Object.keys(nearestStoreLocs || []).reduce((full, locationId) => {
      return `${full}
        locationId${locationId}: metafield(namespace: "location_details", key: "${locationId}") {
          value
        }`;
    }, '') || ''
  );
};

function slugify(str) {
  return String(str)
    .normalize('NFKD') // split accented characters into their base characters and diacritical marks
    .replace(/[\u0300-\u036f]/g, '') // remove all the accents, which happen to be all in the \u03xx UNICODE block.
    .trim() // trim leading or trailing whitespace
    .toLowerCase() // convert to lowercase
    .replace(/[^a-z0-9 -]/g, '') // remove non-alphanumeric characters
    .replace(/\s+/g, '_') // replace spaces with underscore
    .replace(/_+/g, '_'); // remove consecutive underscores
}

const stateSlug = {
  AZ: 'arizona',
  AL: 'alabama',
  AK: 'alaska',
  AR: 'arkansas',
  CA: 'california',
  CO: 'colorado',
  CT: 'connecticut',
  DC: 'district_of_clumbia',
  DE: 'delaware',
  FL: 'florida',
  GA: 'georgia',
  HI: 'hawaii',
  ID: 'idaho',
  IL: 'illinois',
  IN: 'indiana',
  IA: 'iowa',
  KS: 'kansas',
  KY: 'kentucky',
  LA: 'louisiana',
  ME: 'maine',
  MD: 'maryland',
  MA: 'massachusetts',
  MI: 'michigan',
  MN: 'minnesota',
  MS: 'mississippi',
  MO: 'missouri',
  MT: 'montana',
  NE: 'nebraska',
  NV: 'nevada',
  NH: 'new_hampshire',
  NJ: 'new_jersey',
  NM: 'new_mexico',
  NY: 'new_york',
  NC: 'north_carolina',
  ND: 'north_dakota',
  OH: 'ohio',
  OK: 'oklahoma',
  OR: 'oregon',
  PA: 'pennsylvania',
  RI: 'rhode_island',
  SC: 'south_carolina',
  SD: 'south_dakota',
  TN: 'tennessee',
  TX: 'texas',
  UT: 'utah',
  VT: 'vermont',
  VA: 'virginia',
  WA: 'washington',
  WV: 'west_virginia',
  WI: 'wisconsin',
  WY: 'wyoming',
};

/**
 * A client component that shows the content of the Nearest Stores drawer.
 * @paran {state} location
 * @param {function} location setter
 * @param {state} loadingLocation
 * @param {function} loadingLocation setter
 */
export function NearestStoresDetails({
  location,
  setLocation,
  loadingLocation,
  setLoadingLocation,
  setNearestStoreLocs,
  nearestStoreLocs,
  onClose,
}) {
  const locationInputRef = useRef(null);
  useEffect(() => {
    if (locationInputRef.current?.value) {
      locationInputRef.current.blur();
    } else {
      locationInputRef.current.focus();
    }
  }, [locationInputRef.current]);

  const scrollRef = useRef(null);
  const {y} = useScroll(scrollRef);

  const [locationInput, setLocationInput] = useState('');
  const [errorMsg, setErrorMsg] = useState('');
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');

  const reCityAndState = / ?([a-zA-Z0-9)]+), ([A-Z]{2})( \d{5})?, USA$/;
  const autoCompleteRef = useRef();
  const options = {
    componentRestrictions: {country: 'us'},
    strictbounds: true,
    fields: [
      'address_components',
      'geometry',
      'icon',
      'name',
      'formatted_address',
    ],
    types: ['geocode'],
  };

  useEffect(() => {
    if (location) {
      const {locality, state, country} = location;
      if (location?.locationInput) {
        setLocationInput(location?.locationInput || '');
      } else {
        setLocationInput(`${locality}, ${state} ${country || ''}`);
      }
      setLatitude(location?.latitude || '');
      setLongitude(location?.longitude || '');
    }
  }, [location]);

  // useEffect(() => {
  //   initialize();
  // }, []);

  useEffect(() => {
    if (
      locationInputRef?.current?.value?.length >= 3 && // do not initialize if the input is empty or less than 3 characters
      !autoCompleteRef?.current
    ) {
      initialize();
    }
  }, [locationInputRef?.current?.value]);

  const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };

  const enableEnterKey = (input) => {
    const _addEventListener = input.addEventListener; // Store original event listener

    const addEventListenerWrapper = (type, listener) => {
      if (type === 'keydown') {
        // Store existing listener function
        const _listener = listener;
        listener = (event) => {
          const lookupButton = document.getElementById('store-locator-look-up');
          // Simulate a 'down arrow' keypress if no address has been selected
          const suggestionSelected =
            document.getElementsByClassName('pac-item-selected').length;
          if (event.key === 'Enter' && !suggestionSelected) {
            const e = new KeyboardEvent('keydown', {
              key: 'ArrowDown',
              code: 'ArrowDown',
              keyCode: 40,
            });
            _listener.apply(input, [e]);
            // Simulate a click on Lookup button
            if (lookupButton) {
              sleep(500).then(() => lookupButton.click()); // 500ms delay to allow Google to populate the input field
            }
          }
          _listener.apply(input, [event]);
        };
      }
      _addEventListener.apply(input, [type, listener]);
    };

    input.addEventListener = addEventListenerWrapper;
  };

  const initialize = async () => {
    const {Autocomplete} = await google.maps.importLibrary('places');
    autoCompleteRef.current = new Autocomplete(
      locationInputRef.current,
      options,
    );
    autoCompleteRef.current.addListener('place_changed', async function () {
      const data = await autoCompleteRef.current.getPlace();
      setLatitude(data?.geometry?.location?.lat() || '');
      setLongitude(data?.geometry?.location?.lng() || '');
      setLocationInput(
        locationInputRef.current?.value || data?.formatted_address,
      );
    });
    enableEnterKey(locationInputRef.current);
  };

  const setNewNearestStoreLocs = (newNearestStoreLocs) => {
    const locationDetailsQuery = generateLocationDetailsQuery({
      nearestStoreLocs: newNearestStoreLocs,
    });
    getLocationDetails(locationDetailsQuery)
      .then((locationDetails) => {
        Object.entries(locationDetails || {}).map((loc) => {
          const locId =
            loc[0]?.indexOf('locationId') > -1
              ? loc[0]?.replace('locationId', '')
              : '00';
          const locInfo = JSON.parse(loc[1]?.value) || {};
          newNearestStoreLocs[locId] = {
            ...newNearestStoreLocs[locId],
            ...locInfo,
          };
          return loc;
        });
        setNearestStoreLocs(newNearestStoreLocs || {});
        setLoadingLocation(false);
        const locIds = Object.keys(newNearestStoreLocs || {});
        return yextLocationDetailsByIds({locIds});
      })
      .then((responseObj) => {
        const {response} = responseObj;
        const yextObj = response?.docs || {};
        Object.entries(yextObj || {}).map((loc) => {
          const locId = loc[1]?.id ? loc[1]?.id : '00';
          const locTimezone = loc[1]?.timezone || YEXT_API_DEFAULT_TIMEZONE;
          const locInfo = {
            timezone: locTimezone,
          };
          // only append timezone if the location data already exists
          if (newNearestStoreLocs?.[locId]) {
            newNearestStoreLocs[locId] = {
              ...newNearestStoreLocs[locId],
              ...locInfo,
            };
          }
          return loc;
        });
        setNearestStoreLocs(newNearestStoreLocs || {});
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {});
  };

  useEffect(() => {
    setNewNearestStoreLocs(nearestStoreLocs || {});
  }, [nearestStoreLocs]);

  const navigate = useNavigate();
  function handleBrowseByStateClick(event) {
    event.preventDefault();
    navigate('/store-locator', {replace: true});
    onClose();
  }

  function handleSubmit(event) {
    event.preventDefault();
    if (!longitude || !latitude || !locationInput?.length) {
      return;
    }
    const getNearestLocations = async (loc) => {
      const nearestLocationsResponse = await fetch('/api/nearestLocations', {
        method: 'POST',
        body: loc,
      });
      const nearestLocations = await nearestLocationsResponse.json();
      return nearestLocations;
    };

    setLoadingLocation(true);
    geoLookupByLatLng({latitude, longitude, locationInput})
      .then((loc) => {
        setLocation(loc);
        const locJson = JSON.stringify(loc) || '';
        setCookie('location', locJson, 7);
        return getNearestLocations(locJson);
      })
      .then((nearestLocations) => {
        setNewNearestStoreLocs(nearestLocations?.nearestStoreLocations || {});
        setErrorMsg('');
        // setLoadingLocation(false);
      })
      .catch((err) => {
        console.log(err);
        setLoadingLocation(false);
        setErrorMsg('Invalid Location.');
      });
  }

  function handleUseMyCurrentLocation(event) {
    event.preventDefault();
    setLoadingLocation(true);
    UseBrowserGeolocator()
      .then(async (position) => {
        const ll = {
          latitude: position?.coords?.latitude,
          longitude: position?.coords?.longitude,
        };
        return await geoLookupByLatLng(ll);
      })
      .then(async (loc) => {
        setLocation(loc);
        const locJson = JSON.stringify(loc) || '';
        setCookie('location', locJson, 7);
        setErrorMsg('');
        const nearestLocationsResponse = await fetch('/api/nearestLocations', {
          method: 'POST',
          body: locJson,
        });
        const nearestLocations = await nearestLocationsResponse.json();
        setNewNearestStoreLocs(nearestLocations?.nearestStoreLocations || {});
      })
      .catch((err) => {
        console.log(err);
        setLoadingLocation(false);
        setErrorMsg('Unable to retrieve your location.');
      });
  }

  return (
    <Suspense fallback={<div className="p-2">Loading…</div>}>
      <div className="grid grid-cols-1 h-screen-no-nav grid-rows-[1fr_auto]">
        <section
          ref={scrollRef}
          aria-labelledby="store-selector-heading"
          className={`overflow-auto transition ${y > 0 ? 'border-t' : ''}`}
        >
          <h2 id="store-selector-heading" className="sr-only">
            Stores Nearest You
          </h2>
          <div className="flex flex-col gap-6">
            <div className="bg-mw-grayscale-100 px-6 md:px-12">
              <div className="mt-5 mb-4 flex justify-between">
                <button
                  className={`${
                    loadingLocation ? 'opacity-50' : 'cursor-pointer'
                  } flex items-center gap-1.5`}
                  onClick={handleUseMyCurrentLocation}
                  aria-label="Use my current location"
                  disabled={loadingLocation}
                >
                  <span className="text-mw-red-500">
                    <HiLocationMarker />
                  </span>
                  <span className="text-sm text-mw-grayscale-800 font-bold underline underline-offset-4">
                    Use my current location
                  </span>
                </button>
                <button
                  onClick={handleBrowseByStateClick}
                  className="text-sm text-mw-grayscale-800 font-bold underline underline-offset-4"
                >
                  Browse by State
                </button>
              </div>
              <div className="flex flex-wrap md:flex-nowrap items-center gap-3">
                <div className="relative w-full">
                  <input
                    className={`${
                      loadingLocation && 'animate-pulse bg-mw-grayscale-200'
                    } w-full border-mw-grayscale-300 rounded-md`}
                    disabled={loadingLocation}
                    type="text"
                    placeholder="Enter a Location"
                    onChange={(event) => setLocationInput(event?.target?.value)}
                    value={locationInput}
                    ref={locationInputRef}
                  />
                  <HiXCircle
                    className="absolute right-[5px] top-[8px] w-[25px] h-[25px] text-mw-grayscale-300 cursor-pointer"
                    onClick={() => {
                      setLocationInput('');
                      locationInputRef.current.focus();
                    }}
                  />
                </div>
                <button
                  className={`${
                    loadingLocation
                      ? 'bg-mw-grayscale-200 border border-mw-grayscale-200'
                      : 'bg-mw-red-500 border border-mw-red-500 hover:text-mw-red-500 hover:bg-white'
                  } inline-block rounded-lg font-bold text-center text-contrast py-2 px-6 focus:shadow-outline whitespace-nowrap transition`}
                  onClick={handleSubmit}
                  aria-label="Look Up"
                  disabled={loadingLocation}
                  id="store-locator-look-up"
                >
                  Look Up
                </button>
              </div>
              <div className="my-1 ml-3">
                {errorMsg ? (
                  <div className="text-mw-red-500 text-sm">{errorMsg}</div>
                ) : (
                  <div className="text-sm">&nbsp;</div>
                )}
              </div>
            </div>
            <div className="px-6 md:px-12 pb-12">
              <div
                className={`flex flex-col gap-2 ${
                  !loadingLocation ? '' : 'justify-center items-center'
                }`}
              >
                {loadingLocation ? (
                  <img className="w-[80px]" alt="" src={loadingImage} />
                ) : (
                  <>
                    {Object.keys(nearestStoreLocs || {}).length === 0 && (
                      <div>
                        There are no stores within a 100 miles of your location.
                      </div>
                    )}
                    {Object.entries(nearestStoreLocs || {}).map(
                      (storeLoc, index) => {
                        const locId = storeLoc[0] || '00';
                        const locKey = `loc-${locId}-${index}`; // include map index to force re-render
                        const locInfo = storeLoc[1] || null;
                        let cityName = '';
                        let stateAbbrev = '';
                        let locationDetailHandle = '';
                        const foundCityAndState =
                          locInfo?.address?.match(reCityAndState);
                        // Two possible formats:
                        //   1. Springfield, VA, USA
                        //   2. 9400 Snowden River Pkwy #101, Columbia, MD 21045, USA
                        // Regex match result count is 3 for format 1, and 4 for format 2.
                        if (
                          foundCityAndState?.length === 3 ||
                          foundCityAndState?.length === 4
                        ) {
                          cityName = foundCityAndState?.[1] || '';
                          stateAbbrev = foundCityAndState?.[2] || '';
                          locationDetailHandle = `${slugify(
                            locInfo?.name,
                          )}-${locId}-${slugify(cityName)}-${
                            stateSlug[stateAbbrev]
                          }`;
                        }
                        const locTimezone = locInfo?.timezone || '';
                        const storeMoment = moment.tz(locTimezone);
                        // const storeMoment = moment.tz(locTimezone).add(-1, 'days'); // TODO: remove this line
                        const showHolidayHours =
                          locInfo?.hours?.holidayHours?.some(
                            (holiday) =>
                              storeMoment.diff(holiday?.date, 'days') >= -7 &&
                              storeMoment.diff(holiday?.date, 'days') <= 0,
                          ) || false;
                        return (
                          <Disclosure
                            as="div"
                            className="border border-mw-grayscale-300 rounded-md"
                            key={locKey}
                            defaultOpen={index === 0}
                          >
                            {({open}) => (
                              <>
                                <div className="flex flex-col gap-0 p-3">
                                  {locationDetailHandle ? (
                                    <>
                                      <Link
                                        to={`/store-locator/mattress-warehouse-of/${locationDetailHandle}`}
                                        className="inline-block w-fit text-lead font-bold hover:underline hover:underline-offset-2"
                                        onClick={onClose}
                                      >
                                        {locInfo?.name}
                                      </Link>
                                    </>
                                  ) : (
                                    <div className="text-lead font-bold whitespace-nowrap">
                                      {locInfo?.name}
                                    </div>
                                  )}
                                  <div className="w-full flex justify-end">
                                    <Disclosure.Button className="flex justify-between items-center underline underline-offset-2">
                                      <Heading size="copy">
                                        Quick Store Details
                                      </Heading>
                                      <HiChevronUp
                                        className={`${
                                          open ? '' : 'rotate-[180deg]'
                                        } transition-transform transform-gpu duration-200 text-heading text-mw-grayscale-800`}
                                      />
                                    </Disclosure.Button>
                                  </div>
                                </div>
                                <Disclosure.Panel>
                                  <div className="flex flex-col gap-2 p-3 bg-mw-grayscale-100">
                                    <div className="flex gap-3">
                                      {locInfo?.hours ? (
                                        <div className="flex flex-col gap-3 shrink-0">
                                          <div className="grid grid-cols-[32px_1fr] text-[13px]">
                                            <div>Sun</div>
                                            <div>
                                              {`${convert24To12(
                                                locInfo?.hours?.['sunday']
                                                  ?.openIntervals?.[0]?.start ||
                                                  '',
                                              )} - ${convert24To12(
                                                locInfo?.hours?.['sunday']
                                                  ?.openIntervals?.[0]?.end ||
                                                  '',
                                              )}`}
                                            </div>
                                            <div>Mon</div>
                                            <div>
                                              {`${convert24To12(
                                                locInfo?.hours?.['monday']
                                                  ?.openIntervals?.[0]?.start ||
                                                  '',
                                              )} - ${convert24To12(
                                                locInfo?.hours?.['monday']
                                                  ?.openIntervals?.[0]?.end ||
                                                  '',
                                              )}`}
                                            </div>
                                            <div>Tue</div>
                                            <div>
                                              {`${convert24To12(
                                                locInfo?.hours?.['tuesday']
                                                  ?.openIntervals?.[0]?.start ||
                                                  '',
                                              )} - ${convert24To12(
                                                locInfo?.hours?.['tuesday']
                                                  ?.openIntervals?.[0]?.end ||
                                                  '',
                                              )}`}
                                            </div>
                                            <div>Wed</div>
                                            <div>
                                              {`${convert24To12(
                                                locInfo?.hours?.['wednesday']
                                                  ?.openIntervals?.[0]?.start ||
                                                  '',
                                              )} - ${convert24To12(
                                                locInfo?.hours?.['wednesday']
                                                  ?.openIntervals?.[0]?.end ||
                                                  '',
                                              )}`}
                                            </div>
                                            <div>Thu</div>
                                            <div>
                                              {`${convert24To12(
                                                locInfo?.hours?.['thursday']
                                                  ?.openIntervals?.[0]?.start ||
                                                  '',
                                              )} - ${convert24To12(
                                                locInfo?.hours?.['thursday']
                                                  ?.openIntervals?.[0]?.end ||
                                                  '',
                                              )}`}
                                            </div>
                                            <div>Fri</div>
                                            <div>
                                              {`${convert24To12(
                                                locInfo?.hours?.['friday']
                                                  ?.openIntervals?.[0]?.start ||
                                                  '',
                                              )} - ${convert24To12(
                                                locInfo?.hours?.['friday']
                                                  ?.openIntervals?.[0]?.end ||
                                                  '',
                                              )}`}
                                            </div>
                                            <div>Sat</div>
                                            <div>
                                              {`${convert24To12(
                                                locInfo?.hours?.['saturday']
                                                  ?.openIntervals?.[0]?.start ||
                                                  '',
                                              )} - ${convert24To12(
                                                locInfo?.hours?.['saturday']
                                                  ?.openIntervals?.[0]?.end ||
                                                  '',
                                              )}`}
                                            </div>
                                          </div>
                                        </div>
                                      ) : (
                                        <div className="grid grid-cols-[32px_1fr] text-[13px]">
                                          <div></div>
                                          <div>(showing default hours)</div>
                                          <div>Sun</div>
                                          <div>11:00 AM - 6:00 PM</div>
                                          <div>Mon</div>
                                          <div>10:00 AM - 9:00 PM</div>
                                          <div>Tue</div>
                                          <div>10:00 AM - 9:00 PM</div>
                                          <div>Wed</div>
                                          <div>10:00 AM - 9:00 PM</div>
                                          <div>Thu</div>
                                          <div>10:00 AM - 9:00 PM</div>
                                          <div>Fri</div>
                                          <div>10:00 AM - 9:00 PM</div>
                                          <div>Sat</div>
                                          <div>10:00 AM - 8:00 PM</div>
                                        </div>
                                      )}
                                      <div className="grow-1 flex flex-col gap-3 text-[13px]">
                                        {locInfo?.phone && (
                                          <a
                                            href={`tel:${
                                              locInfo?.phone ||
                                              MW_DEFAULT_PHONE_NUMBER
                                            }`}
                                            className="font-bold hover:text-mw-red-500 hover:underline hover:underline-offset-1"
                                          >
                                            {formatPhoneNumber(
                                              locInfo?.phone ||
                                                MW_DEFAULT_PHONE_NUMBER,
                                            )}
                                          </a>
                                        )}
                                        <div className="break-all break-words">
                                          <a
                                            href="mailto:info@mattresswarehouse.com"
                                            className="font-bold hover:text-mw-red-500 hover:underline hover:underline-offset-1"
                                          >
                                            info@mattresswarehouse.com
                                          </a>
                                        </div>
                                        {/* <a href="mailto:info@mattresswarehouse.com">
                                      info@mattresswarehouse.com
                                    </a> */}
                                        <div className="flex flex-col gap-1">
                                          <div>{locInfo?.address}</div>
                                          <Link
                                            to={generateGoogleMapLink({
                                              address: locInfo?.address || '',
                                              googlePlaceId:
                                                locInfo?.googlePlaceId || '',
                                            })}
                                            target="_blank"
                                            className="underline underline-offset-1 font-bold hover:text-mw-red-500"
                                          >
                                            Get Directions
                                          </Link>
                                        </div>
                                      </div>
                                    </div>
                                    {showHolidayHours && (
                                      <div className="grid grid-cols-1 gap-1">
                                        <div className="text-sm font-bold">
                                          Holiday Hours
                                        </div>
                                        <div className="flex flex-col gap-1 text-[13px]">
                                          {locInfo?.hours?.holidayHours?.map(
                                            (holiday, index) => {
                                              const dateFormatted =
                                                holiday?.date
                                                  ? moment
                                                      .tz(holiday?.date, 'UTC')
                                                      .format(
                                                        'MMM DD, YYYY (ddd)',
                                                      )
                                                  : '';
                                              return (
                                                <div
                                                  className="grid grid-cols-[125px_1fr] gap-1"
                                                  // eslint-disable-next-line react/no-array-index-key
                                                  key={`${holiday?.date}-${index}`}
                                                >
                                                  <div>{dateFormatted}</div>
                                                  <div className="flex gap-3">
                                                    {holiday?.openIntervals?.map(
                                                      ({start, end}, index) => {
                                                        return (
                                                          <div key={index}>
                                                            <div>{`${convert24To12(
                                                              start || '',
                                                            )} - ${convert24To12(
                                                              end || '',
                                                            )}`}</div>
                                                          </div>
                                                        );
                                                      },
                                                    )}
                                                  </div>
                                                </div>
                                              );
                                            },
                                          )}
                                        </div>
                                      </div>
                                    )}
                                  </div>
                                </Disclosure.Panel>
                              </>
                            )}
                          </Disclosure>
                        );
                      },
                    )}
                  </>
                )}
              </div>
            </div>
          </div>
        </section>
      </div>
    </Suspense>
  );
}
