import {dataLayerForPages, remainingDataLayer} from '~/lib/dataLayer';
import {triggerCustomPageEvent} from '~/lib/utils';
import {useEffect} from 'react';

export function DataLayer({dataLayer}) {
  useEffect(() => {
    if (dataLayer) {
      triggerCustomPageEvent();
      const script = dataLayerForPages(dataLayer);
      document.body.appendChild(script);
      return () => {
        if (window?.dataLayer?.mwDataLayer?.page !== undefined) {
          delete window.dataLayer.mwDataLayer.page;
        }
        const newScript = remainingDataLayer();
        if (newScript === null) {
          document.body.removeChild(script);
        }
      };
    }
  }, [dataLayer]);

  useEffect(() => {
    const link = document.createElement('link');
    link.setAttribute('rel', 'canonical');
    link.setAttribute('href', window.location.href.split('?')[0]);
    document.head.appendChild(link);
    return () => {
      document.head.removeChild(link);
    };
  }, []);
}
