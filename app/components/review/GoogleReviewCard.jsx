import 'moment-timezone';

import googleLogoImage from '~/assets/google-logo-44x44.png';
import moment from 'moment';

export function GoogleReviewCard({review}) {
  const rating = review?.rating || 0;
  // We don't show any reviews that are 3 stars or lower
  const skip = rating < 4 ? true : false;
  const ratingStars = rating === 5 ? '100%' : rating === 4 ? '80%' : '100%';

  const content = review?.content || '';
  const authorName = review?.authorName || '';
  const publisherDate = review?.publisherDate
    ? moment(review?.publisherDate).format('ddd MMM D YYYY')
    : '';

  return skip ? (
    <></>
  ) : (
    <div className="flex border border-mw-grayscale-300 rounded-lg overflow-hidden p-4 sm:p-5 md:p-6 xl:p-[1.875rem]">
      <div className="w-full flex flex-col gap-3">
        <div className="flex flex-col md:flex-row md:justify-between md:items-center gap-2">
          <div className="flex gap-3 grow">
            <div className="shrink-0">
              <img
                src={googleLogoImage}
                width="48"
                height="48"
                alt="Customer Reviews"
              />
            </div>
            <div className="">
              <div className="inline-block">
                <div className="leading-5 relative text-[#ccc]">
                  <div
                    className="absolute w-0 overflow-hidden whitespace-nowrap text-[#ff8400]"
                    style={{width: ratingStars}}
                  >
                    <div className="before:content-['\2605'] w-6 h-6 inline" />
                    <div className="before:content-['\2605'] w-6 h-6 inline" />
                    <div className="before:content-['\2605'] w-6 h-6 inline" />
                    <div className="before:content-['\2605'] w-6 h-6 inline" />
                    <div className="before:content-['\2605'] w-6 h-6 inline" />
                  </div>
                  <div className="before:content-['\2605'] w-6 h-6 inline" />
                  <div className="before:content-['\2605'] w-6 h-6 inline" />
                  <div className="before:content-['\2605'] w-6 h-6 inline" />
                  <div className="before:content-['\2605'] w-6 h-6 inline" />
                  <div className="before:content-['\2605'] w-6 h-6 inline" />
                </div>
              </div>
              <div className="">
                Google <span className="text-mw-grayscale-400">&bull;</span>{' '}
                {authorName}
              </div>
            </div>
          </div>
          <div className="flex flex-col self-end md:self-auto">
            <div className="flex gap-1">{publisherDate} </div>
          </div>
        </div>
        <div className="text-base sm:text-lg">{content}</div>
      </div>
    </div>
  );
}
