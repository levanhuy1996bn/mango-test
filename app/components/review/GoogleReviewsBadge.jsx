import googleLogoImage from '~/assets/google-logo-44x44.png';
import {Link} from '~/components';
import {YEXT_MANAGEMENT_API_KEY, YEXT_MANAGEMENT_API_VER} from '~/lib/const';
import {useEffect, useState} from 'react';

export function GoogleReviewsBadge() {
  const [isPending, setIsPending] = useState(true);
  const [totalCount, setTotalCount] = useState(0);
  const [averageRating, setAverageRating] = useState(0);

  /**
   * Format a 5 star rating to a percentage string.
   */
  const formatRating = (rating) => {
    const rawValue = rating ? rating * 2 * 10 : 0;
    const fixed = rawValue.toFixed(0);
    return fixed ? fixed + '%' : '0%';
  };

  /**
   * Format a number as a string with commas separating the thousands.
   * @param num - The number to be formatted (e.g. 10000)
   * @return A string representing the formatted number (e.g. "10,000")
   */
  const formatNumber = (num) => {
    var array = num.toString().split('');
    var index = -3;
    while (array.length + index > 0) {
      array.splice(index, 0, ',');
      // Decrement by 4 since we just added another unit to the array.
      index -= 4;
    }
    return array.join('');
  };

  /**
   * Get the summary of reviews.
   * Step 1: Request proxy (Google Apps Script) => Yext API => create Google User Content
   * Step 2: Access the created Google User Content
   */
  const getSummary = () => {
    setIsPending(true);
    // We must use the old domain (api.yext.com) for the management API. The new domain (api.yextapis.com) does not work.
    //   See: https://hitchhikers.yext.com/docs/managementapis/introduction/overview-policies-and-conventions/?target=management-api
    const urlYextApi = `https://api.yext.com/v2/accounts/me/reviews?api_key=${YEXT_MANAGEMENT_API_KEY}&publisherIds=GOOGLEMYBUSINESS&v=${YEXT_MANAGEMENT_API_VER}`;
    const proxy =
      'https://script.google.com/macros/s/AKfycbxH_t0MnnzTDvWnGKwpyIJUkJJqpuBOiZjwnerTgtGLsONojZg/exec?url=' +
      encodeURIComponent(urlYextApi);
    // Step 1
    fetch(proxy, {
      method: 'GET',
    })
      .then(async (responseProxy) => {
        const urlGoogleUserContent = responseProxy?.url || '';
        if (!urlGoogleUserContent) {
          throw new Error('Response from proxy did not contain a URL.');
        }
        // Step 2
        const response = await fetch(urlGoogleUserContent, {
          method: 'GET',
        });
        return await response?.text();
      })
      .then((data) => {
        // trim 'callback(' and ')' from the response content
        const trimmed = data?.substring(9, data?.length - 1) || '{}';
        let responseSummary = {};
        try {
          responseSummary = JSON.parse(trimmed);
        } catch (error) {
          throw new Error('Failed to parse response content.');
        }
        const totalCount = responseSummary?.response?.count || 0;
        const averageRating = responseSummary?.response?.averageRating || 0;
        setTotalCount(totalCount);
        setAverageRating(averageRating);
      })
      .finally(() => {
        setIsPending(false);
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  useEffect(() => {
    getSummary();
  }, []);

  return (
    <div className={`${isPending ? 'animate-pulse' : ''}`}>
      <div className="hidden sm:block fixed top-auto bottom-[110px] left-0 z-30">
        {isPending ? (
          <div className="m-1 p-2.5 text-mw-grayscale-500">Loading...</div>
        ) : totalCount ? (
          <div className="bg-white m-1 p-2.5 border border-mw-grayscale-200 rounded-md">
            <div className="flex gap-2">
              <img
                src={googleLogoImage}
                width="28"
                height="22"
                alt="Customer Reviews"
              />
              <div className="text-lg font-bold">
                {averageRating ? parseFloat(averageRating).toFixed(1) : '...'}
              </div>
            </div>
            <div className="inline-block">
              <div className="leading-5 relative text-[#ccc]">
                <div
                  className="absolute w-0 overflow-hidden whitespace-nowrap text-[#ff8400]"
                  style={{
                    width: averageRating ? formatRating(averageRating) : '0%',
                  }}
                >
                  <div className="before:content-['\2605'] w-4 h-4 inline" />
                  <div className="before:content-['\2605'] w-4 h-4 inline" />
                  <div className="before:content-['\2605'] w-4 h-4 inline" />
                  <div className="before:content-['\2605'] w-4 h-4 inline" />
                  <div className="before:content-['\2605'] w-4 h-4 inline" />
                </div>
                <div className="before:content-['\2605'] w-4 h-4 inline" />
                <div className="before:content-['\2605'] w-4 h-4 inline" />
                <div className="before:content-['\2605'] w-4 h-4 inline" />
                <div className="before:content-['\2605'] w-4 h-4 inline" />
                <div className="before:content-['\2605'] w-4 h-4 inline" />
              </div>
            </div>
            <div>
              <Link to="/pages/customer-reviews" className="text-sm">
                See all {totalCount ? formatNumber(totalCount) : '...'} reviews
              </Link>
            </div>
          </div>
        ) : (
          // No reviews found
          <></>
        )}
      </div>
    </div>
  );
}
