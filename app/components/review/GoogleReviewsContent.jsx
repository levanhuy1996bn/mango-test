import {GoogleReviewCard} from '~/components';
import {YEXT_MANAGEMENT_API_KEY, YEXT_MANAGEMENT_API_VER} from '~/lib/const';
import {useEffect, useState} from 'react';

export function GoogleReviewsContent() {
  const [isPending, setIsPending] = useState(true);
  const [reviews, setReviews] = useState([]);

  /**
   * Get the summary of reviews.
   * Step 1: Request proxy (Google Apps Script) => Yext API => create Google User Content
   * Step 2: Access the created Google User Content
   */
  const getSummary = () => {
    setIsPending(true);
    const limit = 100;
    // We must use the old domain (api.yext.com) for the management API. The new domain (api.yextapis.com) does not work.
    //   See: https://hitchhikers.yext.com/docs/managementapis/introduction/overview-policies-and-conventions/?target=management-api
    const urlYextApi = `https://api.yext.com/v2/accounts/me/reviews?api_key=${YEXT_MANAGEMENT_API_KEY}&limit=${limit}&publisherIds=GOOGLEMYBUSINESS&v=${YEXT_MANAGEMENT_API_VER}`;
    const proxy =
      'https://script.google.com/macros/s/AKfycbxH_t0MnnzTDvWnGKwpyIJUkJJqpuBOiZjwnerTgtGLsONojZg/exec?url=' +
      encodeURIComponent(urlYextApi);
    // Step 1
    fetch(proxy, {
      method: 'GET',
    })
      .then(async (responseProxy) => {
        const urlGoogleUserContent = responseProxy?.url || '';
        if (!urlGoogleUserContent) {
          throw new Error('Response from proxy did not contain a URL.');
        }
        // Step 2
        const response = await fetch(urlGoogleUserContent, {
          method: 'GET',
        });
        return await response?.text();
      })
      .then((data) => {
        // trim 'callback(' and ')' from the response content
        const trimmed = data?.substring(9, data?.length - 1) || '{}';
        let responseSummary = {};
        try {
          responseSummary = JSON.parse(trimmed);
        } catch (error) {
          throw new Error('Failed to parse response content.');
        }
        const reviews = responseSummary?.response?.reviews || [];
        setReviews(reviews);
      })
      .finally(() => {
        setIsPending(false);
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  useEffect(() => {
    getSummary();
  }, []);

  return (
    <div>
      {isPending ? (
        <div className="">Loading reviews ...</div>
      ) : reviews?.length ? (
        <div className="flex flex-col gap-5">
          {reviews.map((review, index) => {
            return (
              <GoogleReviewCard review={review} key={review?.id || index} />
            );
          })}
        </div>
      ) : (
        <div></div>
      )}
    </div>
  );
}
