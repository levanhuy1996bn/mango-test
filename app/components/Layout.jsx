import {Header} from '~/components';
import {Footer} from '~/components/global/Footer';
import {Suspense} from 'react';
export function Layout({
  children,
  carbonOffsetVariant,
  customerAccessToken,
  nearestStoreLocations,
  warrantyVariant,
  userLocation,
  shopData,
}) {
  return (
    <>
      <div className="flex flex-col min-h-screen">
        <div className="">
          <a href="#mainContent" className="sr-only">
            Skip to content
          </a>
        </div>
        <HeaderWithMenu
          customerAccessToken={customerAccessToken}
          nearestStoreLocations={nearestStoreLocations}
          warrantyVariant={warrantyVariant}
          carbonOffsetVariant={carbonOffsetVariant}
          userLocation={userLocation}
          shopData={shopData}
        />
        <main role="main" id="mainContent" className="flex-grow">
          {children}
        </main>
      </div>
      <Suspense fallback={<Footer />}>
        <FooterWithMenu
          customerAccessToken={customerAccessToken}
          shopData={shopData}
        />
      </Suspense>
    </>
  );
}

function HeaderWithMenu({
  nearestStoreLocations,
  warrantyVariant,
  carbonOffsetVariant,
  customerAccessToken,
  userLocation,
  shopData,
}) {
  const {
    shopName,
    headerMenu,
    blockPromoBar,
    blockCartPromo,
    blockCartShippingNotice,
    blockMobileShippingPromo,
  } = shopData;
  return (
    <Header
      title={shopName}
      customerAccessToken={customerAccessToken}
      menu={headerMenu}
      promoBar={blockPromoBar}
      cartPromo={blockCartPromo}
      cartShippingNotice={blockCartShippingNotice}
      mobileShippingPromo={blockMobileShippingPromo}
      nearestStoreLocations={nearestStoreLocations}
      warrantyVariant={warrantyVariant}
      carbonOffsetVariant={carbonOffsetVariant}
      userLocation={userLocation}
    />
  );
}

function FooterWithMenu({customerAccessToken, shopData}) {
  const {headerMenu} = shopData;
  return <Footer menu={headerMenu} customerAccessToken={customerAccessToken} />;
}
