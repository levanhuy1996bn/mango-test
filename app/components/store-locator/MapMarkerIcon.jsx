import {FaMapMarkerAlt} from 'react-icons/fa';
export function MapMarkerIcon({className = ''}) {
  return <FaMapMarkerAlt className={className} />;
}
