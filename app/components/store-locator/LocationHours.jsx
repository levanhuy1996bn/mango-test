import 'moment-timezone';

import {convert24To12} from '~/lib/utils';
import moment from 'moment';

export function LocationHours({locHours, locTimezone}) {
  const storeMoment = moment.tz(locTimezone);
  const storeWeekDay = storeMoment.format('dddd').toLowerCase();
  const isHoliday = locHours?.holidayHours?.some((holiday) =>
    storeMoment.isSame(holiday?.date, 'day'),
  );
  const showHolidayHours =
    locHours?.holidayHours?.some(
      (holiday) =>
        storeMoment.diff(holiday?.date, 'days') >= -7 &&
        storeMoment.diff(holiday?.date, 'days') <= 0,
    ) || false;
  const timeStr = storeMoment.format('HH:mm');
  const storeTime = moment.tz(timeStr, 'HH:mm', locTimezone);
  const startTime = locHours?.[storeWeekDay]?.openIntervals?.[0]?.start
    ? moment.tz(
        locHours?.[storeWeekDay]?.openIntervals?.[0]?.start,
        'HH:mm',
        locTimezone,
      )
    : '';
  const endTime = locHours?.[storeWeekDay]?.openIntervals?.[0]?.end
    ? moment.tz(
        locHours?.[storeWeekDay]?.openIntervals?.[0]?.end,
        'HH:mm',
        locTimezone,
      )
    : '';
  const isOpen = storeTime ? storeTime?.isBetween(startTime, endTime) : false;
  const closesAt = endTime ? endTime?.format('h:mm a') : '';

  return (
    <div className="flex flex-col gap-8">
      <div className="flex flex-col gap-5">
        <div className="text-2xl md:text-[1.6875rem] font-bold">
          Store Hours
        </div>
        <div className="flex flex-col gap-3">
          {!isHoliday && isOpen && (
            <div className="w-fit border rounded-lg bg-[#ECF7EA] border-[#3D931F] px-3 py-2">
              <div className="text-sm font-bold text-[#3D931F]">
                {`Open Now - Closes at ${closesAt}`}
              </div>
            </div>
          )}
          {locHours ? (
            <div className="grid grid-cols-[80px_1fr] gap-2">
              <div
                className={
                  !isHoliday && storeWeekDay === 'sunday' ? 'font-bold' : ''
                }
              >
                Sunday
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'sunday' ? 'font-bold' : ''
                }
              >
                {locHours?.['sunday']?.openIntervals
                  ? `${convert24To12(
                      locHours?.['sunday']?.openIntervals?.[0]?.start || '',
                    )} - ${convert24To12(
                      locHours?.['sunday']?.openIntervals?.[0]?.end || '',
                    )}`
                  : locHours?.['sunday']?.isClosed
                  ? 'Closed'
                  : ''}
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'monday' ? 'font-bold' : ''
                }
              >
                Monday
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'monday' ? 'font-bold' : ''
                }
              >
                {locHours?.['monday']?.openIntervals
                  ? `${convert24To12(
                      locHours?.['monday']?.openIntervals?.[0]?.start || '',
                    )} - ${convert24To12(
                      locHours?.['monday']?.openIntervals?.[0]?.end || '',
                    )}`
                  : locHours?.['monday']?.isClosed
                  ? 'Closed'
                  : ''}
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'tuesday' ? 'font-bold' : ''
                }
              >
                Tuesday
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'tuesday' ? 'font-bold' : ''
                }
              >
                {locHours?.['tuesday']?.openIntervals
                  ? `${convert24To12(
                      locHours?.['tuesday']?.openIntervals?.[0]?.start || '',
                    )} - ${convert24To12(
                      locHours?.['tuesday']?.openIntervals?.[0]?.end || '',
                    )}`
                  : locHours?.['tuesday']?.isClosed
                  ? 'Closed'
                  : ''}
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'wednesday' ? 'font-bold' : ''
                }
              >
                Wednesday
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'wednesday' ? 'font-bold' : ''
                }
              >
                {locHours?.['wednesday']?.openIntervals
                  ? `${convert24To12(
                      locHours?.['wednesday']?.openIntervals?.[0]?.start || '',
                    )} - ${convert24To12(
                      locHours?.['wednesday']?.openIntervals?.[0]?.end || '',
                    )}`
                  : locHours?.['wednesday']?.isClosed
                  ? 'Closed'
                  : ''}
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'thursday' ? 'font-bold' : ''
                }
              >
                Thursday
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'thursday' ? 'font-bold' : ''
                }
              >
                {locHours?.['thursday']?.openIntervals
                  ? `${convert24To12(
                      locHours?.['thursday']?.openIntervals?.[0]?.start || '',
                    )} - ${convert24To12(
                      locHours?.['thursday']?.openIntervals?.[0]?.end || '',
                    )}`
                  : locHours?.['thursday']?.isClosed
                  ? 'Closed'
                  : ''}
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'friday' ? 'font-bold' : ''
                }
              >
                Friday
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'friday' ? 'font-bold' : ''
                }
              >
                {locHours?.['friday']?.openIntervals
                  ? `${convert24To12(
                      locHours?.['friday']?.openIntervals?.[0]?.start || '',
                    )} - ${convert24To12(
                      locHours?.['friday']?.openIntervals?.[0]?.end || '',
                    )}`
                  : locHours?.['friday']?.isClosed
                  ? 'Closed'
                  : ''}
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'saturday' ? 'font-bold' : ''
                }
              >
                Saturday
              </div>
              <div
                className={
                  !isHoliday && storeWeekDay === 'saturday' ? 'font-bold' : ''
                }
              >
                {locHours?.['saturday']?.openIntervals
                  ? `${convert24To12(
                      locHours?.['saturday']?.openIntervals?.[0]?.start || '',
                    )} - ${convert24To12(
                      locHours?.['saturday']?.openIntervals?.[0]?.end || '',
                    )}`
                  : locHours?.['saturday']?.isClosed
                  ? 'Closed'
                  : ''}
              </div>
            </div>
          ) : (
            <div className="grid grid-cols-[80px_1fr] gap-2">
              <div>(showing default hours)</div>
              <div>Sunday</div>
              <div>11:00 AM - 6:00 PM</div>
              <div>Monday</div>
              <div>10:00 AM - 9:00 PM</div>
              <div>Tuesday</div>
              <div>10:00 AM - 9:00 PM</div>
              <div>Wednesday</div>
              <div>10:00 AM - 9:00 PM</div>
              <div>Thursday</div>
              <div>10:00 AM - 9:00 PM</div>
              <div>Friday</div>
              <div>10:00 AM - 9:00 PM</div>
              <div>Saturday</div>
              <div>10:00 AM - 8:00 PM</div>
            </div>
          )}
        </div>
      </div>
      {showHolidayHours && (
        <div className="flex flex-col gap-5">
          <div className="text-xl font-bold">Holiday Hours</div>
          <div className="flex flex-col gap-3">
            {isHoliday && isOpen && (
              <div className="w-fit border rounded-lg bg-[#ECF7EA] border-[#3D931F] px-3 py-2">
                <div className="text-sm font-bold text-[#3D931F]">
                  {`Open Now - Closes at ${closesAt}`}
                </div>
              </div>
            )}
            <div className="flex flex-col gap-2.5">
              {(locHours?.holidayHours || []).map((holiday, index) => {
                const dateFormatted = holiday?.date
                  ? moment.tz(holiday?.date, 'UTC').format('MMM DD, YYYY (ddd)')
                  : '';
                return (
                  <div
                    className={`${
                      storeMoment.isSame(holiday?.date, 'day') && 'font-bold'
                    } grid grid-cols-[125px_1fr] gap-3 text-sm`}
                    // eslint-disable-next-line react/no-array-index-key
                    key={`${holiday?.date}-${index}`}
                  >
                    <div>{dateFormatted}</div>
                    <div className="flex flex-wrap gap-3">
                      {(holiday?.openIntervals || []).map(
                        ({start, end}, index) => {
                          return (
                            <div
                              // eslint-disable-next-line react/no-array-index-key
                              key={`${holiday?.date}-${index}-${start}-${end}`}
                            >
                              {`${convert24To12(start || '')} - ${convert24To12(
                                end || '',
                              )}`}
                            </div>
                          );
                        },
                      )}
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}
