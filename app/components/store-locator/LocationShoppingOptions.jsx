import {Image} from '@shopify/hydrogen';
import imgCurbsidePickup from '~/assets/iconImages/MW-Icon-CurbsidePickup.png';
import imgInStorePickup from '~/assets/iconImages/MW-Icon-InStorePickup.png';
import imgNextDayDelivery from '~/assets/iconImages/MW-Icon-NextDayDelivery.png';

export function LocationShoppingOptions({locPickupAndDeliveryServices}) {
  if (locPickupAndDeliveryServices?.length > 0) {
    return (
      <div className="flex flex-col gap-8">
        <h2 className="text-2xl lg:text-[1.6875rem] font-bold">
          Shopping Options
        </h2>
        <div className="flex flex-col md:flex-row md:flex-wrap gap-8">
          {locPickupAndDeliveryServices.map((item, index) => {
            if (item === 'IN_STORE_PICKUP') {
              return (
                // eslint-disable-next-line react/no-array-index-key
                <div className="flex items-center gap-2" key={item + index}>
                  <img
                    src={imgInStorePickup}
                    width="auto"
                    height="auto"
                    alt="In Store Pickup"
                    className="font-bold"
                  />
                  <div className="font-bold">
                    In-Store
                    <br />
                    Pickup
                  </div>
                </div>
              );
            }
            if (item === 'CURBSIDE_PICKUP') {
              return (
                // eslint-disable-next-line react/no-array-index-key
                <div className="flex items-center gap-2" key={item + index}>
                  <img
                    src={imgCurbsidePickup}
                    width="auto"
                    height="auto"
                    alt="Curbside Pickup"
                  />
                  <div className="font-bold">
                    Curbside
                    <br />
                    Pickup
                  </div>
                </div>
              );
            }
            if (item === 'DELIVERY') {
              return (
                // eslint-disable-next-line react/no-array-index-key
                <div className="flex items-center gap-2" key={item + index}>
                  <img
                    src={imgNextDayDelivery}
                    width="auto"
                    height="auto"
                    alt="Next Day Delivery"
                    className="font-bold"
                  />
                  <div className="font-bold">
                    Next Day
                    <br />
                    Delivery
                  </div>
                </div>
              );
            }
            return <></>;
          })}
        </div>
      </div>
    );
  }
  return <div></div>;
}
