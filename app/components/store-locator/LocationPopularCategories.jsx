import {Image} from '@shopify/hydrogen';
import imgAdjustableBedBases from '~/assets/iconImages/MW-Icon-AdjustableBedBases.png';
import imgBedFrames from '~/assets/iconImages/MW-Icon-BedFrames.png';
import imgBoxSprings from '~/assets/iconImages/MW-Icon-BoxSprings.png';
import imgBunkBeds from '~/assets/iconImages/MW-Icon-BunkBeds.png';
import imgCribMattresses from '~/assets/iconImages/MW-Icon-CribMattresses.png';
import imgDogBeds from '~/assets/iconImages/MW-Icon-DogBeds.jpg';
import imgFutons from '~/assets/iconImages/MW-Icon-Futons.png';
import imgHeadboards from '~/assets/iconImages/MW-Icon-Headboards.png';
import imgHybrid from '~/assets/iconImages/MW-Icon-Hybrid.png';
import imgInnerspring from '~/assets/iconImages/MW-Icon-Innerspring.png';
import imgMattresses from '~/assets/iconImages/MW-Icon-Mattresses.png';
import imgMattressInABox from '~/assets/iconImages/MW-Icon-MattressInABox.png';
import imgMattressToppers from '~/assets/iconImages/MW-Icon-MattressToppers.png';
import imgMemoryFoam from '~/assets/iconImages/MW-Icon-MemoryFoam.png';
import imgPillows from '~/assets/iconImages/MW-Icon-Pillows.png';
import imgSleepAccessories from '~/assets/iconImages/MW-Icon-SleepAccessories.png';

export function LocationPopularCategories({locProducts}) {
  if (locProducts?.length > 0) {
    return (
      <div className="w-full max-w-page-width mx-auto px-6 md:px-8 lg:px-12">
        <div className="flex flex-col gap-4">
          <div id="section-review" className="text-3xl font-bold mb-4 md:mb-0">
            Popular Categories
          </div>
          <div className="grid grid-cols-3 sm:grid-cols-4 md:grid-cols-5 lg:grid-cols-6 gap-3 md:gap-5">
            {(locProducts || []).map((product, index) => {
              const key = `${product || ''}-${index}`;
              let iconImage = null;
              let altText = '';

              if (product === 'Adjustable Bed Bases') {
                iconImage = imgAdjustableBedBases;
                altText = 'Adjustable Bed Bases';
              }
              if (product === 'Bed Frames') {
                iconImage = imgBedFrames;
                altText = 'Bed Frames';
              }
              if (product === 'Box Springs') {
                iconImage = imgBoxSprings;
                altText = 'Box Springs';
              }
              if (product === 'Bunk Beds') {
                iconImage = imgBunkBeds;
                altText = 'Bunk Beds';
              }
              if (product === 'Crib Mattresses') {
                iconImage = imgCribMattresses;
                altText = 'Crib Mattresses';
              }
              if (product === 'Dog Beds') {
                iconImage = imgDogBeds;
                altText = 'Dog Beds';
              }
              if (product === 'Futons') {
                iconImage = imgFutons;
                altText = 'Futons';
              }
              if (product === 'Headboards') {
                iconImage = imgHeadboards;
                altText = 'Headboards';
              }
              if (product === 'Hybrid') {
                iconImage = imgHybrid;
                altText = 'Hybrid';
              }
              if (product === 'Innerspring') {
                iconImage = imgInnerspring;
                altText = 'Innerspring';
              }
              if (product === 'Mattresses') {
                iconImage = imgMattresses;
                altText = 'Mattresses';
              }
              if (product === 'Mattress in a box') {
                iconImage = imgMattressInABox;
                altText = 'Mattress in a box';
              }
              if (product === 'Mattress Toppers') {
                iconImage = imgMattressToppers;
                altText = 'Mattress Toppers';
              }
              if (product === 'Memory Foam') {
                iconImage = imgMemoryFoam;
                altText = 'Memory Foam';
              }
              if (product === 'Pillows') {
                iconImage = imgPillows;
                altText = 'Pillows';
              }
              if (product === 'Sleep Accessories') {
                iconImage = imgSleepAccessories;
                altText = 'Sleep Accessories';
              }

              if (iconImage) {
                return (
                  <div key={key} className="flex flex-col">
                    <img
                      src={iconImage}
                      width="auto"
                      height="auto"
                      alt={altText}
                      className="flex justify-center items-center"
                    />
                    <div className="flex justify-center items-center text-sm sm:text-base md:text-lg font-bold text-center">
                      {product}
                    </div>
                  </div>
                );
              }

              // no-image fallback
              return (
                // eslint-disable-next-line react/jsx-key
                <div className="flex justify-center items-center font-bold text-center">
                  {product}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
  return <div></div>;
}
