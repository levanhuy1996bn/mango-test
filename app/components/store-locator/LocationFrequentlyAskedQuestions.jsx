import {Disclosure} from '@headlessui/react';
import {HiChevronUp} from 'react-icons/hi';

const Linkify = ({children}) => {
  const isUrl = (word) => {
    const urlPattern =
      /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm;
    return word.match(urlPattern);
  };
  const addMarkup = (word) => {
    return isUrl(word) ? `<a href="${word}">${word}</a>` : word;
  };
  const words = children.split(' ');
  const formatedWords = words.map((w) => addMarkup(w));
  const html = formatedWords.join(' ');
  return (
    <div
      className="prose text-base md:text-lg"
      dangerouslySetInnerHTML={{__html: html}}
    />
  );
};

export function LocationFrequentlyAskedQuestions({
  locFrequentlyAskedQuestions,
}) {
  if (locFrequentlyAskedQuestions?.length > 0) {
    return (
      <div className="w-full max-w-page-width mx-auto px-6 md:px-8 lg:px-12">
        <div className="flex flex-col gap-8">
          <div id="section-review" className="text-3xl font-bold mb-4 md:mb-0">
            Frequently Asked Questions
          </div>
          <div className="">
            {locFrequentlyAskedQuestions.map((faq, index) => {
              const key = `faq-${index}`;
              return (
                <Disclosure key={key} as="div" className="grid w-full">
                  {({open}) => (
                    <>
                      <Disclosure.Button className="text-left border-t border-t-mw-grayscale-300 border-b border-b-transparent py-3 break-words">
                        <div className="flex justify-between">
                          <div className="font-bold text-lg md:text-xl">
                            {faq.question}
                          </div>
                          <HiChevronUp
                            className={`${
                              open ? '' : 'rotate-[180deg]'
                            } transition-transform transform-gpu duration-200 text-heading text-mw-grayscale-800 shrink-0`}
                          />
                        </div>
                      </Disclosure.Button>
                      <Disclosure.Panel className={'pb-6'}>
                        {faq.answer && <Linkify>{faq.answer}</Linkify>}
                      </Disclosure.Panel>
                    </>
                  )}
                </Disclosure>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
  return <div></div>;
}
