import {LocationReviewCard} from '~/components';
import {YEXT_ACCOUNT_ID, YEXT_API_KEY, YEXT_API_VER} from '~/lib/const';
import {useEffect, useState} from 'react';

export function LocationCustomerReview({locId}) {
  const [reviews, setReviews] = useState([]);
  const [totalResults, setTotalResults] = useState(0);
  const [isPending, setIsPending] = useState(true);
  const [nextPageToken, setNextPageToken] = useState('');
  const [isFirstVisitTime, setIsFirstVisitTime] = useState(true);

  const initializeReviews = (locId = null, pathname = undefined) => {
    setIsPending(true);
    const url = pathname
      ? `https://cdn.yextapis.com/v2/accounts/${YEXT_ACCOUNT_ID}/content/reviews?entity.id=${locId}&$sortBy__desc=reviewDate&api_key=${YEXT_API_KEY}&v=${YEXT_API_VER}&pageToken=${pathname}`
      : `https://cdn.yextapis.com/v2/accounts/${YEXT_ACCOUNT_ID}/content/reviews?entity.id=${locId}&$sortBy__desc=reviewDate&api_key=${YEXT_API_KEY}&v=${YEXT_API_VER}`;
    fetch(url, {
      method: 'GET',
    })
      .then((response) => {
        return response.json();
      })
      .then(async (data) => {
        setIsPending(false);
        if (data?.response?.count) {
          setTotalResults(data.response.count);
          if (data?.response?.nextPageToken) {
            setNextPageToken(data?.response?.nextPageToken);
          }
          const newReviews = data?.response?.docs || [];
          setReviews([...reviews, ...newReviews]);
        }
      })
      .catch((error) => {
        setIsPending(false);
        console.warn(error);
      });
  };

  useEffect(() => {
    setReviews([]);
    setIsFirstVisitTime(true);
  }, [locId]);

  useEffect(() => {
    if (isFirstVisitTime === false) {
      initializeReviews(locId);
    } else {
      setIsFirstVisitTime(false);
    }
  }, [isFirstVisitTime, locId]);

  return (
    <div className="w-full max-w-page-width mx-auto px-6 md:px-8 lg:px-12">
      <div className="flex flex-col gap-8">
        <div id="section-review" className="text-3xl font-bold mb-4 md:mb-0">
          Customer Reviews
        </div>
        {totalResults > 0 ? (
          <>
            <div
              className={`flex flex-col gap-5 ${isPending ? 'opacity-30' : ''}`}
            >
              {reviews.map((review) => {
                return (
                  <LocationReviewCard
                    key={review?.$key?.primary_key}
                    review={review}
                  />
                );
              })}
            </div>
            {nextPageToken.length > 0 && (
              <div className="flex justify-center w-full">
                <button
                  onClick={() => initializeReviews(locId, nextPageToken)}
                  className={`border inline-block text-center border-mw-red-500 bg-mw-red-500 text-white hover:text-mw-red-500 hover:bg-white py-2 px-4 rounded-lg font-bold text-base min-w-[175px] ${
                    isPending ? 'opacity-50' : ''
                  }`}
                >
                  {isPending ? 'Loading...' : 'Load More Reviews'}
                </button>
              </div>
            )}
          </>
        ) : (
          <>
            {isPending ? (
              ''
            ) : (
              <div className="text-xl font-bold text-mw-grayscale-800 mt-5">
                No Reviews Found
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
}
