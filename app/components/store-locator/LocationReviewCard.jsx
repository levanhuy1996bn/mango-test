import 'moment-timezone';

import {StarIconRating} from '~/components';
import moment from 'moment';

export function LocationReviewCard({review}) {
  const key = review?.$key?.primary_key || review?.apiIdentifier || '';
  const authorName = review?.authorName || '';
  const content = review?.content || '';
  const rating = review?.rating || 0;
  const timeFromNow = review?.reviewDate
    ? moment(review?.reviewDate).fromNow()
    : '';
  return (
    <div
      key={key}
      className="flex flex-col border border-mw-grayscale-300 rounded-lg overflow-hidden p-4 sm:p-5 md:p-6 xl:p-[1.875rem]"
    >
      <div className="flex flex-wrap md:flex-nowrap gap-4 md:gap-8 lg:gap-10 xl:gap-[18rem]">
        <div className="flex flex-col gap-3 basis-40 shrink-0">
          <div className="flex">
            {StarIconRating(rating, 1)}
            {StarIconRating(rating, 2)}
            {StarIconRating(rating, 3)}
            {StarIconRating(rating, 4)}
            {StarIconRating(rating, 5)}
          </div>
          <div className="flex flex-col">
            <div className="text-lg font-bold">{authorName}</div>
            <div className="">{timeFromNow}</div>
          </div>
        </div>
        <div className="text-base sm:text-lg max-w-prose">{content}</div>
      </div>
    </div>
  );
}
