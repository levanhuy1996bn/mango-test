import {useLocation} from '@remix-run/react';
import phoneImage from '~/assets/phone-icon.png';
import {Section} from '~/components';
import {useEffect, useState} from 'react';

export function FooterForm() {
  const [emailError, setEmailError] = useState(null);
  const {pathname} = useLocation();
  // const [firstNameError, setFirstNameError] = useState(null);
  // const [lastNameError, setLastNameError] = useState(null);
  const [isSubmitted, setIsSubmitted] = useState(null);
  const [inputEmail, setInputEmail] = useState('');
  // const [inputFirstName, setInputFirstName] = useState('');
  // const [inputLastName, setInputLastName] = useState('');
  const handleSubmit = (event) => {
    event.preventDefault();
    let isError = false;
    setIsSubmitted(null);
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const email = event?.currentTarget?.FooterNewsletterEmail?.value;
    // const firstName = event?.currentTarget?.FooterNewsletterFirstName?.value;
    // const lastName = event?.currentTarget?.FooterNewsletterLastName?.value;
    // if (!firstName.trim()) {
    //   isError = true;
    //   setFirstNameError(true);
    // }
    //
    // if (!lastName.trim()) {
    //   isError = true;
    //   setLastNameError(true);
    // }

    if (!emailRegex.test(email?.trim())) {
      isError = true;
      setEmailError(true);
    }

    if (isError) {
      return;
    }

    setIsSubmitted(true);
  };

  useEffect(() => {
    setIsSubmitted(null);
    setEmailError(null);
    // setFirstNameError(null);
    // setLastNameError(null);
    setInputEmail('');
    // setInputFirstName('');
    // setInputLastName('');
  }, [pathname]);

  const removeError = (value, setError, setValue) => {
    setValue(value);
    if (value.trim()) {
      setError(null);
    }
  };
  return (
    <div className="bg-mw-grayscale-100">
      <Section
        as="footer"
        role="contentinfo"
        display="flex"
        padding="none"
        className="flex bg-mw-grayscale-100 px-6 md:px-8 lg:px-12 overflow-hidden flex-wrap gap-10 justify-center max-w-page-width mx-auto"
      >
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 md:gap-0 pt-8 pb-3 mx-auto md:mx-0 md:w-full">
          <div>
            <div className="klaviyo-form-YmNXA3" />
          </div>
          <div className="border-t md:border-t-0 md:border-l-2 border-mw-grayscale-700">
            <div className="max-w-[400px] flex items-center pt-6 md:pt-0 text-center md:text-left md:mx-auto">
              <img
                src={phoneImage}
                width="auto"
                height="auto"
                alt="Illustration of mobile phone showing a notification icon"
                className="h-20 lg:h-[7rem]"
              />
              <div className="mw-textmessage-cta-wrapper">
                <h2 className="uppercase text-mw-red-500 font-black text-lg sm:text-xl md:text-2xl mb-1">
                  Let&apos;s Keep In Touch!
                </h2>
                <p className="font-bold text-base sm:text-lg">
                  Text NAP to 21048 to Save 10%
                </p>
              </div>
            </div>
          </div>
        </div>
      </Section>
      <div className="w-full max-w-page-width mx-auto gap-6 px-6 md:px-8 lg:px-12 overflow-hidden mt-3">
        <div className="border-t border-mw-grayscale-200" />
      </div>
    </div>
  );
}
