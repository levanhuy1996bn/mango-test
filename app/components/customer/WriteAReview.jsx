import {MediaFile, useLoadScript} from '@shopify/hydrogen';
import {Link} from '~/components';
import {
  POWER_REVIEWS_MERCHANT_GROUP_ID,
  POWER_REVIEWS_MERCHANT_ID,
  POWER_REVIEWS_READ_API_KEY,
} from '~/lib/const';
import {useEffect} from 'react';

export function WriteAReview({product, pageID}) {
  const powerReviewsReadApiKey = POWER_REVIEWS_READ_API_KEY;
  const merchantId = POWER_REVIEWS_MERCHANT_ID;
  const merchantGroupId = POWER_REVIEWS_MERCHANT_GROUP_ID;
  useLoadScript('https://ui.powerreviews.com/stable/4.1/ui.js');
  useEffect(() => {
    window.pwr =
      window.pwr ||
      function () {
        (pwr.q = pwr.q || []).push(arguments);
      };
    pwr('render', {
      api_key: powerReviewsReadApiKey,
      locale: 'en_US',
      merchant_group_id: merchantGroupId,
      merchant_id: merchantId,
      page_id: pageID,
      return_url: `/products/${product.handle}`,
      on_submit(config, data) {
        console.log(config);
        window.scrollTo({top: 100, left: 0, behavior: 'smooth'});
      },
      components: {
        Write: 'pr-write',
      },
    });
  }, []);

  let productImage = product?.featuredImage || {};
  let dataMedia = {
    image: {
      ...productImage,
      altText: product?.featuredImage.altText || `Picture of ${product.title}`,
    },
    mediaContentType: 'IMAGE',
    __typename: 'MediaImage',
  };

  return (
    <>
      <div className="w-fit my-8 gap-4 p-4 lg:p-8 max-w-prose-wide mx-auto border md:border rounded-2xl border-transparent lg:border-mw-grayscale-300">
        <div className="w-full border-b border-mw-grayscale-300 mb-5">
          <div className="text-subheading2 font-bold text-mw-grayscale-800 mb-3">
            Review This Item
          </div>
        </div>
        <div className="flex flex-col md:flex-row gap-4 md:gap-6">
          {product?.featuredImage && (
            <Link to={`/products/${product?.handle}`}>
              <MediaFile
                className="rounded-2xl"
                data={dataMedia}
                tabIndex="0"
                sizes="600px"
                options={{
                  crop: 'center',
                  scale: 1,
                }}
              />
            </Link>
          )}
          <Link
            to={`/products/${product?.handle}`}
            className="flex align-items items-center text-heading font-bold text-mw-grayscale-800 hover:underline hover:underline-offset-2"
          >
            {product?.title}
          </Link>
        </div>
        <div id="pr-write" />
      </div>
    </>
  );
}
