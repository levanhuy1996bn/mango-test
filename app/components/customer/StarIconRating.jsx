import {StarEmptyIcon, StarFullIcon, StarHalfIcon} from '~/components';

export const StarIconRating = (rating, star) => {
  if (rating >= star - 0.5) {
    return (
      <span className="inline-block mr-1">
        <StarFullIcon />
      </span>
    );
  } else {
    if (rating > star - 1) {
      return (
        <span className="inline-block mr-1">
          <StarHalfIcon />
        </span>
      );
    }

    return (
      <span className="inline-block mr-1">
        <StarEmptyIcon />
      </span>
    );
  }
};
