import {FeaturedCollections, ProductSwimlane} from '~/components';

export function FeaturedSection({featuredCollections, featuredProducts}) {
  return (
    <>
      {featuredCollections?.nodes?.length < 2 && (
        <FeaturedCollections
          title="Popular Collections"
          data={featuredCollections?.nodes}
        />
      )}
      <ProductSwimlane data={featuredProducts?.nodes ?? []} />
    </>
  );
}
