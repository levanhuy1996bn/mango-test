import {Image} from '@shopify/hydrogen';
import {Link} from '~/components';

export function ArticleCard({article, loading, blogHandle}) {
  return (
    <Link
      key={article.id}
      to={`/${blogHandle}/${article.handle}`}
      className="bg-gray-200 rounded-lg align-top overflow-hidden no-underline flex flex-col"
    >
      {article.image && (
        <div className="card-image aspect-[3/2]">
          <Image
            alt={article.image.altText || article.title}
            className="object-cover w-full"
            data={article.image}
            height={400}
            loading={loading}
            sizes="(min-width: 768px) 50vw, 100vw"
            width={600}
            // loaderOptions={{
            //   scale: 2,
            //   crop: 'center',
            // }}
          />
        </div>
      )}
      <div className="text-xl font-bold text-black no-underline max-w-xs p-2">
        {article.title}
      </div>
      <div className="text-sm text-black no-underline italic mt-auto p-2">
        {article.publishedAt}
      </div>
    </Link>
  );
}
